﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FotoDeCarnet
{
    public partial class Main : Form
    {
        const int resolutionX = 98;
        const int resolutionY = 121;
        const int fullResolutionX = 567;
        const int fullResolutionY = 378;
        const int totalImages = 15;
        const int imagesPerRow = 5;

        public Main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    string filePath = openFileDialog.FileName;


                    Image myimage = new Bitmap(filePath);
                    this.panel1.BackgroundImage = myimage;

                    Image resized = Utils.resizeImage(myimage, resolutionY, resolutionX);
                    Image background = new Bitmap(fullResolutionX, fullResolutionY);

                    using (Graphics grfx = Graphics.FromImage(background))
                    {

                        using (SolidBrush brush = new SolidBrush(Color.FromArgb(255, 255, 255, 255)))
                        {
                            grfx.FillRectangle(brush, 0, 0, fullResolutionX, fullResolutionY);
                        }

                        int i = 0;
                        int posX = 0;
                        int posY = 0;

                        do
                        {
                            if (posX >= imagesPerRow)
                            {
                                posX = 0;
                                posY++;
                            }
                            grfx.DrawImage(resized, ((resolutionX + 5) * posX) + 1, (posY * 125) + 1);

                            posX++;
                            i++;
                        }
                        while (i < totalImages);

                    }

                    this.panel2.BackgroundImage = background;
                }
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "PNG Image|*.png|JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                this.panel2.BackgroundImage.Save(saveFileDialog1.FileName);
            }
        }

    }
}
