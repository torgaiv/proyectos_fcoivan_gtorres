﻿using System;
using System.Diagnostics;
using System.IO;

namespace AutoFichador
{
    class Program
    {
        //Path to the data folder, where phyton scripts and log folder must be placed
        private static string dataPath = "[yourpath]\\data";
        private static string pathLogger = dataPath + "\\logs";
        private static string path = dataPath + "\\phtyonScritps";

        static void Main(string[] args)
        {
            try
            {
                DayOfWeek dia = System.DateTime.Now.DayOfWeek;
                log("|" + DateTime.Now + " |Fichar| Start \n");
                
                waitSome();

                switch (dia)
                {
                    case DayOfWeek.Monday:
                        runMondayOrWednesdayOrFriday();
                        break;
                    case DayOfWeek.Tuesday:
                        runTuesdayOrThursday();
                        break;
                    case DayOfWeek.Wednesday:
                        runMondayOrWednesdayOrFriday();
                        break;
                    case DayOfWeek.Thursday:
                        runTuesdayOrThursday();
                        break;
                    case DayOfWeek.Friday:
                        runMondayOrWednesdayOrFriday();
                        break;
                    default:
                        errorLog("fichado");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }

        static void runMondayOrWednesdayOrFriday()
        {

            log(DateTime.Now + "- Se va a desfichar un lunes-miercoles-viernes \n");

            if (DateTime.Now.Hour >= 15 && DateTime.Now.Hour <= 16)
            {
                runDesFicha();
                log(DateTime.Now + "- Se ha lanzado el script de desfichado correctamente\n");
            }
            else
            {
                errorLog("desfichado");
            }
        }
        static void runTuesdayOrThursday()
        {
            log(DateTime.Now + "- Se va a desfichar un martes-jueves \n");

            if (DateTime.Now.Hour >= 15 && DateTime.Now.Hour <= 16)
            {
                runDesFicha();
                log(DateTime.Now + "- Se ha lanzado el script de desfichado por la mañana correctamente\n");
            }
            if (DateTime.Now.Hour >= 19 && DateTime.Now.Hour <= 20)
            {
                runDesFicha();
                log(DateTime.Now + "- Se ha lanzado el script de desfichado por la tarde correctamente\n");
            }
            else
            {
                errorLog("fichado");
            }
        }
       
        static void waitSome() 
        {

            var rand = new Random();
            int random = rand.Next(1, 5);
            random *= 60;
            random *= 1000;
            log(DateTime.Now + "- Se va a esperar  " + random + " milisegundos o " + random / 60000 + "minutos \n");
            System.Threading.Thread.Sleep(random);
        }
        static void log(string text)
        {
            string fileName = pathLogger + "\\log.txt";
            //string textToAdd = "Se ha " + text + "el " + System.DateTime.Now.DayOfWeek.ToString() + " a las " + DateTime.Now + "\n";

            using (StreamWriter writer = new StreamWriter(fileName, true))
            {
                writer.Write(text);
            }
        }
        static void errorLog(string text)
        {
            string fileName = pathLogger + "\\log.txt";
            string textToAdd = DateTime.Now + " - No se ha " + text + " el " + System.DateTime.Now.DayOfWeek.ToString() + " \n";

            using (StreamWriter writer = new StreamWriter(fileName, true))
            {
                writer.Write(textToAdd);
            }
        }
        static void runFicha()
        {
            string pathFicha = path + "\\autologficha.py";
            RunCMD(pathFicha);
        }
        static void runDesFicha()
        {
            string pathFicha = path + "\\autologdesficha.py";
            RunCMD(pathFicha);
        }
        static void RunCMD(string path)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = path;

            start.FileName = "C:\\Windows\\py.exe";//cmd is full path to python.exe
            start.Arguments = path;//args is path to .py file and any cmd line args

            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }
            }
        }

    }
}


    

