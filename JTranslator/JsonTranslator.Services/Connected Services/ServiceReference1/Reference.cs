﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JsonTranslator.Services.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="Api.Traductor", ConfigurationName="ServiceReference1.ServicioRestSoap")]
    public interface ServicioRestSoap {
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento input del espacio de nombres Api.Traductor no está marcado para aceptar valores nil.
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/Translate", ReplyAction="*")]
        JsonTranslator.Services.ServiceReference1.TranslateResponse Translate(JsonTranslator.Services.ServiceReference1.TranslateRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/Translate", ReplyAction="*")]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateResponse> TranslateAsync(JsonTranslator.Services.ServiceReference1.TranslateRequest request);
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento input del espacio de nombres Api.Traductor no está marcado para aceptar valores nil.
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/TranslateJson", ReplyAction="*")]
        JsonTranslator.Services.ServiceReference1.TranslateJsonResponse TranslateJson(JsonTranslator.Services.ServiceReference1.TranslateJsonRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/TranslateJson", ReplyAction="*")]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateJsonResponse> TranslateJsonAsync(JsonTranslator.Services.ServiceReference1.TranslateJsonRequest request);
        
        // CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento getAllLanguagesResult del espacio de nombres Api.Traductor no está marcado para aceptar valores nil.
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/getAllLanguages", ReplyAction="*")]
        JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse getAllLanguages(JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="Api.Traductor/getAllLanguages", ReplyAction="*")]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse> getAllLanguagesAsync(JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TranslateRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Translate", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.TranslateRequestBody Body;
        
        public TranslateRequest() {
        }
        
        public TranslateRequest(JsonTranslator.Services.ServiceReference1.TranslateRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="Api.Traductor")]
    public partial class TranslateRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string input;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string langFrom;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string langTo;
        
        public TranslateRequestBody() {
        }
        
        public TranslateRequestBody(string input, string langFrom, string langTo) {
            this.input = input;
            this.langFrom = langFrom;
            this.langTo = langTo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TranslateResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="TranslateResponse", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.TranslateResponseBody Body;
        
        public TranslateResponse() {
        }
        
        public TranslateResponse(JsonTranslator.Services.ServiceReference1.TranslateResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="Api.Traductor")]
    public partial class TranslateResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string TranslateResult;
        
        public TranslateResponseBody() {
        }
        
        public TranslateResponseBody(string TranslateResult) {
            this.TranslateResult = TranslateResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TranslateJsonRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="TranslateJson", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.TranslateJsonRequestBody Body;
        
        public TranslateJsonRequest() {
        }
        
        public TranslateJsonRequest(JsonTranslator.Services.ServiceReference1.TranslateJsonRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="Api.Traductor")]
    public partial class TranslateJsonRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string input;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string langFrom;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string langTo;
        
        public TranslateJsonRequestBody() {
        }
        
        public TranslateJsonRequestBody(string input, string langFrom, string langTo) {
            this.input = input;
            this.langFrom = langFrom;
            this.langTo = langTo;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class TranslateJsonResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="TranslateJsonResponse", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.TranslateJsonResponseBody Body;
        
        public TranslateJsonResponse() {
        }
        
        public TranslateJsonResponse(JsonTranslator.Services.ServiceReference1.TranslateJsonResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="Api.Traductor")]
    public partial class TranslateJsonResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string TranslateJsonResult;
        
        public TranslateJsonResponseBody() {
        }
        
        public TranslateJsonResponseBody(string TranslateJsonResult) {
            this.TranslateJsonResult = TranslateJsonResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getAllLanguagesRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getAllLanguages", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.getAllLanguagesRequestBody Body;
        
        public getAllLanguagesRequest() {
        }
        
        public getAllLanguagesRequest(JsonTranslator.Services.ServiceReference1.getAllLanguagesRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class getAllLanguagesRequestBody {
        
        public getAllLanguagesRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getAllLanguagesResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="getAllLanguagesResponse", Namespace="Api.Traductor", Order=0)]
        public JsonTranslator.Services.ServiceReference1.getAllLanguagesResponseBody Body;
        
        public getAllLanguagesResponse() {
        }
        
        public getAllLanguagesResponse(JsonTranslator.Services.ServiceReference1.getAllLanguagesResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="Api.Traductor")]
    public partial class getAllLanguagesResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string getAllLanguagesResult;
        
        public getAllLanguagesResponseBody() {
        }
        
        public getAllLanguagesResponseBody(string getAllLanguagesResult) {
            this.getAllLanguagesResult = getAllLanguagesResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ServicioRestSoapChannel : JsonTranslator.Services.ServiceReference1.ServicioRestSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServicioRestSoapClient : System.ServiceModel.ClientBase<JsonTranslator.Services.ServiceReference1.ServicioRestSoap>, JsonTranslator.Services.ServiceReference1.ServicioRestSoap {
        
        public ServicioRestSoapClient() {
        }
        
        public ServicioRestSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServicioRestSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServicioRestSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServicioRestSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        JsonTranslator.Services.ServiceReference1.TranslateResponse JsonTranslator.Services.ServiceReference1.ServicioRestSoap.Translate(JsonTranslator.Services.ServiceReference1.TranslateRequest request) {
            return base.Channel.Translate(request);
        }
        
        public string Translate(string input, string langFrom, string langTo) {
            JsonTranslator.Services.ServiceReference1.TranslateRequest inValue = new JsonTranslator.Services.ServiceReference1.TranslateRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.TranslateRequestBody();
            inValue.Body.input = input;
            inValue.Body.langFrom = langFrom;
            inValue.Body.langTo = langTo;
            JsonTranslator.Services.ServiceReference1.TranslateResponse retVal = ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).Translate(inValue);
            return retVal.Body.TranslateResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateResponse> JsonTranslator.Services.ServiceReference1.ServicioRestSoap.TranslateAsync(JsonTranslator.Services.ServiceReference1.TranslateRequest request) {
            return base.Channel.TranslateAsync(request);
        }
        
        public System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateResponse> TranslateAsync(string input, string langFrom, string langTo) {
            JsonTranslator.Services.ServiceReference1.TranslateRequest inValue = new JsonTranslator.Services.ServiceReference1.TranslateRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.TranslateRequestBody();
            inValue.Body.input = input;
            inValue.Body.langFrom = langFrom;
            inValue.Body.langTo = langTo;
            return ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).TranslateAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        JsonTranslator.Services.ServiceReference1.TranslateJsonResponse JsonTranslator.Services.ServiceReference1.ServicioRestSoap.TranslateJson(JsonTranslator.Services.ServiceReference1.TranslateJsonRequest request) {
            return base.Channel.TranslateJson(request);
        }
        
        public string TranslateJson(string input, string langFrom, string langTo) {
            JsonTranslator.Services.ServiceReference1.TranslateJsonRequest inValue = new JsonTranslator.Services.ServiceReference1.TranslateJsonRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.TranslateJsonRequestBody();
            inValue.Body.input = input;
            inValue.Body.langFrom = langFrom;
            inValue.Body.langTo = langTo;
            JsonTranslator.Services.ServiceReference1.TranslateJsonResponse retVal = ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).TranslateJson(inValue);
            return retVal.Body.TranslateJsonResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateJsonResponse> JsonTranslator.Services.ServiceReference1.ServicioRestSoap.TranslateJsonAsync(JsonTranslator.Services.ServiceReference1.TranslateJsonRequest request) {
            return base.Channel.TranslateJsonAsync(request);
        }
        
        public System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.TranslateJsonResponse> TranslateJsonAsync(string input, string langFrom, string langTo) {
            JsonTranslator.Services.ServiceReference1.TranslateJsonRequest inValue = new JsonTranslator.Services.ServiceReference1.TranslateJsonRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.TranslateJsonRequestBody();
            inValue.Body.input = input;
            inValue.Body.langFrom = langFrom;
            inValue.Body.langTo = langTo;
            return ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).TranslateJsonAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse JsonTranslator.Services.ServiceReference1.ServicioRestSoap.getAllLanguages(JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest request) {
            return base.Channel.getAllLanguages(request);
        }
        
        public string getAllLanguages() {
            JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest inValue = new JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.getAllLanguagesRequestBody();
            JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse retVal = ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).getAllLanguages(inValue);
            return retVal.Body.getAllLanguagesResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse> JsonTranslator.Services.ServiceReference1.ServicioRestSoap.getAllLanguagesAsync(JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest request) {
            return base.Channel.getAllLanguagesAsync(request);
        }
        
        public System.Threading.Tasks.Task<JsonTranslator.Services.ServiceReference1.getAllLanguagesResponse> getAllLanguagesAsync() {
            JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest inValue = new JsonTranslator.Services.ServiceReference1.getAllLanguagesRequest();
            inValue.Body = new JsonTranslator.Services.ServiceReference1.getAllLanguagesRequestBody();
            return ((JsonTranslator.Services.ServiceReference1.ServicioRestSoap)(this)).getAllLanguagesAsync(inValue);
        }
    }
}
