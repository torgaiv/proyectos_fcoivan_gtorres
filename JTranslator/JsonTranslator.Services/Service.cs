﻿using JsonTranslator.Services.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace JsonTranslator.Services
{
    public class Service
    {
        private const string autoLanguage = "auto";
        public string TranslateJSon(string jsonData, string languageTo, string languageFrom = autoLanguage)
        {
            if (string.IsNullOrEmpty(languageFrom))
                languageFrom = autoLanguage;

            ServiceReference1.ServicioRestSoapClient servicio = new ServiceReference1.ServicioRestSoapClient();
            return servicio.TranslateJson(jsonData, languageFrom, languageTo);
        }

        public ICollection<Language> GetAllLanguages()
        {
            ServiceReference1.ServicioRestSoapClient servicio = new ServiceReference1.ServicioRestSoapClient();
            return JsonConvert.DeserializeObject<ICollection<Language>>(servicio.getAllLanguages());
        }

        public string TranslateText(string text, string languageTo, string languageFrom = autoLanguage)
        {
            ServiceReference1.ServicioRestSoapClient servicio = new ServiceReference1.ServicioRestSoapClient();
            if (string.IsNullOrEmpty(languageFrom))
                languageFrom = autoLanguage;
            return servicio.Translate(text, languageFrom, languageTo);
        }
    }

}
