﻿using JsonTranslator.Extensions;

namespace JsonTranslator.Mappers
{
    public static class LanguageServiceToWeb
    {
        public static JsonTranslator.Models.Language Map(this JsonTranslator.Services.Models.Language input)
        {
            return new JsonTranslator.Models.Language()
            {
                LanguageCode = input.LanguageCode,
                LanguageDescription = input.LanguageDescription.FirstCharToUpper()
            };
        }
    }
}