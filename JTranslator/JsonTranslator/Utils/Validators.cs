﻿using JsonTranslator.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace JsonTranslator.Utils
{
    public static class Validators
    {
        public static ErrorEnum ValidateTranslator(TranslateViewModel file)
        {
            if(file == null)
            {
                return ErrorEnum.NullFile;
            }
            if (file.Data == null)
            {
                return ErrorEnum.NullDate;
            }
            if (file.Data.ContentLength <= 0)
            {
                return ErrorEnum.EmptyFile;
            }

            return ErrorEnum.OK;
        }

        public static ErrorEnum ValidateJsonSchema(string json)
        {
            try
            {
                Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                return ErrorEnum.OK;
            }
            catch (Exception)
            {
                return ErrorEnum.UnknownError;
            }
        }
    }
}