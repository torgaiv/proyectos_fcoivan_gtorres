﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JsonTranslator.Utils
{
    public enum ErrorEnum
    {
        OK=1,
        EmptyFile = 2,
        UnknownError = 3,
        NullFile = 4,
        NullDate = 5,
        NoDictionaryJSON = 6,
    }
}