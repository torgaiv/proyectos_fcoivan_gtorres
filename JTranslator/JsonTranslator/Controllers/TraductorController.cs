﻿using JsonTranslator.Models;
using JsonTranslator.Models.ViewModels;
using JsonTranslator.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Web.Mvc;

namespace JsonTranslator.Controllers
{
    public class TraductorController : Controller
    {
        Services.Service webService;

        // GET: Traductor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Languages()
        {
            webService = new Services.Service();

            IEnumerable<Language> languages = webService.GetAllLanguages().Select(Mappers.LanguageServiceToWeb.Map);
            LanguagesViewModel languagesViewModel = new LanguagesViewModel();
            languagesViewModel.languages = languages.ToList();

            return View(languagesViewModel);
        }

        public ActionResult TraductorTexto(TraductorTextoViewModel file, string error = null)
        {
            webService = new Services.Service();
            TraductorTextoViewModel model = new TraductorTextoViewModel();

            if (!(file is null))
            {
                model = file;
            }

            try
            {
                webService = new Services.Service();

                IEnumerable<Language> languages = webService.GetAllLanguages().Select(Mappers.LanguageServiceToWeb.Map);
                model.AllLanguages = new SelectList(languages.OrderBy(t => t.LanguageDescription), "LanguageCode", "LanguageDescription");

                if (string.IsNullOrEmpty(error) == false)
                {
                    ViewBag.Message = Localization.ResourceManager.GetString(error);
                }

                return View(model);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(404, e.Message.Replace("\r\n", ""));
            }
        }


        [HttpPost]
        public ActionResult Translate(TraductorTextoViewModel file)
        {

            if (file is null || string.IsNullOrWhiteSpace(file.data))
            {
                return RedirectToAction("TraductorTexto", "Traductor", new { });
            }
            webService = new Services.Service();

            string translatedText = webService.TranslateText(file.data, file.LanguageToSelected, file.LanguageFromSelected);

            file.translatedData = translatedText; 
            return TraductorTexto(file);
 
        }

    }
}