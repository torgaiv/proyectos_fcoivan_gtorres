﻿using JsonTranslator.Extensions;
using JsonTranslator.Models;
using JsonTranslator.Properties;
using JsonTranslator.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace JsonTranslator.Controllers
{
    public class HomeController : Controller
    {
        Services.Service webService;

        public ActionResult Index(string error = null)
        {
            try
            {
                webService = new Services.Service();

                IEnumerable<Language> languages = webService.GetAllLanguages().Select(Mappers.LanguageServiceToWeb.Map);

                TranslateViewModel model = new TranslateViewModel()
                {
                    AllLanguages = new SelectList(languages.OrderBy(t => t.LanguageDescription), "LanguageCode", "LanguageDescription"),
                };

                if (string.IsNullOrEmpty(error) == false)
                {
                    ViewBag.Message = Localization.ResourceManager.GetString(error);
                }

                return View(model);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(404, e.Message.Replace("\r\n", ""));
            }
        }

        [HttpPost]
        public ActionResult Download(TranslateViewModel file)
        {
            ErrorEnum response = Validators.ValidateTranslator(file);
            try
            {
                if (response == ErrorEnum.OK)
                {

                    webService = new Services.Service();

                    string path = Path.Combine(Server.MapPath("/AppFiles/"), Path.GetFileName(file.Data.FileName));
                    file.Data.SaveAs(path);
                    string jsonData = System.Text.Encoding.UTF8.GetString(System.IO.File.ReadAllBytes(path));
                    System.IO.File.Delete(path);

                    if (Validators.ValidateJsonSchema(jsonData) == ErrorEnum.OK)
                    {
                        string translatedJson = webService.TranslateJSon(jsonData, file.LanguageToSelected, file.LanguageFromSelected);
                        byte[] fileBytes = translatedJson.ToByteArray();
                        string fileName = "translated_" + file.Data.FileName;

                        return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                    }
                    else
                    {
                        response = ErrorEnum.NoDictionaryJSON;
                    }
                }
                else
                {
                    ViewBag.Message = response.ToString();
                }
            }
            catch (Exception e)
            {
                //    ViewBag.Exception = e.Message.ToE E String();
                //    response = ErrorEnum.UnknownError;  

                ViewBag.Message = e.Message.ToString();
            }

            return RedirectToAction("Index", "Home", new { error = response.ToString() });
        }

        public ActionResult MainMenu() 
        {
            return View();
        }
    }
}