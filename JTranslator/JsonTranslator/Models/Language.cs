﻿namespace JsonTranslator.Models
{
    public class Language
    {
        public string LanguageCode { get; set; }

        public string LanguageDescription { get; set; }
    }
}