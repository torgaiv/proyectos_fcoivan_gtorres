﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace JsonTranslator.Models
{
    public class TranslateViewModel
    {
        public HttpPostedFileBase Data { get; set; }

        public SelectList AllLanguages { get; set; }

        public string LanguageFromSelected { get; set; }
         

        public string LanguageToSelected { get; set; }
    }
}