﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JsonTranslator.Models.ViewModels
{
    public class TraductorTextoViewModel
    { 

        public string data { get; set; }
        public string translatedData { get; set; }

        public SelectList AllLanguages { get; set; }

        public string LanguageFromSelected { get; set; }


        public string LanguageToSelected { get; set; }
    }
}