﻿using System.Collections.Generic;

namespace JsonTranslator.Models.ViewModels
{
    public class LanguagesViewModel
    {
        public List<Language> languages { get; set; }
    }
}