﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace JsonTranslator.Extensions
{
    public static class ExtensionsMethods
    {
        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        public static byte[] ToByteArray(this string str)
        {
            Encoding encoding = Encoding.Default;
            return encoding.GetBytes(str);
        }

    }
}