﻿using Api.Business;
using Api.Core.Traductor.Models;
using Api.Core.Traductor.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Core.Traductor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TranslatorController : ControllerBase
    {

        private ITranslatorService translatorService;
        private readonly ILogger<TranslatorResponse> _logger;

        public TranslatorController(ILogger<TranslatorResponse> logger)
        {
            _logger = logger;
            this.translatorService = new TranslatorService();
        }

        [HttpPost(Name = "GetTranslation")]
        public TranslatorResponse Get(TranslatorRequest request)
        { 
            return this.translatorService.getTranslation(request);
        }

    }
}
