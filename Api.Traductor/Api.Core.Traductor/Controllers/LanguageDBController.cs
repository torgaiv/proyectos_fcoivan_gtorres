﻿using Api.Business;
using Api.Core.Traductor.Models;
using Api.Core.Traductor.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Core.Traductor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LanguageDBController : ControllerBase
    {
        private ITranslatorService translatorService;
        private readonly ILogger<TranslatorResponse> _logger;

        public LanguageDBController(ILogger<TranslatorResponse> logger)
        {
            _logger = logger;
            this.translatorService = new TranslatorService();
        }

        [HttpGet(Name = "GetLanguages")]
        public IList<LanguagesResponse> Get()
        {
            return this.translatorService.getLanguages();
        }        

    }
}
