﻿using Api.Business;
using Api.Core.Traductor.Models;
using Api.Core.Traductor.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Core.Traductor.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LanguageWEBController : ControllerBase
    {

        private ITranslatorService translatorService;
        private readonly ILogger<TranslatorResponse> _logger;

        public LanguageWEBController(ILogger<TranslatorResponse> logger)
        {
            _logger = logger;
            this.translatorService = new TranslatorService();
        }

        [HttpGet(Name = "Get")]
        public IList<LanguagesResponse> Get()
        {
            return this.translatorService.GetLanguagesFromGoogle();
        }

    }
}
