﻿namespace Api.Core.Traductor.Models
{
    public class LanguagesResponse
    {
        public string LanguageCode { get; set; }

        public string LanguageDescription { get; set; }
    }
}
