﻿namespace Api.Core.Traductor.Models
{
    public class TranslatorRequest
    {
        public string input { get; set; }
        public string languageFrom { get; set; }
        public string languageTo { get; set; }
    }
}
