﻿using Api.Core.Traductor.Models; 
using System.Text.Json;

namespace Api.Core.Traductor.Services
{
    public class TranslatorService : ITranslatorService
    {
        public IList<LanguagesResponse> getLanguages()
        {
            List<LanguagesResponse> languages = new List<LanguagesResponse>();
            var listLanguages = Api.Business.Core.Languages.getAllLanguages();



             foreach (var item in listLanguages)            
             {
                languages.Add(new LanguagesResponse() 
                 {
                     LanguageCode= item.LanguageCode,
                     LanguageDescription= item.LanguageDescription
                 });
             }

            return languages;
        }
        public IList<LanguagesResponse> GetLanguagesFromGoogle()
        {
            List<LanguagesResponse> languages = new List<LanguagesResponse>();
            var listLanguages = Api.Business.Core.Languages.getAllLanguagesFromWeb();


            foreach (var item in listLanguages)
            {
                languages.Add(new LanguagesResponse()
                {
                    LanguageCode = item.LanguageCode,
                    LanguageDescription = item.LanguageDescription
                });
            }



            return languages;
        }

        public TranslatorResponse getTranslation(TranslatorRequest request)
        {
            return new TranslatorResponse() { response = Api.Business.Core.Translator.TranslateText(request.input, request.languageFrom, request.languageTo) };
        }
    }
}
