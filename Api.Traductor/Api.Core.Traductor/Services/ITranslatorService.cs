﻿using Api.Core.Traductor.Models;

namespace Api.Core.Traductor.Services
{
    public interface ITranslatorService
    {
        TranslatorResponse getTranslation(TranslatorRequest request);
        IList<LanguagesResponse> getLanguages();
        IList<LanguagesResponse> GetLanguagesFromGoogle();
    }
}
