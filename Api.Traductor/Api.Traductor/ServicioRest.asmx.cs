﻿using System.Web.Script.Serialization;
using System.Web.Services;

namespace Api.Traductor
{
    [WebService(Namespace = "Api.Traductor")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class ServicioRest : System.Web.Services.WebService
    {
        [WebMethod]
        public string Translate(string input, string langFrom, string langTo)
        {
            if (string.IsNullOrEmpty(input))
                return null;
            if (string.IsNullOrEmpty(langFrom))
                return "Seleccione un lenguaje de entrada";
            if (string.IsNullOrEmpty(langTo))
                return "Seleccione un lenguaje de salida";

            return Api.Business.Traductor.TranslateText(input, langFrom, langTo);
        }

        [WebMethod]
        public string TranslateJson(string input, string langFrom, string langTo)
        {
            if (string.IsNullOrEmpty(input))
                throw new System.Exception("El campo input no puede estar vacío");
            if (string.IsNullOrEmpty(langFrom))
                throw new System.Exception("El campo lenguaje origen no puede estar vacío");
            if (string.IsNullOrEmpty(langTo))
                throw new System.Exception("El campo lenguaje destino no puede estar vacío");

            return Api.Business.JsonTranslator.TranslateJson(input, langFrom, langTo);
        }

        [WebMethod]
        public string getAllLanguages()
        { 
            var jsonSerialiser = new JavaScriptSerializer();
            return jsonSerialiser.Serialize(Api.Business.Languages.getAllLanguages());
        }
    }
}
