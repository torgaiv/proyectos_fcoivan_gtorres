﻿using Api.Business.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using Microsoft.Data.SqlClient;

namespace Api.Business
{
    public static class Languages
    {
        public static string getAllLanguagesJSON()
        {
            ICollection<Language> list = getAllLanguages();
            return JsonConvert.SerializeObject(list);
        }

        public static ICollection<Language> getAllLanguages()
        {
            ICollection<Language> response = null;
            try
            {
                string connectionString = "Server = languagestorgaiv.mssql.somee.com; Database = languagestorgaiv; User ID = torgaiv_SQLLogin_1; Password = 42hx2p47wc; Trusted_Connection = False;";
                //string connectionString = "Data Source=localhost\\SQLEXPRESS;Initial Catalog=languages;Integrated Security=True";

                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();

                    SqlCommand cmd = new SqlCommand("SELECT * FROM languages", cn);
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        response = new List<Language>();

                        while (dr.Read())
                        {
                            response.Add(new Language()
                            {
                                LanguageCode = Convert.ToString(dr["id"]),
                                LanguageDescription = Convert.ToString(dr["name"])
                            });
                        }
                    }
                }
            }
            catch (SqlException sex)
            {
                throw new Exception("Database connection can not be done. Error message:  " + sex.Message);
            }
            catch (Exception ex)
            {

                throw new Exception("Unknown error:" + ex.Message);
            }

            return response;
        }
    }
}
