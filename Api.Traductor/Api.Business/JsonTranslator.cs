﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Api.Business
{
    public static class JsonTranslator
    {
        public static string TranslateJson(string collection, string languageCodeFrom, string languageCodeTo)
        {
            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(collection);

            Dictionary<string, string> translatedCollection = new Dictionary<string, string>();

            foreach (KeyValuePair<string, string> item in values)
            {
                if (string.IsNullOrEmpty(item.Value) == false)
                {
                    string translatedCollectionValue = Traductor.TranslateText(item.Value, languageCodeFrom, languageCodeTo);
                    translatedCollection.Add(item.Key, translatedCollectionValue);
                }
            }
            return JsonConvert.SerializeObject(translatedCollection, Formatting.Indented); 
        }
    }
}
