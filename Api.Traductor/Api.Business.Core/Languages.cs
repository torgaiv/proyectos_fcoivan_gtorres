﻿using Api.Business.Core.Models;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Net.Http;

namespace Api.Business.Core
{

    public static class Languages
    {
        public static string getAllLanguagesJSON()
        {
            ICollection<Language> list = getAllLanguages();
            return JsonConvert.SerializeObject(list);
        }

        public static ICollection<Language> getAllLanguagesFromWeb()
        {
            HttpClient httpClient = new HttpClient();
            ICollection<Language> response = null;
            string googleDevsLanguageCodeURL = "https://developers.google.com/admin-sdk/directory/v1/languages";

            try
            {
                string webResult = System.Net.WebUtility.HtmlDecode(httpClient.GetStringAsync(googleDevsLanguageCodeURL).Result); ;
                string TrimValue = "<tbody>";
                string dividerEnd = "</tbody>";

                int getTranslatedString = webResult.IndexOf(TrimValue) + TrimValue.Length;
                string getTranslatedFullString = webResult.Substring(getTranslatedString);

                int FinalValues = getTranslatedFullString.IndexOf(dividerEnd);
                string Result = getTranslatedFullString.Substring(0, FinalValues).Replace("\r","").Replace("\n","");
                string jsonManipulation = "["+Result+"]";
                jsonManipulation = jsonManipulation.Replace("<tr><td>", "{LanguageDescription:\" ").Replace("</td><td>", "\",LanguageCode:\"").Replace("</td></tr>","\"},");
                return JsonConvert.DeserializeObject<List<Language>>(jsonManipulation);                 
            } 
            catch (Exception ex)
            {
                 
            }

            return response;
        }
    
        public static ICollection<Language> getAllLanguages()
        {
            ICollection<Language> response = null;
            try
            {
                string connectionString = "Server = languagestorgaiv.mssql.somee.com; Database = languagestorgaiv; User ID = torgaiv_SQLLogin_1; Password = 42hx2p47wc; Trusted_Connection = False;";
                //string connectionString = "Data Source=localhost\\SQLEXPRESS;Initial Catalog=languages;Integrated Security=True";

                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    cn.Open();

                    SqlCommand cmd = new SqlCommand("SELECT * FROM languages", cn);
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows)
                    {
                        response = new List<Language>();

                        while (dr.Read())
                        {
                            response.Add(new Language()
                            {
                                LanguageCode = Convert.ToString(dr["id"]),
                                LanguageDescription = Convert.ToString(dr["name"])
                            });
                        }
                    }
                }
            }
            catch (SqlException sex)
            {
                throw new Exception("Database connection can not be done. Error message:  " + sex.Message);
            }
            catch (Exception ex)
            {

                throw new Exception("Unknown error:" + ex.Message);
            }

            return response;
        }
    
    
    }
}
