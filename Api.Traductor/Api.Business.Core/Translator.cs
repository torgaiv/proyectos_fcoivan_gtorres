﻿namespace Api.Business.Core
{
    public static class Translator
    {
        public static string TranslateText(string input, string languageFrom, string languageTo)
        {
            HttpClient httpClient = new HttpClient();

            string divider = "<resulttotrim>";
            string dividerEnd = "</resulttotrim>";
            input = divider + input + dividerEnd;
            string url = String.Format("https://translate.google.com/m?hl={1}&sl={1}&tl={2}&ie=UTF-8&prev=_m&q={0}", input, languageFrom, languageTo);
            
            string result = httpClient.GetStringAsync(url).Result;
            string Aresult = System.Net.WebUtility.HtmlDecode(result);
            string TrimValue = divider;
            int getTranslatedString = Aresult.IndexOf(TrimValue) + TrimValue.Length;
            string getTranslatedFullString = Aresult.Substring(getTranslatedString);


            getTranslatedString = getTranslatedFullString.IndexOf(TrimValue) + TrimValue.Length;
            getTranslatedFullString = getTranslatedFullString.Substring(getTranslatedString);

            if (getTranslatedFullString.Contains("\\"))
            {
                if (getTranslatedFullString.Contains("\\x26#39;"))
                {
                    getTranslatedFullString = getTranslatedFullString.Replace("\\x26#39;", "'");
                }
            }

            int FinalValues = getTranslatedFullString.IndexOf(dividerEnd);
            string Result = getTranslatedFullString.Substring(0, FinalValues);
            return Result;
        }
    }
}
