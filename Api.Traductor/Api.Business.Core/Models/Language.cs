﻿namespace Api.Business.Core.Models
{

    [Serializable]
    public class Language
    {
        public string LanguageCode { get; set; }

        public string LanguageDescription { get; set; }
    }
}
