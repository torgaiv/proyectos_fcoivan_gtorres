﻿using Newtonsoft.Json;

namespace Api.Business.Core
{
    public static class JsonTranslator
    {
        public static string TranslateJson(string collection, string languageCodeFrom, string languageCodeTo)
        {
            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(collection);

            Dictionary<string, string> translatedCollection = new Dictionary<string, string>();

            foreach (KeyValuePair<string, string> item in values)
            {
                if (string.IsNullOrEmpty(item.Value) == false)
                {
                    string translatedCollectionValue = Translator.TranslateText(item.Value, languageCodeFrom, languageCodeTo);
                    translatedCollection.Add(item.Key, translatedCollectionValue);
                }
            }
            return JsonConvert.SerializeObject(translatedCollection, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
