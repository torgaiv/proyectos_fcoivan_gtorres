﻿using Crossword.Api.Contracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Crossword.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CrosswordController : ControllerBase
    {
        private ICrosswordService crossword;

        public CrosswordController(ICrosswordService crossword)
        {
            this.crossword = crossword;
        }

        [HttpGet]
        [Route("api/GetDefaultCrosswordAsJson")]

        public string GetDefaultCrosswordAsJson()
        {
            return crossword.GetCrossWordAsJson();
        }


        [HttpGet]
        [Route("api/GetDefaultCrosswordAsText")]

        public string GetDefaultCrosswordAsText()
        {
            return crossword.GetCrosswordAsText();
        }

        [HttpGet]
        [Route("api/GetDefaultCrosswordSolvedAsJson")]

        public string GetDefaultCrosswordSolvedAsJson()
        {   
            crossword.ResolveCrossword();
            return crossword.GetCrossWordAsJson();
        }

        [HttpGet]
        [Route("api/GetDefaultCrosswordSolvedAsText")]

        public string GetDefaultCrosswordSolvedAsText()
        {
            crossword.ResolveCrossword();
            return crossword.GetCrosswordAsText();
        }


        [HttpPost]
        [Route("api/ResolveCrossword")]

        public string Resolve(string requestCrossword)
        {
            crossword = JsonConvert.DeserializeObject<CrosswordService>(requestCrossword);
            crossword.ResolveCrossword();
            return JsonConvert.SerializeObject(crossword);
        }
    }
}
