﻿
using Crossword.Api.Contracts;
using Crossword.Api.Models;
using Newtonsoft.Json;

namespace Crossword.Api
{
    public class CrosswordService : ICrosswordService
    {
        private CrosswordModel Crossword;

        public CrosswordService()
        {
            this.Crossword = new CrosswordModel();
        }

        public void ResolveCrossword()
        {
            int tries = 0;

            do
            {
                tries++;
                SortCrossword();
            } while (AnyUnkwon() && tries < Variables.DifficultySolution);
        }

        public void SortCrossword()
        {
            for (int i = 0; i < Variables.TamX; i++)
            {
                for (int j = 0; j < Variables.TamY; j++)
                {
                    CalculateDistance(i, j);
                }
            }
        }

        public bool AnyUnkwon()
        {
            for (int i = 0; i < Variables.TamX; i++)
            {
                for (int j = 0; j < Variables.TamY; j++)
                {
                    if (this.Crossword.Dijkstra[i, j] == Variables.UnknownPosition)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void CalculateDistance(int x, int y)
        {
            int minVale = int.MaxValue;

            if (x == 0 && y == 0)
            {
                this.Crossword.Dijkstra[x, y] = 0;
                return;
            }

            if (this.Crossword.Dijkstra[x, y] != Variables.UnknownPosition)
            {
                return;
            }

            if (x > 0)
            {
                if (this.Crossword.Dijkstra[x - 1, y] < minVale && this.Crossword.Dijkstra[x - 1, y] >= 0)
                {
                    minVale = this.Crossword.Dijkstra[x - 1, y];
                }
            }
            if (y > 0)
            {
                if (this.Crossword.Dijkstra[x, y - 1] < minVale && this.Crossword.Dijkstra[x, y - 1] >= 0)
                {
                    minVale = this.Crossword.Dijkstra[x, y - 1];
                }
            }
            if (x < Variables.TamX - 1)
            {
                if (this.Crossword.Dijkstra[x + 1, y] < minVale && this.Crossword.Dijkstra[x + 1, y] >= 0)
                {
                    minVale = this.Crossword.Dijkstra[x + 1, y];
                }
            }
            if (y < Variables.TamY - 1)
            {
                if (this.Crossword.Dijkstra[x, y + 1] < minVale && this.Crossword.Dijkstra[x, y + 1] >= 0)
                {
                    minVale = this.Crossword.Dijkstra[x, y + 1];
                }
            }
            if (minVale != int.MaxValue)
            {
                this.Crossword.Dijkstra[x, y] = minVale + 1;
            }
        }

        public string GetValueFromCossword(int inputCharacter)
        {
            if (inputCharacter == Variables.InitialPosition)
            {
                return "*";
            }
            if (inputCharacter == Variables.UnreacheablePosition)
            {
                return "X";
            }
            if (inputCharacter == Variables.UnknownPosition)
            {
                return "?";
            }
            else
            {
                return inputCharacter.ToString();
            }
        }

        public string GetCrossWordAsJson()
        {
            return JsonConvert.SerializeObject(this.Crossword);
        }

        public string GetCrosswordAsText()
        {
            string result = string.Empty;
            bool twoDigits = false;

            for (int i = Variables.TamX - 1; i >= 0; i--)
            {
                for (int j = 0; j < Variables.TamY; j++)
                {
                    string printValue = GetValueFromCossword(this.Crossword.Dijkstra[i, j]);

                    twoDigits = printValue.Length > 1;

                    result += $"|{printValue}";

                    if (!twoDigits)
                    {
                        result += " ";
                    }

                }
                result += "|";
                result += "\n";
            }

            return result;
        }
    }
}
