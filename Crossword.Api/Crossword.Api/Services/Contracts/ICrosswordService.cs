﻿namespace Crossword.Api.Contracts
{
    public interface ICrosswordService
    {

        public void ResolveCrossword();         
        public void SortCrossword();         
        public bool AnyUnkwon();         
        public void CalculateDistance(int x, int y);         
        public string GetValueFromCossword(int inputCharacter);         
        
        string GetCrosswordAsText();
        string GetCrossWordAsJson() ;
    }

}
