﻿namespace Crossword.Api.Models
{
    public static class Variables
    {
        public static readonly int TamX = 8;
        public static readonly int TamY = 8;
        public static readonly int UnknownPosition = -2;
        public static readonly int InitialPosition = 0;
        public static readonly int UnreacheablePosition = -1;
        public static readonly int DifficultySolution = 20;
    }

}
