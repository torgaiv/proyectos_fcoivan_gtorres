﻿namespace Crossword.Api.Models
{
    public class CrosswordModel
    {
        public int[,] Dijkstra { get; set; } = new int[Variables.TamX, Variables.TamY];

        public CrosswordModel() 
        {
            SetDefaultUp();
        }         

        public void SetDefaultUp()
        {
            for (int i = 0; i < Variables.TamX; i++)
            {
                for (int j = 0; j < Variables.TamY; j++)
                {
                    this.Dijkstra[i, j] = Variables.UnknownPosition;
                }
            }

            //Default crossword values
            this.Dijkstra[0, 0] = Variables.InitialPosition;

            this.Dijkstra[0, 2] = Variables.UnreacheablePosition;
            this.Dijkstra[0, 6] = Variables.UnreacheablePosition;

            this.Dijkstra[1, 0] =Variables.UnreacheablePosition;
            this.Dijkstra[1, 2] =Variables.UnreacheablePosition;
            this.Dijkstra[1, 4] = Variables.UnreacheablePosition;

            this.Dijkstra[2, 4] = Variables.UnreacheablePosition;
            this.Dijkstra[2, 5] = Variables.UnreacheablePosition;
            this.Dijkstra[2, 6] = Variables.UnreacheablePosition;
                                  
            this.Dijkstra[3, 1] = Variables.UnreacheablePosition;
            this.Dijkstra[3, 2] = Variables.UnreacheablePosition;
            this.Dijkstra[3, 4] = Variables.UnreacheablePosition;
            this.Dijkstra[3, 5] = Variables.UnreacheablePosition;
                                  
            this.Dijkstra[4, 7] = Variables.UnreacheablePosition;
                                  
            this.Dijkstra[5, 0] = Variables.UnreacheablePosition;
            this.Dijkstra[5, 1] = Variables.UnreacheablePosition;
            this.Dijkstra[5, 3] = Variables.UnreacheablePosition;
            this.Dijkstra[5, 4] = Variables.UnreacheablePosition;
            this.Dijkstra[5, 5] = Variables.UnreacheablePosition;
                                  
            this.Dijkstra[6, 1] = Variables.UnreacheablePosition;
            this.Dijkstra[6, 3] = Variables.UnreacheablePosition;
            this.Dijkstra[6, 5] = Variables.UnreacheablePosition;
            this.Dijkstra[6, 6] = Variables.UnreacheablePosition;

        }
    }
}
