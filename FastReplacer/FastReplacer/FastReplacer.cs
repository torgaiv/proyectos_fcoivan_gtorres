﻿using System;
using System.IO;
using System.Text;

namespace FastReplacer
{
    /// <summary>
    /// =============================================================================
    /// Desarrollador: F. Iván G. Torres 
    /// Fecha: 11/04/2018
    /// =============================================================================
    /// Descripcion:
    /// Programa de consola que replazará una letra, una palabra o una frase
    /// de un documento de TEXTO PLANO por otra letra, palabra o frase introducida 
    /// por el usuario.
    /// 
    /// =============================================================================
    /// Ejemplo de uso:
    /// -Para solo un caracter o palabra
    ///     ./FastReplacer.appref-ms C:/Carpeta(s)/Archivo.txt PalabraARemplazar NuevaPalabra
    /// -Para más de una palabra
    ///     FastReplacer.appref-ms C:/Carpeta(s)/Archivo.txt Palabras_A_Remplazar Nuevas_Palabras
    /// =============================================================================
    /// /!\ Advertencias: 
    /// ----Aplicacion pensada para reemplazar caracteres en texto plano si se usa
    ///     sobre texto enriquecido puede causar pérdida de datos.
    /// ----La apliación está pensada para texto codificado bajo UTF-8, por lo que si
    /// se pretende que se puedan leer carácteres especiales como tildes o 'ñ' el archivo
    /// de texto deberá estar codificado en UTF-8. No obstante también se acepta el resto 
    /// de codificaciones, causando pérdidas de datos en carácteres no encontrados.
    /// =============================================================================
    /// </summary>

    class FReplacer
    {
        /* Version :1.0 */
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            try
            {
                if (CheckArgumentos(args))
                {
                    if (!String.IsNullOrEmpty(CheckPath(args[0])))
                    {
                        string path = args[0];
                        if (CheckFile(path))
                        {
                            string palabraAntigua = LimpiaPalabras(args[1]);
                            string palabraNueva = LimpiaPalabras(args[2]);
                            if (CambiaPalabras(path, palabraAntigua, palabraNueva))
                            {
                                Console.WriteLine("OK! : Operación realizada con éxito");
                            }
                            else
                            {
                                Console.WriteLine("Error: No se ha podido realizar la operación");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Error: No se ha podido abrir el archivo");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error: Ruta del archivo incorrecto");
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
                Console.WriteLine("Pulse Enter para finalizar...");
                Console.ReadLine();
        }

        /// <summary>
        /// Comprueba si solo se han introducido 3 argumentos.
        /// </summary>
        /// <param name="argumentos">The argumentos.</param>
        /// <returns></returns>
        private static bool CheckArgumentos(string[] argumentos)
        {
            if (argumentos.Length == 3)
            {
                if (argumentos[0].Length > 0 && argumentos[1].Length > 0 && argumentos[2].Length > 0)
                {
                    return true;
                }
                Console.WriteLine("Error: Argumentos incorrectos. Uno de los argumentos está vacío");
            }
            Console.WriteLine("Error: Numero de argumentos incorrectos. Se esperaban (3) y se introdujeron ({0})", argumentos.Length);

            return false;
        }

        /// <summary>
        /// Comprueba si la ruta del archivo es válida.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static string CheckPath(string path)
        {
            try
            {
                Path.GetFullPath(path);
                return path;
            }
            catch (Exception) { return null; }
        }

        /// <summary>
        /// Comprueba si se puede abrir el archivo.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private static bool CheckFile(string path)
        {
            try
            {
                using (FileStream fs = File.Open(path, FileMode.Open))
                {
                    byte[] b = new byte[64];
                    UTF8Encoding temp = new UTF8Encoding(true);

                    fs.Read(b, 0, b.Length);
                    fs.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Limpia las barras bajas y las reemplaza por espacios.
        /// </summary>
        /// <returns></returns>
        private static string LimpiaPalabras(string palabras)
        {
            if (palabras.Contains("_"))
            {
                string palabraConEspacios = palabras.Replace("_", " ");
                return palabraConEspacios;
            }
            else
            {
                return palabras;
            }
        }

        /// <summary>
        /// Lee el fichero y cambia la(s) palabra(s).
        /// </summary>
        /// <param name="Path">The path.</param>
        /// <param name="Antigua">Antigua palabra.</param>
        /// <param name="Nueva">Nueva palabra.</param>
        /// <returns></returns>
        private static bool CambiaPalabras(string Path, string Antigua, string Nueva)
        {
            try
            {
                string Texto = string.Empty;

                StreamReader sr = new StreamReader(Path, System.Text.Encoding.UTF8);
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Texto = Texto + s + Environment.NewLine;
                    }
                 }
                sr.Close();

                string TextoNuevo = Texto.Replace(Antigua, Nueva);
                File.WriteAllText(Path, TextoNuevo);                 
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
