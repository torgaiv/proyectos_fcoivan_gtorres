﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class MuestraColores : Form
    {
        List<Color> colors;

        public MuestraColores(List<Color> buenos)
        {
            InitializeComponent();
            this.colors = buenos;
        }

        private void MuestraColores_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < colors.Count; i++)
            {
                PictureBox p = new PictureBox();
                p.BackColor = colors[i];
                p.Height = 40;
                p.Width = 40;
                p.Location = new Point((1+i) * 50, 45);
                panel1.Controls.Add(p);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
