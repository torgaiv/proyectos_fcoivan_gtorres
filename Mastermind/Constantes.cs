﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind
{
    public class Constantes
    {
        public int dificultad;

        public Constantes() {
            dificultad = -1;
        }
        public int Dificultad
        {
            get { return dificultad; }
            set { dificultad = value; }
        }
    }
}
