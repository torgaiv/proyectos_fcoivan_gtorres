﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class SeleccionaColores : Form
    {
        List<PictureBox> colors;
        public SeleccionaColores(ref List<PictureBox> colores)
        {
            InitializeComponent();
            this.colors = colores;
            for (int i = 0; i < colors.Count; i++)
            {
                PictureBox p = colors[i];
                p.Height = 40;
                p.Width = 40;
                p.Location = new Point(i*45, 45);

                panel1.Controls.Add(p);
                p.Click += new System.EventHandler(p_Click);
            }


        }
        private void p_Click(object sender, EventArgs e){
            ColorDialog colorDlg = new ColorDialog();

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                PictureBox p = (PictureBox)sender;
                p.BackColor = colorDlg.Color;
            }

        }



            private void SeleccionaColores_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool sal = true;
            for (int i = 0; i < this.colors.Count; i++) {
                if (this.colors[i].BackColor==Color.White) {
                    sal = false;
                }
            }
            
             this.Close(); 
            
        }
    }
}
