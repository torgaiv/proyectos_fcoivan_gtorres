﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class Form1 : Form
    {
        Random random = new Random();

        int intentos;
        int intentos_posibles;
        Color[] colores = { Color.Red, Color.Blue, Color.Green, Color.Yellow,
                           Color.Brown,Color.Purple,Color.Black,Color.BlueViolet};
        Button Check;
        Constantes c;
        int colores_posibles;

        List<PictureBox> picturcicos;
        List<Color> ordenbueno;
        List<Color> coloresDisponibles;
        List<PictureBox> AddedColors;
        List<Color> ColoresUsables;


        public Form1()
        {
            //Inicialización de variables
            InitializeComponent();
            this.c = new Constantes();
            intentos = 0;
            this.picturcicos = new List<PictureBox>();
            this.ordenbueno = new List<Color>();
            this.coloresDisponibles = new List<Color>();
            this.AddedColors = new List<PictureBox>();
            this.ColoresUsables = new List<Color>();

           

            }

        /**
         * Carga los colores que se usarán en la partida.
         * dependiendo del nivel de dificultad
         * usaremos de entre 4 a 8 colores
         */
        private void CargaColores()
        {
            int coloresPosibles = 0;

            if (c.dificultad == 1)
            {
                coloresPosibles = 4;
                //this.colores_posibles = 4;
                intentos_posibles = 10;
            }
            else if (c.dificultad == 2)
            {
                coloresPosibles = 5;
                this.colores_posibles = 5;
                intentos_posibles = 8;

            }
            else if (c.dificultad == 3)
            {
                coloresPosibles = 6;
                this.colores_posibles = 6;
                intentos_posibles = 6;
            }

            //Consigue los colores SIN repeticion
            while (ColoresUsables.Count < coloresPosibles)
            {
                int rnd = random.Next(1, coloresDisponibles.Count);
                if (ColoresUsables.Contains(coloresDisponibles[rnd])) { }
                else {
                    ColoresUsables.Add(coloresDisponibles[rnd]);
                }
            }
            
        }
        private void activaFoto() {
            this.pictureBox1.Visible = true;
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            pictureBox1.Image = Image.FromFile(path + "\\Recursos\\mmind.png");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            activaFoto();
            for (int i = 0; i < colores.Length; i++)
            {
                this.coloresDisponibles.Add(colores[i]);
            }

        }

        /**
         * Devuelve la posicion que ocupa un color
         * en el array de colores de la partida
         * */
        public int NumeroColor(Color c)
        {
            for (int i = 0; i < ColoresUsables.Count; i++)
            {
                if (ColoresUsables[i] == c)
                {
                    return i;
                }
            }
            return -1;
        }

        void ClickDerechoPicture(ref PictureBox p)
        {
            //Si es blanco lo cambia al color 0
            if (p.BackColor == Color.White)
            {
                p.BackColor = ColoresUsables[0];
            }
            //Si no es blanco comprueba que no se salga del array y lo cambia
            else {
                int n = NumeroColor(p.BackColor);
                if (n+1<ColoresUsables.Count) {
                    p.BackColor = ColoresUsables[n+1];
                }
                else {
                    p.BackColor = ColoresUsables[0];
                }
            }

        }

        void ClickIzquierdoPicture(ref PictureBox p)
        {
            //Si es blanco lo pone a 0
            if (p.BackColor == Color.White)
            {
                p.BackColor = ColoresUsables[0];
            }
            int n = NumeroColor(p.BackColor);
            //Si no es blanco comprueba que no se salga del array y lo cambia

            if (n>0) {
                p.BackColor = ColoresUsables[n-1];
            }
            else
            {
              p.BackColor = ColoresUsables[ColoresUsables.Count-1];
            }

    }
        /**
         * Metodo click en un pictureBox
         * evalua si se ha hecho click izquierdo o derecho
         * */
        private void p_Click(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            MouseEventArgs me = (MouseEventArgs)e;

            if (me.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ClickIzquierdoPicture(ref p);
            }
            if (me.Button == System.Windows.Forms.MouseButtons.Left)
            {
                ClickDerechoPicture(ref p);
            }

        }
       /**
        * Metodo principal, controla la creacion de las primeras casillas
        * la combinacion ganadora y los picture box que se pueden usar
        * */
        private void IniciaPartida(){

            panel2.Visible = true;
            //Crea la combinacion secreta
            for (int i = 0; i< 4; i++) {
               
                int ran =random.Next(0, ColoresUsables.Count);
                Color c = ColoresUsables[ran];
                ordenbueno.Add(c);
                PictureBox p = new PictureBox();
                p.Name = i + intentos.ToString();
                p.BackColor = c;
                p.Height = 30;
                p.Width = 30;
                p.BorderStyle = BorderStyle.FixedSingle;
                p.Location = new Point((10+i) * 25, 25);
                panel3.Controls.Add(p);
            }
            //Crea el panel de los picturebox usables
            for (int i = 0; i < this.ColoresUsables.Count; i++)
            {
                Color c = ColoresUsables[i];
                PictureBox p = new PictureBox();
                p.Name = i + intentos.ToString();
                p.BackColor = c;
                p.Height = 30;
                p.Width = 30;
                p.BorderStyle = BorderStyle.FixedSingle;
                p.Location = new Point(i * 38, 25);
                panel2.Controls.Add(p);
            }


            
            this.label1.Visible = true;
            this.label3.Visible = true;
            this.label5.Visible = true;
            this.label6.Visible = true;

            if (c.dificultad==1) {
                this.label7.Text = "Facil";
                this.label7.ForeColor = Color.Green;
            }
            else if (c.dificultad == 2)
            {
                this.label7.Text = "Medio";
                this.label7.ForeColor = Color.Orange;
            }
            else if (c.dificultad == 3)
            {
                this.label7.Text = "Dificil";
                this.label7.ForeColor = Color.Red;
            }
            this.label7.Visible = true;

            //Crea los primeros picturebox
            for (int i = 0; i < 4; i++)
            {
                PictureBox p = new PictureBox();
                p.Name = i + intentos.ToString();
                p.BackColor = Color.White;
                p.Height = 20;
                p.Width = 20;
                p.BorderStyle = BorderStyle.FixedSingle;
                p.Location = new Point(i * 25, intentos * 25);
                panel1.Controls.Add(p);
                picturcicos.Add(p);
                p.Click += new System.EventHandler(p_Click);

            }
            //Crea el boton de la comprobacion
            Button b = new Button();
            b.Location = new Point(125, intentos * 25);
            panel1.Controls.Add(b);
            b.Text = "Comprobar";
            b.Click += new System.EventHandler(b_Click);
            Check = b;
            Button id = new Button();
            id.Text = (1 + intentos).ToString();
            id.Width = 30;
            id.Location = new Point(330, intentos * 25);
            panel1.Controls.Add(id);
        }

        /**
         * Comprueba cuantas fichas están en la 
         * combinacion final
         * */
        private int CuentaBuenos()
        {
            List<Color> C = new List<Color>();
            int estan = 0;
            for (int i = 0;i<ordenbueno.Count;i++) {
                C.Add(ordenbueno[i]);
            }

            for (int i = 0; i < picturcicos.Count; i++)
            {
                if (C.Contains(picturcicos[i].BackColor))
                {
                    estan++;
                    C.Remove(picturcicos[i].BackColor);
                }
            }
            return estan;
        }
        /**
         * Comprueban cuantas fichas
         * está correctamente ordenadas
         * */
        private int EstanEsSuSitio() {
            int ordenadas = 0;
            for (int i =0;i<picturcicos.Count;i++) {
                if (picturcicos[i].BackColor==ordenbueno[i]) {
                    ordenadas++;
                }
            }
            return ordenadas;
        }
        /**
         * Evento de click del boton comprobar,
         * Comprueba si has ganado o perdido
         * Crea la siguiente fila de fichas y rellena
         * la fila de la comprobacion
         **/
        private void b_Click(object sender, EventArgs e)
        {
            this.label2.Visible = true;
            this.label5.Visible = true;
            int estan =CuentaBuenos();
            int ordenadas = EstanEsSuSitio();

            if (ordenadas == 4)
            {
                MessageBox.Show("Has ganado ! ;D");
                LimpiaMesa();
                this.ajustarColoresToolStripMenuItem.Enabled = true;
                this.activaFoto();

            }

            else
            {
                for (int i = 5; i < 9; i++)
                {
                    PictureBox p = new PictureBox();
                    p.Name = i + intentos.ToString();
                    if (ordenadas > 0)
                    {
                        //Si la ficha está en posición correcta
                        p.BackColor = Color.Red;
                        ordenadas--;
                        estan--;
                    }
                    else if (estan > 0)
                    {
                        //Si la ficha está en otro lugar
                        p.BackColor = Color.Yellow;
                        estan--;
                    }
                    else
                    {
                        p.BackColor = Color.White;

                    }
                    p.Height = 20;
                    p.Width = 20;
                    p.BorderStyle = BorderStyle.FixedSingle;
                    p.Location = new Point(i * 25, intentos * 25);
                    panel1.Controls.Add(p);
                }
                intentos++;
                /** Si los intentos se superan se usa una etiqueta
                 * para evitar crear la siguiente fila de fichas
                 **/
                
                if (intentos >= intentos_posibles)
                {
                    goto compruebaestado;
                }

                foreach (PictureBox p in picturcicos)
                {
                    p.Enabled = false;
                }

                picturcicos.Clear();

                for (int i = 0; i < 4; i++)
                {
                    PictureBox p = new PictureBox();
                    p.Name = i + intentos.ToString();
                    p.BackColor = Color.White;
                    p.Height = 20;
                    p.Width = 20;
                    p.BorderStyle = BorderStyle.FixedSingle;
                    p.Location = new Point(i * 25, intentos * 25);
                    panel1.Controls.Add(p);
                    picturcicos.Add(p);
                    p.Click += new System.EventHandler(p_Click);

                }

                Check.Enabled = false;
                Check.Visible = false;
                Check = null;

                Button b = new Button();
                b.Location = new Point(125, intentos * 25);
                panel1.Controls.Add(b);
                b.Text = "Comprobar";
                b.Click += new System.EventHandler(b_Click);
                Check = b;


                Button id = new Button();
                id.Text = (1 + intentos).ToString();
                //     id.Height = 40;
                id.Width = 30;
                id.Location = new Point(330, intentos * 25);
                panel1.Controls.Add(id);

                compruebaestado:
                CheckEstado(intentos);
            }
        }

        /**
         * Comprueba si has perdido
         * */
        private bool CheckEstado(int intentos)
        {

            if (intentos >= intentos_posibles)
            {
                Check.Visible = false;
                MessageBox.Show("¡Has perdido!");
                this.ajustarColoresToolStripMenuItem.Enabled = true;
                MuestraColores m = new MuestraColores(ordenbueno);
                m.ShowDialog();
                LimpiaMesa();
                this.ajustarColoresToolStripMenuItem.Enabled = true;
                activaFoto();


            }
            return false;

        }
        /**
         * Consigue todos los colores recorriendo los que
         * tenemos por defecto mas los que 
         * puedan haberse añadido
         **/
        private void ConsigueColores() {            
            for (int i = 0;i<AddedColors.Count;i++) {
                if (coloresDisponibles.Contains(AddedColors[i].BackColor)) { }
                else {
                    this.coloresDisponibles.Add(AddedColors[i].BackColor);
                }
                
            }
            AddedColors.Clear();
        }

        /**
         * Partida nueva, se limpia y se inicia
         * */
        private void nuevoJuegoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dificultad d = new Dificultad(ref c);
            d.ShowDialog();
            this.pictureBox1.Visible = false;
            LimpiaMesa();

            ConsigueColores();

            CargaColores();

            IniciaPartida();
        }
        /**
         * Borra toda la parte visual y borra
         * los contenedores
         * */
        private void LimpiaMesa() {
            panel1.Controls.Clear();
            this.intentos = 0;
            this.picturcicos.Clear();
            this.ordenbueno.Clear();
        //    this.coloresDisponibles.Clear();
            this.ColoresUsables.Clear();

            this.ajustarColoresToolStripMenuItem.Enabled = false;
            panel2.Controls.Clear();
            panel3.Controls.Clear();
            label6.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label1.Visible = false;
            label7.Visible = false;
            label3.Visible = false;
            this.panel2.Visible = false;
        }

        private void instruccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Instrucciones i = new Instrucciones();
            i.Show();        
        }
        //Colores del usuario
        private void ajustarColoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<PictureBox> add = new List<PictureBox>();
            for (int i= 0; i<c.dificultad+3;i++) {
                PictureBox p = new PictureBox();
                p.BackColor = Color.White;
                add.Add(p);
            }

            SeleccionaColores s1 = new SeleccionaColores(ref add);
            s1.ShowDialog();

            for (int i= 0; i<add.Count;i++) {
                if (add[i].BackColor!=Color.White) {
                    AddedColors.Add(add[i]);
                }
            }

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void opcionesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sobreNosotrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SobreNosotros n = new SobreNosotros();
            n.Show();
        }
    }
}
