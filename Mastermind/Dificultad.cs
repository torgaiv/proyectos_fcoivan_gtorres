﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class Dificultad : Form
    {
        Constantes c;
        public Dificultad(ref Constantes c)
        {
            InitializeComponent();
            this.c = c;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == false && radioButton2.Checked == false && radioButton3.Checked == false)
            {
                MessageBox.Show("Elige una dificultad");
            }
            else if (radioButton1.Checked == true)
            {
                c.Dificultad = 1;
                this.Close();
                
            }
            else if (radioButton2.Checked == true)
            {
                c.Dificultad = 2;
                this.Close();
            }
            else if (radioButton3.Checked == true)
            {
                c.Dificultad = 3;
                this.Close();
            }
        }

        private void Dificultad_Load(object sender, EventArgs e)
        {

        }
    }
}
