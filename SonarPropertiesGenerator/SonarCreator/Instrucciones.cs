﻿using System;
using System.Windows.Forms;

namespace SonarCreator
{
    /// <summary>
    /// Instrucciones class.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Instrucciones : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Instrucciones"/> class.
        /// </summary>
        public Instrucciones()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            string mss5 = "https://www.microsoft.com/es-es/download/confirmation.aspx?id=28359";
            System.Diagnostics.Process.Start(mss5);
        }

        /// <summary>
        /// Handles the Click event of the button2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            string java = "https://www.java.com/es/download/win10.jsp";
            System.Diagnostics.Process.Start(java);
        }
    }
}
