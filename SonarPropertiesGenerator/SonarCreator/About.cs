﻿using System;
using System.Windows.Forms;

namespace SonarCreator
{
    /// <summary>
    /// About me class.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class About : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="About"/> class.
        /// </summary>
        public About()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the button2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            string linkedin = "https://www.linkedin.com/in/torgaiv";
            System.Diagnostics.Process.Start(linkedin);
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            string linkedin = "https://www.cakaos.es/";
            System.Diagnostics.Process.Start(linkedin);
        }
    }
}
