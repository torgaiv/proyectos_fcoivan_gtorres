﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SonarCreator
{
    /// <summary>
    /// Main class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Main : Form
    {
        #region Properties
        /// <summary>
        /// The nombreproyecto
        /// </summary>
        private string nombreproyecto;

        /// <summary>
        /// The rutaproyecto
        /// </summary>
        private string rutaproyecto;

        /// <summary>
        /// Initializes a new instance of the <see cref="Main"/> class.
        /// </summary>
        #endregion
        #region Main
        public Main()
        {
            InitializeComponent();
        }
        #endregion
        #region Events
        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "SLN Files (*.sln)|*.sln|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        this.textBox1.Text = openFileDialog1.FileName;
                        this.textBox2.Text = openFileDialog1.SafeFileName;
                        this.nombreproyecto = openFileDialog1.SafeFileName;
                        this.rutaproyecto = openFileDialog1.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

        }

        /// <summary>
        /// Handles the Click event of the button2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textBox1.Text) && !string.IsNullOrEmpty(this.textBox2.Text))
            {
                if (this.nombreproyecto.Contains(".sln"))
                {
                    string nombre = this.nombreproyecto.Replace(".sln", "");
                    CreaArchivo(nombre, this.rutaproyecto);
                }
                else
                {
                    MessageBox.Show("El archivo no tiene el formato esperado. (.sln)");
                }
            }
            else
            {
                MessageBox.Show("No se ha encontrado el proyecto.");
            }
        }
        /// <summary>
        /// Handles the Click event of the cerrarToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the sobreMíToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void sobreMíToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.Show();
        }

        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the Click event of the instruccionesToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void instruccionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Instrucciones I = new Instrucciones();
            I.Show();
        }
        #endregion
        #region Methods
        /// <summary>
        /// Creas the archivo.
        /// </summary>
        /// <param name="nombre">The nombre.</param>
        /// <param name="rutaproyecto">The rutaproyecto.</param>
        /// <returns></returns>
        private bool CreaArchivo(string nombre, string rutaproyecto)
        {
            string path = rutaproyecto.Replace(this.nombreproyecto, "");
            string id = nombre.Replace(" ", "");
            path = path + "sonar-project.properties";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("#----- Default source code encoding" + "\n" +
                        "sonar.sourceEncoding=UTF-8" + "\n" + "\n" +

                        "# Project identification" + "\n" +
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.projectKey=sonar:" + id + "" + DateTime.Now.Day.ToString() + "\n" +
                        "sonar.projectVersion=1.0" + "\n" +
                        "sonar.projectName=" + nombre + "" + "\n" + "\n" +

                        "# Info required for Sonar " + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.sources=." + "\n" +
                        "sonar.language=cs" + "\n" + "\n" +

                        "#Core C# Settings" + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.dotnet.visualstudio.solution.file=" + this.nombreproyecto + "" + "\n" +
                        "sonar.silverlight.4.mscorlib.location=C:/Program Files (x86)/Reference Assemblies/Microsoft/Framework/Silverlight/v5.0" + "\n" +
                        "sonar.dotnet.excludeGeneratedCode=true" + "\n" +
                        "sonar.dotnet.4.0.sdk.directory=C:/Windows/Microsoft.NET/Framework/v4.0.30319" + "\n" +
                        "sonar.dotnet.version=4.5" + "\n" + "\n" +

                        "#Gendarme" + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.gendarme.mode=skip" + "\n" + "\n" +

                        "# FXCop " + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.fxcop.mode=skip" + "\n" +
                        "sonar.fxcop.installDirectory=C:/Program Files/Microsoft Fxcop 10.0/" + "\n" + "\n" +

                        "# StyleCop " + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.stylecop.mode=" + "\n" + "\n" +

                        "# NDeps" + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "sonar.ndeps.mode=skip" + "\n" + "\n" +

                        "# Gallio" + "\n" +
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        "#sonar.gallio.mode=" + "\n" +
                        "#sonar.gallio.coverage.tool=OpenCover" + "\n" +
                        "#sonar.gallio.runner=IsolatedProcess" + "\n" +
                        "#sonar.dotnet.visualstudio.testProjectPattern=*UnitTest*;*IntegrationTest*" + "\n" +
                        "#sonar.opencover.installDirectory=C:/Program Files (x86)/OpenCover/");
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (CreaBat(rutaproyecto))
                    {
                        MessageBox.Show("Para utilizar Sonar requiere que esté instalado en la raíz (C:\\Sonar\\)");
                    }
                    return false;
                }
            }
            else
            {
                MessageBox.Show("El archivo .properties ya existe");
                return false;
            }
        }

        /// <summary>
        /// Creates the launch bat file.
        /// </summary>
        /// <param name="ruta">The ruta.</param>
        /// <returns></returns>
        private bool CreaBat(string ruta)
        {
            string path = rutaproyecto.Replace(this.nombreproyecto, "");
            path = path + "Sonar 02 - Ejecutar Sonar.bat";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(@"C:\Sonar\sonar-runner\bin\sonar-runner.bat > sonar.log");
                    CreaServer(ruta);
                    return true;
                }
            }
            else if (File.Exists(path))
            {
                MessageBox.Show("Ya existe el lanzador");
                CreaServer(ruta);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Creas the server shortcut.
        /// </summary>
        /// <param name="ruta">The ruta.</param>
        /// <returns></returns>
        private bool CreaServer(string ruta)
        {
            string path = rutaproyecto.Replace(this.nombreproyecto, "");
            path = path + "Sonar 01 - Arrancar servidor";
            string deskDir = @"C:\Sonar\sonar-server\bin\windows-x86-64\StartSonar.bat";
            try
            {
                using (StreamWriter writer = new StreamWriter(path + ".url"))
                {
                    string app = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    writer.WriteLine("[InternetShortcut]");
                    writer.WriteLine("URL=file:///" + deskDir);
                    writer.WriteLine("IconIndex=0");
                    string icon = app.Replace('\\', '/');
                    writer.WriteLine("IconFile=" + icon);
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"No se ha encontrado el StartSonar.bat en C:\Sonar\sonar-server\bin\windows-x86-64\", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }
        #endregion
    }
}
