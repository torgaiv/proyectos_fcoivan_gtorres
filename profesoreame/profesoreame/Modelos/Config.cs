﻿namespace profesoreame
{
    public class Config
    {
        public string smtpClient { get; set; }
        public string yourEmail { get; set; }
        public string yourPassword { get; set; }
        public string yourName { get; set; }
        public string Subject { get; set; }
    }
}
