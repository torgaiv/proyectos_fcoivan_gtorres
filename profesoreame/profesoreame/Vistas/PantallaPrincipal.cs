﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Threading;
using System.Windows.Forms;

namespace profesoreame
{
    public partial class PantalaPrincipal : Form
    {
        private List<Alumno> alumnos { get; set; }

        public PantalaPrincipal()
        {
            InitializeComponent();
        }

        private void btLoadFile_Click(object sender, EventArgs e)
        {
            this.btSendEmail.Enabled = false;
            this.textConsola.Text = string.Empty;
            this.textConsola.Text += "\n------------------------------------------------------------------------------------------------------------------------------------------------------------";
            this.textConsola.Text += "\n Operacion carga de datos";
            this.textConsola.Text += "\n Fecha de la operacion: " + DateTime.Now.ToString();
            this.textConsola.Text += "\n Id de la operacion : " + Guid.NewGuid().ToString();
            this.textConsola.Text += "\n------------------------------------------------------------------------------------------------------------------------------------------------------------";
            this.textConsola.Text += "\n Buscando archivo...";

            OpenFileDialog browser = new OpenFileDialog();
            browser.Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";


            if (browser.ShowDialog() == DialogResult.OK)
            {
                this.textConsola.Text += "\n Abriendo archivo...";
                this.textFilePathBox.Text = browser.FileName;


                this.textConsola.Text += "\n Archivo encontrado: " + browser.FileName;

                using (var stream = File.Open(browser.FileName, FileMode.Open, FileAccess.Read))
                {

                    this.textConsola.Text += "\n Lectura correcta";

                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        List<Alumno> alumnos = new List<Alumno>();

                        this.textConsola.Text += "\n Cargando datos...";
                        do
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    Alumno alumno = new Alumno();
                                    alumno.correo = reader[0]?.ToString();
                                    alumno.nombre = reader[1]?.ToString();
                                    alumno.nota = reader[2]?.ToString();
                                    alumnos.Add(alumno);
                                }
                                catch (Exception ex)
                                {
                                    this.textConsola.Text += "\n ERROR:  " + ex.Message;
                                }
                            }
                        } while (reader.NextResult());

                        this.alumnos = alumnos;

                        this.textConsola.Text += "\n Carga finalizada. Se han cargado [" + alumnos.Count + "] alumnos";

                        if (alumnos.Count > 0)
                        {
                            this.btSendEmail.Enabled = true;
                            var list = new BindingList<Alumno>(alumnos);
                            dataGridView1.DataSource = list;
                        }
                    }
                }
            }
            else
            {
                this.textConsola.Text += "\n Operación cancelada";
            }

            writeToLog();
        }

        private void btSendEmail_Click(object sender, EventArgs e)
        {
            this.textConsola.Text += "\n Operación envio de datos";

            if (!(this.alumnos is null) && this.alumnos.Count > 0)
            {
                this.progressBar1.Maximum = this.alumnos.Count;
                int totalAlumnos = this.alumnos.Count;
                this.textConsola.Text += "\n Se va a enviar la nota a " + totalAlumnos + " personas";

                int contador = 1;
                int alumnosMax = totalAlumnos + 1;
                Config config = loadConfig();

                if (!(config is null))
                {
                    foreach (Alumno alumno in alumnos)
                    {
                        this.textConsola.Text += "\n Enviando a (" + contador + "/" + alumnosMax + ")";

                        string message = this.textMensajeBody.Text;
                        message = message.Replace("@nombre@", alumno.nombre);
                        message = message.Replace("@nota@", alumno.nota);

                        enviarMensaje(config, message, alumno.correo);

                        Thread.Sleep(3000);

                        this.progressBar1.Value++;

                        this.textConsola.Text += "\n Envidado correctamente a " + alumno.correo + " (" + alumno.nombre + ") con nota : (" + alumno.nota + ")";
                    }

                    this.progressBar1.Value = this.progressBar1.Maximum;
                }
                else
                {
                    this.textConsola.Text += "\n ERROR : Configuracion erronea";
                }
            }
            else
            {
                this.textConsola.Text += "\n ERROR : No hay datos que enviar";
            }

            writeToLog();
        }

        private void writeToLog()
        {
            string logFile = Directory.GetCurrentDirectory() + "\\logs.txt";
            if (File.Exists(logFile))
            {
                using (var tw = new StreamWriter(logFile, true))
                {
                    tw.WriteLine(textConsola.Text.Replace("\n", Environment.NewLine));
                }
            }
            else
            {
                File.WriteAllText(logFile, textConsola.Text.Replace("\n", Environment.NewLine));
            }
        }

        private void enviarMensaje(Config config, string mensaje, string email)
        {


            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(config.smtpClient);

            mail.From = new MailAddress(config.yourEmail);
            mail.To.Add(email);
            mail.Subject = "Resultado examen";
            mail.Body = mensaje;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(config.yourName, config.yourPassword);
            SmtpServer.EnableSsl = true;

            // SmtpServer.Send(mail);

        }

        private Config loadConfig()
        {
            Config c = new Config();
            try
            {
                c.smtpClient = Properties.Settings.Default["smtpClient"].ToString();
                c.yourEmail = Properties.Settings.Default["yourEmail"].ToString();
                c.yourName = Properties.Settings.Default["yourName"].ToString();
                c.yourPassword = Properties.Settings.Default["yourPassword"].ToString();
                c.Subject = Properties.Settings.Default["Subject"].ToString();
            }
            catch (Exception ex)
            {
                this.textConsola.Text += "\n ERROR : No se ha podido cargar la configuración: " + ex.Message;
            }

            return c;

        }

    }
}
