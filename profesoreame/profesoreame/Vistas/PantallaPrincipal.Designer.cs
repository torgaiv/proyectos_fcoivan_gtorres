﻿namespace profesoreame
{
    partial class PantalaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btLoadFile = new System.Windows.Forms.Button();
            this.textFilePathBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textMensajeBody = new System.Windows.Forms.RichTextBox();
            this.btSendEmail = new System.Windows.Forms.Button();
            this.textConsola = new System.Windows.Forms.RichTextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btLoadFile
            // 
            this.btLoadFile.Location = new System.Drawing.Point(594, 7);
            this.btLoadFile.Name = "btLoadFile";
            this.btLoadFile.Size = new System.Drawing.Size(121, 23);
            this.btLoadFile.TabIndex = 0;
            this.btLoadFile.Text = "Cargar archivo";
            this.btLoadFile.UseVisualStyleBackColor = true;
            this.btLoadFile.Click += new System.EventHandler(this.btLoadFile_Click);
            // 
            // textFilePathBox
            // 
            this.textFilePathBox.Location = new System.Drawing.Point(12, 9);
            this.textFilePathBox.Name = "textFilePathBox";
            this.textFilePathBox.Size = new System.Drawing.Size(576, 20);
            this.textFilePathBox.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(703, 172);
            this.dataGridView1.TabIndex = 2;
            // 
            // textMensajeBody
            // 
            this.textMensajeBody.Location = new System.Drawing.Point(12, 213);
            this.textMensajeBody.Name = "textMensajeBody";
            this.textMensajeBody.Size = new System.Drawing.Size(703, 136);
            this.textMensajeBody.TabIndex = 3;
            this.textMensajeBody.Text = "Hola @nombre@,\nTu nota en el examen ha sido @nota@.\nSalu2\n";
            // 
            // btSendEmail
            // 
            this.btSendEmail.Enabled = false;
            this.btSendEmail.Location = new System.Drawing.Point(323, 354);
            this.btSendEmail.Name = "btSendEmail";
            this.btSendEmail.Size = new System.Drawing.Size(75, 23);
            this.btSendEmail.TabIndex = 4;
            this.btSendEmail.Text = "Enviar";
            this.btSendEmail.UseVisualStyleBackColor = true;
            this.btSendEmail.Click += new System.EventHandler(this.btSendEmail_Click);
            // 
            // textConsola
            // 
            this.textConsola.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.textConsola.ForeColor = System.Drawing.Color.Lime;
            this.textConsola.Location = new System.Drawing.Point(12, 400);
            this.textConsola.Name = "textConsola";
            this.textConsola.ReadOnly = true;
            this.textConsola.Size = new System.Drawing.Size(703, 219);
            this.textConsola.TabIndex = 5;
            this.textConsola.Text = "";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 383);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(703, 11);
            this.progressBar1.TabIndex = 6;
            // 
            // PantalaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 626);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.textConsola);
            this.Controls.Add(this.btSendEmail);
            this.Controls.Add(this.textMensajeBody);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textFilePathBox);
            this.Controls.Add(this.btLoadFile);
            this.Name = "PantalaPrincipal";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btLoadFile;
        private System.Windows.Forms.TextBox textFilePathBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RichTextBox textMensajeBody;
        private System.Windows.Forms.Button btSendEmail;
        private System.Windows.Forms.RichTextBox textConsola;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

