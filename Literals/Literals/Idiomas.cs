﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Literals
{
    public enum Idiomas
    {
        Ingles=3,
        Americano=57,
        Frances=5,
        Español =1,
        Mexicano=4,
        Colombiano=25,
        Peruano=26,
        Portugues=2,
        Catalan=6,
        Italiano=12,
		Chileno=9
    }
}