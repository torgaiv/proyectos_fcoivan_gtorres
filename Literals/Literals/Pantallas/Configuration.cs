﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Literals.Negocio;

namespace Literals
{
    public partial class Configuration : Form
    {
        /// <summary>
        /// The configuration file path
        /// </summary>
        private string configFilePath = Utils.getConfigPath();

        /// <summary>
        /// The c local data
        /// </summary>
        private ConfigurationData cLocalData = new ConfigurationData();

        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            InitializeComponent();

            try
            { 
                string contents = File.ReadAllText(cLocalData.configPath + "configData");

                cLocalData = JsonConvert.DeserializeObject<ConfigurationData>(contents);
                if (cLocalData.ConfigData.ContainsKey(ConfigurationManager.AppSettings["HeaderInfoKey"] ))
                {
                    cLocalData.HeaderInfoValue = cLocalData.ConfigData[ConfigurationManager.AppSettings["HeaderInfoKey"]];
                }
                if (cLocalData.ConfigData.ContainsKey(ConfigurationManager.AppSettings["ActualDateInfoKey"]))
                {
                cLocalData.ActualDateInfoValue = cLocalData.ConfigData[ConfigurationManager.AppSettings["ActualDateInfoKey"]];
                }

                this.richTextBox1.Text = cLocalData.HeaderInfoValue;

                if (cLocalData.ActualDateInfoValue == "1")
                {
                    this.cbDate.Checked = true;
                }

                if (!string.IsNullOrEmpty(cLocalData.SQLData.BaseDeDatos))
                {
                    this.txtBBDD.Text = cLocalData.SQLData.BaseDeDatos;
                }
                if (!string.IsNullOrEmpty(cLocalData.SQLData.IdUsuario))
                {
                    this.txtUsername.Text = cLocalData.SQLData.IdUsuario;
                }
                if (!string.IsNullOrEmpty(cLocalData.SQLData.PassUsuario))
                {
                    this.txtUserPassword.Text = Utils.Decrypt(cLocalData.SQLData.PassUsuario);
                }
                if (!string.IsNullOrEmpty(cLocalData.SQLData.Servidor))
                {
                    this.txtServer.Text = cLocalData.SQLData.Servidor;
                }
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigurationData configData = new ConfigurationData();

                if (!string.IsNullOrEmpty(this.richTextBox1.Text))
                {
                    configData.ConfigData.Add(ConfigurationManager.AppSettings["HeaderInfoKey"], this.richTextBox1.Text);
                }
                if (this.cbDate.Checked)
                {
                    configData.ConfigData.Add(ConfigurationManager.AppSettings["ActualDateInfoKey"], "1");
                }
                configData.SQLData.BaseDeDatos=this.txtBBDD.Text;
                configData.SQLData.IdUsuario = this.txtUsername.Text;
                configData.SQLData.Servidor = this.txtServer.Text;
                if (!string.IsNullOrEmpty(this.txtUserPassword.Text))
                {
                    configData.SQLData.PassUsuario = Utils.Encrypt(this.txtUserPassword.Text);
                }
                configData.ConfigCustomLanguages = this.cLocalData.ConfigCustomLanguages;
                string jsonItem = JsonConvert.SerializeObject(configData, Formatting.Indented);
                System.IO.File.WriteAllText(configData.configPath+"configData", jsonItem);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error. Mensaje original: " + ex.Message);
            }           
        }

        private void Configuration_Load(object sender, EventArgs e)
        {

        }
    }
}
