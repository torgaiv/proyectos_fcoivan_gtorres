﻿namespace Literals.Pantallas
{
    partial class FastLoadLanguages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSave = new System.Windows.Forms.Button();
            this.cbPersoName = new System.Windows.Forms.ComboBox();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btAdd = new System.Windows.Forms.Button();
            this.lbIdioma = new System.Windows.Forms.Label();
            this.cbCargar = new System.Windows.Forms.Button();
            this.btNew = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(322, 305);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 0;
            this.btSave.Text = "Guardar";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // cbPersoName
            // 
            this.cbPersoName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPersoName.FormattingEnabled = true;
            this.cbPersoName.Location = new System.Drawing.Point(130, 10);
            this.cbPersoName.Name = "cbPersoName";
            this.cbPersoName.Size = new System.Drawing.Size(386, 21);
            this.cbPersoName.TabIndex = 1;
            this.cbPersoName.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cbLanguage
            // 
            this.cbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Location = new System.Drawing.Point(522, 141);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(183, 21);
            this.cbLanguage.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 260);
            this.panel1.TabIndex = 3;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(522, 168);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(183, 23);
            this.btAdd.TabIndex = 4;
            this.btAdd.Text = "Añadir";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // lbIdioma
            // 
            this.lbIdioma.AutoSize = true;
            this.lbIdioma.Location = new System.Drawing.Point(9, 13);
            this.lbIdioma.Name = "lbIdioma";
            this.lbIdioma.Size = new System.Drawing.Size(111, 13);
            this.lbIdioma.TabIndex = 5;
            this.lbIdioma.Text = "Nombre del conjunto: ";
            // 
            // cbCargar
            // 
            this.cbCargar.Location = new System.Drawing.Point(524, 8);
            this.cbCargar.Name = "cbCargar";
            this.cbCargar.Size = new System.Drawing.Size(183, 23);
            this.cbCargar.TabIndex = 6;
            this.cbCargar.Text = "Cargar";
            this.cbCargar.UseVisualStyleBackColor = true;
            this.cbCargar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btNew
            // 
            this.btNew.Location = new System.Drawing.Point(524, 39);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(183, 23);
            this.btNew.TabIndex = 7;
            this.btNew.Text = "Nuevo conjunto";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(524, 68);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(183, 23);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Eliminar";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // FastLoadLanguages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 332);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.cbCargar);
            this.Controls.Add(this.lbIdioma);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cbLanguage);
            this.Controls.Add(this.cbPersoName);
            this.Controls.Add(this.btSave);
            this.Name = "FastLoadLanguages";
            this.Text = "FastLoadLanguages";
            this.Load += new System.EventHandler(this.FastLoadLanguages_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.ComboBox cbPersoName;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Label lbIdioma;
        private System.Windows.Forms.Button cbCargar;
        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.Button btDelete;
    }
}