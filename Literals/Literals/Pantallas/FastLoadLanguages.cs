﻿using Literals.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Literals.Pantallas
{
    public partial class FastLoadLanguages : Form
    {

        /// <summary>
        /// The main configuration data
        /// </summary>
        private ConfigurationData MainConfigData = new ConfigurationData();
        /// <summary>
        /// The loc idiomas
        /// </summary>
        private List<Idioma> locIdiomas = new List<Idioma>();

        /// <summary>
        /// The loc idiomas perso
        /// </summary>
        IdiomasPerso locIdiomasPerso = new IdiomasPerso();

        public FastLoadLanguages(ref IdiomasPerso argConfigCustomLanguages)
        {

            try
            {
                this.locIdiomasPerso = argConfigCustomLanguages;

                ConfigurationData cData = new ConfigurationData();
                string contents = File.ReadAllText(cData.configPath + "configData");
                cData = JsonConvert.DeserializeObject<ConfigurationData>(contents);

                if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["HeaderInfoKey"]).Value != null)
                {
                    cData.HeaderInfoValue = cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["HeaderInfoKey"]).Value;
                }
                if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["ActualDateInfoKey"]).Value != null)
                {
                    if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["ActualDateInfoKey"]).Value == "1")
                    {
                        cData.ActualDateInfoValue = "1";
                    }
                }

                if (cData.ConfigCustomLanguages.ConfigIdiomasPerso.Count() < 0)
                {
                    foreach (KeyValuePair<string, List<Idioma>> IP in cData.ConfigCustomLanguages.ConfigIdiomasPerso)
                    {
                        this.cbPersoName.Items.Add(IP.Key);
                    }
                }

                this.MainConfigData = cData;
            }
            catch (Exception ex)
            {

            }
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                OrdenaBotones();
            }
            catch (Exception) { };
        }

        /// <summary>
        /// Handles the Load event of the FastLoadLanguages control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FastLoadLanguages_Load(object sender, System.EventArgs e)
        {
            string servidor = string.Empty;
            string databaseName = string.Empty;
            string user = string.Empty;
            string pass = string.Empty;

            Dictionary<string, string> dcFuc = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(this.MainConfigData.SQLData.Servidor))
            {
                servidor = "yourdefaultserver";
            }
            else
            {
                servidor = this.MainConfigData.SQLData.Servidor;
            }
            if (string.IsNullOrEmpty(this.MainConfigData.SQLData.BaseDeDatos))
            {
                databaseName = "defaultdatabase";
            }
            else
            {
                databaseName = this.MainConfigData.SQLData.BaseDeDatos;
            }
            if (string.IsNullOrEmpty(this.MainConfigData.SQLData.IdUsuario) ||
                string.IsNullOrEmpty(this.MainConfigData.SQLData.PassUsuario))
            {
                FastUserCredentials FUC = new FastUserCredentials(ref dcFuc);
                FUC.ShowDialog();
                if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "user").ToString()))
                {
                    user = dcFuc.SingleOrDefault(x => x.Key == "user").Value;
                }
                else
                {
                    Exception ex = new Exception("Usuario o contraseña incorrecta");
                    throw ex;
                }
                if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "pass").ToString()))
                {
                    pass = dcFuc.SingleOrDefault(x => x.Key == "pass").Value;
                }
                else
                {
                    Exception ex = new Exception("Usuario o contraseña incorrecta");
                    throw ex;
                }
            }
            else
            {
                user = this.MainConfigData.SQLData.IdUsuario;
                pass = Utils.Decrypt(this.MainConfigData.SQLData.PassUsuario);
            }
            SqlConnection conn = new SqlConnection(
                        "Data Source=" + servidor + ";" +
                        "Initial Catalog=" + databaseName + ";" +
                        "Persist Security Info=True;" +
                        "User Id=" + user + ";" +
                        "Password=" + pass + ";");

            conn.Open();

            string query = "SELECT CODIGO,DESCRIPCION,FLOCALE FROM IDIOMAS";
            SqlCommand cmd = new SqlCommand(query, conn);

            DataTable valuesData = new DataTable();
            valuesData.Load(cmd.ExecuteReader());


            conn.Close();

            foreach (var dr in valuesData.Rows)
            {
                DataRow dt = (DataRow)dr;
                Idioma locIdioma = new Idioma();

                if (dt.ItemArray[0] != null &&
                    !string.IsNullOrEmpty(dt.ItemArray[0].ToString())
                    )
                {
                    locIdioma.Codigo = dt.ItemArray[0].ToString();

                    if (dt.ItemArray[1] != null &&
                        !string.IsNullOrEmpty(dt.ItemArray[1].ToString())
                    )
                    {
                        locIdioma.Descripcion = dt.ItemArray[1].ToString();

                        if (dt.ItemArray[2] != null &&
                            !string.IsNullOrEmpty(dt.ItemArray[2].ToString())
                        )
                        {
                            locIdioma.Culture = dt.ItemArray[2].ToString();
                        }
                        else
                        {
                            locIdioma.Culture = "unknown";
                        }
                        this.locIdiomas.Add(locIdioma);
                    }
                }
            }

            this.RellenaConfigIdiomasPerso();
            this.cbLanguage.DataSource = new BindingSource(locIdiomas, null);
            this.cbLanguage.DisplayMember = "Descripcion";
            this.cbLanguage.ValueMember = "Codigo";
        }


        /// <summary>
        /// Rellenas the configuration idiomas perso.
        /// </summary>
        private void RellenaConfigIdiomasPerso()
        {
            this.cbPersoName.Items.Clear();
            this.cbPersoName.Items.Add(string.Empty);
            foreach (KeyValuePair<string, List<Idioma>> kvpair in this.locIdiomasPerso.ConfigIdiomasPerso)
            {
                this.cbPersoName.Items.Add(kvpair.Key);
            }
            this.cbPersoName.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrdenaBotones();
        }

        /// <summary>
        /// Ordenas the botones.
        /// </summary>
        private void OrdenaBotones()
        {
            //Coordenadas para posicionar los botones
            int x = 0;
            int y = 0;
            this.panel1.Controls.Clear();

            KeyValuePair<string, List<Idioma>> TempLanguage
                = locIdiomasPerso.ConfigIdiomasPerso.Single(X => X.Key == this.cbPersoName.Text);

            foreach (Idioma i in TempLanguage.Value)
            {
                Button B = new Button();
                //if (i.Descripcion.Count() > 10)
                //{
                //B.Text = i.Descripcion.Substring(0,11).ToString();
                //}
                //else
                //{
                B.Text = i.Descripcion.ToString();
                //}
                B.Name = i.Codigo.ToString();
                //B.AutoSize = true;
                B.Width = 120;

                int totalControles = this.panel1.Controls.Count;

                if (x == 4)
                {
                    y++;
                    x = 0;
                }

                B.Left = x * 128;
                x++;
                B.Top = y * 23;
                B.Click += new EventHandler(BotonB_Click);
                this.panel1.Controls.Add(B);
                //   rellenaCB();
            }
        }

        /// <summary>
        /// Handles the Click event of the BotonB control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonB_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            Idioma valor = locIdiomas.SingleOrDefault(Y => Y.Codigo == b.Name);
            locIdiomasPerso.ConfigIdiomasPerso.Single(X => X.Key == this.cbPersoName.Text).Value.Remove(valor);
            this.OrdenaBotones();
        }

        /// <summary>
        /// Handles the Click event of the btNew control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btNew_Click(object sender, EventArgs e)
        {
            List<string> newConjunto = new List<string>();
            TextBoxDialog txBD = new TextBoxDialog(ref newConjunto);
            txBD.ShowDialog();
            if (!string.IsNullOrEmpty(newConjunto[0]))
            {
                string name = newConjunto[0];
                if (!this.locIdiomasPerso.ConfigIdiomasPerso.Any(x => x.Key == name))
                {
                    List<Idioma> ConfigIdiomasPerso = new List<Idioma>();
                    this.locIdiomasPerso.ConfigIdiomasPerso.Add(name, ConfigIdiomasPerso);
                    this.cbPersoName.DataSource = new BindingSource(locIdiomasPerso.ConfigIdiomasPerso, null);
                    this.cbPersoName.DisplayMember = "Key";
                    this.cbPersoName.ValueMember = "Key";
                    this.cbPersoName.SelectedIndex = this.cbPersoName.Items.Count - 1;
                }
                else
                {
                    MessageBox.Show("Ya existe una colección con ese nombre.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Se borrarrá el conjunto actual. ¿Continuar?", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.Yes)
                {
                    this.locIdiomasPerso.ConfigIdiomasPerso.Remove(cbPersoName.Text);
                    if (locIdiomasPerso.ConfigIdiomasPerso.Count > 0)
                    {
                        this.cbPersoName.DataSource = new BindingSource(locIdiomasPerso.ConfigIdiomasPerso, null);
                        this.cbPersoName.DisplayMember = "Key";
                        this.cbPersoName.ValueMember = "Key";
                        this.cbPersoName.SelectedIndex = this.cbPersoName.Items.Count - 1;
                    }
                    else
                    {
                        this.cbPersoName.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Idioma idioma = this.locIdiomas.SingleOrDefault(Y => Y.Codigo == this.cbLanguage.SelectedValue.ToString());
                if (!this.locIdiomasPerso.ConfigIdiomasPerso.SingleOrDefault(X => X.Key == this.cbPersoName.Text).Value.Any(P=>P.Codigo==idioma.Codigo))
                {
                    locIdiomasPerso.ConfigIdiomasPerso.Single(X => X.Key == this.cbPersoName.Text).Value.Add(idioma);
                    this.OrdenaBotones();
                }
            }
            catch (Exception ex) { }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
