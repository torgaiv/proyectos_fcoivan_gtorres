﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Literals
{
    /// <summary>
    /// FastUserCredentials
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class FastUserCredentials : Form
    {
        Dictionary<string, string> fuc;
        public  FastUserCredentials(ref Dictionary<string, string>  f)
        {
            InitializeComponent();
            this.fuc = f;
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textBox1.Text) && !string.IsNullOrWhiteSpace(this.textBox1.Text))
            {
                if (!string.IsNullOrEmpty(this.textBox2.Text) && !string.IsNullOrWhiteSpace(this.textBox2.Text))
                {
                    this.fuc.Add("user", this.textBox1.Text);
                    this.fuc.Add("pass", this.textBox2.Text);
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("La contraseña es incorrecta");
                }
            }
            else
            {
                MessageBox.Show("El usuario es incorrecto");
            }

        }

        private void FastUserCredentials_Load(object sender, EventArgs e)
        {

        }
    }
}
