﻿using GemBox.Spreadsheet;
using Literals.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Literals
{
    /// <summary>
    /// Main thread
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainMenu : Form
    {
        /// <summary>
        /// The l botones control
        /// </summary>
        private List<Idiomas> lBotones = new List<Idiomas>();

        /// <summary>
        /// The l botones control
        /// </summary>
        private List<Idioma> lNewBotones = new List<Idioma>();

        /// <summary>
        /// The loc idiomas
        /// </summary>
        private List<Idioma> locIdiomas = new List<Idioma>();

        /// <summary>
        /// The main configuration data
        /// </summary>
        private ConfigurationData MainConfigData = new ConfigurationData();


        /// <summary>
        /// The loc idiomas perso
        /// </summary>
        IdiomasPerso locIdiomasPerso = new IdiomasPerso();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            InitializeComponent();
            //rellenaCB();
            LoadConfigData();
            this.LoadCbLanguagues();
        }

        /// <summary>
        /// Loads the configuration data.
        /// </summary>
        private void LoadConfigData()
        {
            try
            {
                ConfigurationData cData = new ConfigurationData();
                string contents = File.ReadAllText(cData.configPath + "configData");
                cData = JsonConvert.DeserializeObject<ConfigurationData>(contents);

                if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["HeaderInfoKey"]).Value != null)
                {
                    cData.HeaderInfoValue = cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["HeaderInfoKey"]).Value;
                }
                if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["ActualDateInfoKey"]).Value != null)
                {
                    if (cData.ConfigData.SingleOrDefault(x => x.Key == ConfigurationManager.AppSettings["ActualDateInfoKey"]).Value == "1")
                    {
                        cData.ActualDateInfoValue = "1";
                    }
                }

                if (cData.ConfigCustomLanguages.ConfigIdiomasPerso.Count() > 0)
                {
                    this.locIdiomasPerso.ConfigIdiomasPerso = cData.ConfigCustomLanguages.ConfigIdiomasPerso;
                    RellenaConfigIdiomasPerso();
                }
                else
                {

                }

                this.MainConfigData = cData;
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Rellenas the configuration idiomas perso.
        /// </summary>
        private void RellenaConfigIdiomasPerso()
        {
            this.cbDefault.Items.Clear();
            this.cbDefault.Items.Add(string.Empty);
            foreach (KeyValuePair<string, List<Idioma>> kvpair in this.locIdiomasPerso.ConfigIdiomasPerso)
            {
                this.cbDefault.Items.Add(kvpair.Key);
            }
            this.cbDefault.SelectedIndex = 0;
        }

        /// <summary>
        /// Ordenas the botones.
        /// </summary>
        private void OrdenaBotones()
        {
            //Coordenadas para posicionar los botones
            int x = 0;
            int y = 0;
            this.panel1.Controls.Clear();

            foreach (Idioma i in lNewBotones.OrderBy(o => o.Codigo))
            {
                Button B = new Button();
                B.Text = i.Descripcion.ToString();
                B.Name = i.Codigo.ToString();
                B.Width = 120;

                int totalControles = this.panel1.Controls.Count;

                if (totalControles == 6)
                {
                    y++;
                    x = 0;
                }

                B.Left = x * 121;
                x++;
                B.Top = y * 23;
                B.Click += new EventHandler(BotonB_Click);
                this.panel1.Controls.Add(B);
            }
        }

        /// <summary>
        /// Manages the date and extra data.
        /// </summary>
        /// <returns></returns>
        private string ManageData()
        {
            string values = string.Empty;

            if (!string.IsNullOrEmpty(MainConfigData.HeaderInfoValue))
            {
                values += MainConfigData.HeaderInfoValue;
            }
            if (!string.IsNullOrEmpty(MainConfigData.ActualDateInfoValue))
            {
                if (values[values.Length - 1] != '\n')
                {
                    values += "\n";
                }
                if (MainConfigData.ActualDateInfoValue == "1")
                {
                    values += "--DATE OF CREATION: " + DateTime.Now.ToShortDateString();
                    values += "\n";
                }
            }

            return values;
        }

        /// <summary>
        /// Gets the translate queries
        /// </summary>
        /// <returns></returns>
        private string UpdateTranslateQuery()
        {
            string litName = this.txtNombreTabla.Text.ToUpper();
            txtNombreTabla.Text = litName;
            string query = string.Empty;

            string translatedString = string.Empty;

            try
            {
                this.progressBar1.Maximum = lNewBotones.Count() + 1;
                this.progressBar1.Value = this.progressBar1.Minimum;

                foreach (Idioma i in lNewBotones)
                {
                    string concatenatedValues = string.Empty;

                    List<string> listStrLineElements = new List<string>();
                    string screen = string.Empty;

                    if (cbNoTraducir.Checked || Utils.EsCastellano(i))
                    {
                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            concatenatedValues += this.dgvMainItems.Rows[j].Cells[1].Value.ToString() + "[]";
                        }

                        translatedString = concatenatedValues;
                        listStrLineElements = translatedString.Split(new string[] { "[]" }, StringSplitOptions.None).ToList();

                        query += "\n --- " + i.Descripcion.ToString() + " \n";
                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[0].Value.ToString()))
                            {
                                string translatedValue = string.Empty;
                                if (listStrLineElements.Count > j)
                                {
                                    translatedValue = listStrLineElements[j];
                                    if (translatedValue.Length > 0)
                                    {
                                        if (translatedValue[0] == ' ')
                                        {
                                            translatedValue = translatedValue.TrimStart();
                                        }
                                    }
                                }

                                query += "IF COL_LENGTH('" + this.txtNombreTabla.Text + "','" + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() + "') IS NOT NULL \n" +
                                    " BEGIN\n";
                                query += "  UPDATE dbo." + this.txtNombreTabla.Text + " SET " + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() +
                                    " = '" + translatedValue.Replace("'", "''").TrimEnd().TrimStart() + "' WHERE IDIOMA = '" + i.Codigo.ToString() + "'\n";
                                query += " END\n";
                            }
                        }
                    }
                    else
                    {
                        string ParValues = string.Empty;

                        int TotalRows = 0;

                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            concatenatedValues += this.dgvMainItems.Rows[j].Cells[1].Value.ToString() + "[]";
                            TotalRows++;
                        }

                        string languageCode = Utils.CodigoIdioma(i);

                        List<string> tempListString = concatenatedValues.Split(new string[] { "[]" }, StringSplitOptions.None).ToList();

                        if (cbSecureMode.Checked)
                        {
                            query += "\n --- " + i.Descripcion.ToString() + "\n";
                            List<string> securedListString = new List<string>();
                            foreach (string s in tempListString)
                            {
                                string translateValue = string.Empty;

                                if (!string.IsNullOrEmpty(languageCode))
                                {
                                    translateValue = Traductor.TranslateText(s, "es|" + languageCode + "");
                                    securedListString.Add(translateValue);
                                }
                                else
                                {
                                    securedListString.Add(s);
                                }
                            }

                            for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                            {
                                if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[0].Value.ToString()))
                                {
                                    string translatedValue = string.Empty;
                                    if (securedListString.Count > j)
                                    {
                                        translatedValue = securedListString[j];
                                        if (translatedValue.Length > 0)
                                        {
                                            if (this.dgvMainItems.Rows[j].Cells[1] != null &&
                                                this.dgvMainItems.Rows[j].Cells[1].Value != null &&
                                                !string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[0].Value.ToString()))
                                            {
                                                string check = this.dgvMainItems.Rows[j].Cells[0].Value.ToString();

                                                if (char.IsUpper(check[0]) && char.IsLower(translatedValue[0]))
                                                {
                                                    translatedValue = Utils.FirstLetterToUpper(translatedValue);
                                                }
                                                else if (char.IsLower(check[0]) && char.IsUpper(translatedValue[0]))
                                                {
                                                    translatedValue = Utils.FirstLetterToLower(translatedValue);
                                                }
                                            }
                                        }
                                    }

                                    query += "IF COL_LENGTH('" + this.txtNombreTabla.Text + "','" + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() + "') IS NOT NULL \n" +
                                        " BEGIN\n";
                                    query += "  UPDATE dbo." + this.txtNombreTabla.Text + " SET " + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() +
                                        " = '" + translatedValue.Replace("'", "''").TrimEnd().TrimStart() + "' WHERE IDIOMA = '" + i.Codigo + "'\n";
                                    query += " END\n";
                                }
                            }
                        }
                        else
                        {
                            string tempConcatenatedValues = string.Empty;
                            int tempPosition = 0;
                            string tempLast = tempListString.Last();
                            translatedString = string.Empty;
                            foreach (string s in tempListString)
                            {
                                tempConcatenatedValues += s + "[]";
                                if (tempPosition >= 10 ||
                                    s == tempLast
                                    )
                                {
                                    translatedString += Traductor.TranslateText(tempConcatenatedValues, "es|" + languageCode + "");
                                    tempConcatenatedValues = string.Empty;
                                    tempPosition = 0;
                                }
                                tempPosition++;
                            }

                            translatedString = Utils.FormatResultString(translatedString);

                            listStrLineElements = translatedString.Split(new string[] { "[]" }, StringSplitOptions.None).ToList();
                            query += "\n --- " + i.Descripcion.ToString() + " \n";

                            for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                            {
                                if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[0].Value.ToString()))
                                {
                                    string translatedValue = string.Empty;
                                    if (listStrLineElements.Count > j)
                                    {
                                        translatedValue = listStrLineElements[j];
                                        if (translatedValue.Length > 0)
                                        {
                                            if (translatedValue[0] == ' ')
                                            {
                                                translatedValue = translatedValue.TrimStart();
                                            }
                                        }
                                    }

                                    query += "IF COL_LENGTH('" + this.txtNombreTabla.Text + "','" + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() + "') IS NOT NULL \n" +
                                        " BEGIN\n";
                                    query += "  UPDATE dbo." + this.txtNombreTabla.Text + " SET " + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() +
                                        " = '" + translatedValue.Replace("'", "''").TrimEnd().TrimStart() + "' WHERE IDIOMA = '" + i.Codigo + "'\n";
                                    query += " END\n";
                                }
                            }
                        }
                    }

                    if (this.progressBar1.Value < this.progressBar1.Maximum)
                    {
                        this.progressBar1.Value++;
                    }
                }
                this.progressBar1.Value = this.progressBar1.Maximum;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error. Mensaje original: " + ex.Message);
            }
            return query;
        }

        #region Events

        #region Buttons 

        /// <summary>
        /// Handles the 3 event of the button1_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonFastLoad_Click(object sender, EventArgs e)
        {
            bool ok = true;

            //Checks if there is already languages in the panel and ask for confirmation
            if (cbDefault.SelectedIndex > 0)
            {
                if (this.lBotones.Count > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Se perderán los idiomas actuales. ¿Continuar?", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialogResult == DialogResult.No)
                    {
                        ok = false;
                    }
                }
                if (ok)
                {
                    if (this.locIdiomasPerso.ConfigIdiomasPerso.Any(x => x.Key == cbDefault.Text))
                    {
                        this.lNewBotones = this.locIdiomasPerso.ConfigIdiomasPerso.SingleOrDefault(x => x.Key == cbDefault.Text).Value;
                    }
                    else
                    {
                        if (cbDefault.SelectedIndex == 0)
                        {
                            this.lNewBotones = Utils.getDefaultLoyaltyDemoConfig();
                        }
                    }
                    this.lBotones.Clear();
                    this.panel1.Controls.Clear();
                    //this.rellenaCB();



                    this.OrdenaBotones();
                    //this.rellenaCB();
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveFile control.
        /// Saves the richtextbox content as .sql file
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                Stream myStream;
                saveFileDialog1.Filter = "sql files (*.sql)|*.sql|All files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.DefaultExt = ".sql";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string fileName = string.Empty;
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        myStream.Seek(0, SeekOrigin.Begin);
                        fileName = saveFileDialog1.FileName;
                        myStream.Close();
                    }
                }

                using (StreamWriter objWriter = new StreamWriter(saveFileDialog1.FileName))
                {
                    objWriter.Write(richTextBox1.Text);
                    MessageBox.Show("Guardado correctamente.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido algun error al guardar el archivo. Mensaje completo: " + ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnFullSql control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnFullSql_Click(object sender, EventArgs e)
        {
            this.HideAllButtons();
#if DEBUG
            var watch = System.Diagnostics.Stopwatch.StartNew();

#endif
            this.LoadConfigData();
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;

            string values = string.Empty;
            string tablevalue = this.txtNombreTabla.Text;
            string itemvalue = this.txtNombreTabla.Text.Replace("LIT_", string.Empty);

            if (string.IsNullOrWhiteSpace(tablevalue)
               || string.IsNullOrWhiteSpace(itemvalue)
               || itemvalue.Length != 3
               || !tablevalue.Contains("LIT_"))
            {
                values += "-- El formato de tabla es desconocido. Formato correcto: LIT_XXX\n";
            }

            if (this.dgvMainItems.Rows.Count > 0 && this.lNewBotones.Count > 0)
            {
                string extraData = this.ManageData();
                if (!string.IsNullOrEmpty(extraData))
                {
                    values += extraData;
                    values += "\n\n";
                }

                values += "" +
                          "IF(SELECT COUNT(*) FROM sysobjects WHERE NAME LIKE '" + this.txtNombreTabla.Text + "') > 0" +
                          " \n DROP TABLE dbo." + tablevalue + "" +
                          " \n CREATE TABLE dbo." + tablevalue + "(" +
                          " \n [IDIOMA] [varchar](5) NOT NULL";

                for (int i = 0; i < this.dgvMainItems.Rows.Count; i++)
                {
                    if (this.dgvMainItems.Rows[i].Cells[0].Value != null)
                    {
                        if (this.dgvMainItems.Rows[i].Cells[1].Value != null)
                        {
                            if (!string.IsNullOrWhiteSpace(this.dgvMainItems.Rows[i].Cells[0].Value.ToString()))
                            {
                                values += "," + "\n" + " [" + this.dgvMainItems.Rows[i].Cells[0].Value.ToString().ToUpper() + "] VARCHAR(100)";
                            }
                            else
                            {
                                MessageBox.Show("No se permiten espacios en blanco en el nombre del literal. Fila: " + i + "");
                            }
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                    }
                }
                if (!string.IsNullOrEmpty(values))
                {
                    values += "\n" +
                        ")\n" +
                        "GO \n" +
                        "IF(SELECT TABLA_LITERALES FROM OBJETOS WHERE ITEM = '" + itemvalue + "') IS NULL OR CHARINDEX('" + tablevalue + "', (SELECT TABLA_LITERALES FROM OBJETOS WHERE ITEM = '" + itemvalue + "')) <= 0 \n" +
                        " BEGIN \n" +
                        " UPDATE OBJETOS SET TABLA_LITERALES   = '" + tablevalue + "' WHERE ITEM = '" + itemvalue + "' \n" +
                        " END \n" +
                        "ALTER TABLE dbo." + tablevalue + " ADD CONSTRAINT \n" +
                        " PK_" + tablevalue + " PRIMARY KEY CLUSTERED( \n" +
                        " [IDIOMA] \n" +
                        " )" +
                        "WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY] \n" +
                        "GO \n" +
                        " ALTER TABLE dbo." + tablevalue + " WITH CHECK ADD CONSTRAINT \n" +
                        " FK_LIT_" + itemvalue + "_IDIOMAS FOREIGN KEY(IDIOMA) REFERENCES dbo.Idiomas(CODIGO) \n" +
                        "GO";
                }

                foreach (Idioma i in this.lNewBotones)
                {
                    values += "\nINSERT INTO dbo." + tablevalue + " (IDIOMA) VALUES ('" + i.Codigo + "') ";
                }

                values += "\nGO \n";
                values += UpdateTranslateQuery();
                string finalResult = values.Replace(" = ' ", " = '");
                this.richTextBox1.Text = finalResult;
                this.btnCopy.Visible = true;
                this.btOpenWith.Visible = true;
                this.bt_dllGeneration.Visible = true;
                this.btSaveFile.Visible = true;
                this.btParte.Visible = true;
                this.btExecute.Visible = true;
            }
            else
            {
                MessageBox.Show("No se ha introducido ningun literal");
            }
#if DEBUG
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Tiempo de ejecución : {0}", elapsedMs / 1000);
#endif
            this.ShowAllButtons();
        }

        /// <summary>
        /// Handles the Click event of the salvarDatosToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void salvarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            string nombreXMLTabla = string.Empty;
            if (!string.IsNullOrEmpty(this.txtNombreTabla.Text))
            {
                nombreXMLTabla = this.txtNombreTabla.Text;
            }
            else
            {
                Guid nGuid = Guid.NewGuid();
                nombreXMLTabla = nGuid.ToString();
            }

            DataTable locDataTableLitName = new DataTable
            {
                TableName = "nombreTablaLit"
            };
            locDataTableLitName.Columns.Add("NombreTabla");
            locDataTableLitName.Rows.Add(this.txtNombreTabla.Text);


            DataTable locData = new DataTable
            {
                TableName = nombreXMLTabla
            };

            locData.Columns.Add(this.dgvMainItems.Columns[0].HeaderText, typeof(string));
            locData.Columns.Add(this.dgvMainItems.Columns[1].HeaderText, typeof(string));

            foreach (DataGridViewRow dgvR in this.dgvMainItems.Rows)
            {
                locData.Rows.Add(dgvR.Cells[0].Value, dgvR.Cells[1].Value);
            }

            DataTable locTablaIdiomas = new DataTable
            {
                TableName = "TablaIdiomas"
            };

            locTablaIdiomas.Columns.Add("Idioma");

            foreach (Button b in this.panel1.Controls)
            {
                locTablaIdiomas.Rows.Add(b.Text);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(locData);
            ds.Tables.Add(locTablaIdiomas);
            ds.Tables.Add(locDataTableLitName);

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fileName = string.Empty;
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    myStream.Seek(0, SeekOrigin.Begin);

                    fileName = saveFileDialog1.FileName;
                    myStream.Close();
                }

                if (!string.IsNullOrEmpty(fileName))
                {
                    if (!File.Exists(fileName))

                    {
                        FileStream file = File.Create(fileName);
                        file.Close();
                    }
                    ds.WriteXml(fileName, XmlWriteMode.WriteSchema);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the BotonAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Idioma valor = locIdiomas.SingleOrDefault(Y => Y.Codigo == this.cbIdioma.SelectedValue);
                if (!this.lNewBotones.Any(x => x.Codigo == valor.Codigo))
                {
                    this.lNewBotones.Add(valor);
                    OrdenaBotones();
                    this.cbIdioma.Text = string.Empty;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Handles the Click event of the BotonB control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonB_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            Idioma valor = this.lNewBotones.SingleOrDefault(Y => Y.Codigo == b.Name);

            //this.cbIdioma.Items.Add(valor);
            if (this.lNewBotones.Remove(valor))
            {
                this.OrdenaBotones();
            }
        }

        /// <summary>
        /// Loads data from a xml file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cargarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool cargar = true;

            //Checks if there is any data in the DGV and ask for confirmation to replace it 
            if (this.dgvMainItems.Rows.Count > 1)
            {
                DialogResult dialogResult = MessageBox.Show("Al cargar los datos se borrarán los actuales", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.No)
                {
                    cargar = false;
                }
            }

            if (cargar)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                Stream myStream;
                DataSet dataSet = new DataSet();
                DataTable tablaDiccionario = new DataTable();
                DataTable tablaIdiomas = new DataTable();
                DataTable tablaNombreTabla = new DataTable();

                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if ((myStream = openFileDialog1.OpenFile()) != null)
                        {
                            using (myStream)
                            {
                                myStream.Seek(0, SeekOrigin.Begin);

                                dataSet.ReadXml(openFileDialog1.FileName);
                                if (dataSet.Tables.Count > 0)
                                {
                                    tablaDiccionario = dataSet.Tables[0];
                                    this.dgvMainItems.Columns.Clear();
                                    this.dgvMainItems.DataSource = tablaDiccionario;

                                    DataGridViewColumn column = dgvMainItems.Columns[0];
                                    column.Width = 242;
                                    column = dgvMainItems.Columns[1];
                                    column.Width = 243;

                                    foreach (DataGridViewRow r in dgvMainItems.Rows)
                                    {
                                        if (string.IsNullOrEmpty(r.Cells[0].Value.ToString()))
                                        {
                                            this.dgvMainItems.Rows.Remove(r);
                                        }
                                    }

                                    tablaIdiomas = dataSet.Tables[1];
                                    this.panel1.Controls.Clear();
                                    this.LoadCbLanguagues();
                                    //rellenaCB();
                                    this.lBotones.Clear();

                                    foreach (DataRow DR in tablaIdiomas.Rows)
                                    {
                                        try
                                        {
                                            Idiomas valor = (Idiomas)Enum.Parse(typeof(Idiomas), DR.ItemArray[0].ToString());
                                            this.lBotones.Add(valor);
                                            OrdenaBotones();
                                        }
                                        catch (Exception ex) { }
                                    }

                                    if (dataSet.Tables.Count > 2 && dataSet.Tables[2] != null)
                                    {
                                        tablaNombreTabla = dataSet.Tables[2];

                                        foreach (DataRow r in tablaNombreTabla.Rows)
                                        {
                                            if (!string.IsNullOrEmpty(r.ItemArray[0].ToString()))
                                            {
                                                this.txtNombreTabla.Text = r.ItemArray[0].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: No se ha podido leer el archivo. Error original: " + ex.Message);
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Handles the Leave event of the txtNombreTabla control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void txtNombreTabla_Leave(object sender, EventArgs e)
        {
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;
        }

        /// <summary>
        /// Hides all buttons.
        /// </summary>
        private void HideAllButtons()
        {
            this.button1.Visible = false;
            this.btAddLanguage.Visible = false;
            this.btFastLoad.Visible = false;
            this.btFullTraslationSql.Visible = false;
            this.btnCopy.Visible = false;
            this.btUpdate.Visible = false;
            this.bt_dllGeneration.Visible = false;
            this.btOpenWith.Visible = false;
            this.btParte.Visible = false;
            this.btSaveFile.Visible = false;
            this.btExecute.Visible = false;
            this.cbNoTraducir.Enabled = false;
            this.cbSecureMode.Enabled = false;
        }

        /// <summary>
        /// Shows all buttons.
        /// </summary>
        private void ShowAllButtons()
        {
            this.button1.Visible = true;
            this.btAddLanguage.Visible = true;
            this.btFastLoad.Visible = true;
            this.btFullTraslationSql.Visible = true;
            this.btnCopy.Visible = true;
            this.btUpdate.Visible = true;
            this.bt_dllGeneration.Visible = true;
            this.btOpenWith.Visible = true;
            this.btParte.Visible = true;
            this.btSaveFile.Visible = true;
            this.btExecute.Visible = true;
            this.cbNoTraducir.Enabled = true;
            this.cbSecureMode.Enabled = true;
        }

        /// <summary>
        /// Loads the cb languagues.
        /// </summary>
        private void LoadCbLanguagues()
        {
            try
            {
                string servidor = string.Empty;
                string databaseName = string.Empty;
                string user = string.Empty;
                string pass = string.Empty;
                Dictionary<string, string> dcFuc = new Dictionary<string, string>();

                if (string.IsNullOrEmpty(this.MainConfigData.SQLData.Servidor))
                {
                    servidor = "default connection server";
                }
                else
                {
                    servidor = this.MainConfigData.SQLData.Servidor;
                }
                if (string.IsNullOrEmpty(this.MainConfigData.SQLData.BaseDeDatos))
                {
                    databaseName = "default db name";
                }
                else
                {
                    databaseName = this.MainConfigData.SQLData.BaseDeDatos;
                }
                if (string.IsNullOrEmpty(this.MainConfigData.SQLData.IdUsuario) ||
                    string.IsNullOrEmpty(this.MainConfigData.SQLData.PassUsuario))
                {
                    FastUserCredentials FUC = new FastUserCredentials(ref dcFuc);
                    FUC.ShowDialog();
                    if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "user").ToString()))
                    {
                        user = dcFuc.SingleOrDefault(x => x.Key == "user").Value;
                    }
                    else
                    {
                        Exception ex = new Exception("Usuario o contraseña incorrecta");
                        throw ex;
                    }
                    if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "pass").ToString()))
                    {
                        pass = dcFuc.SingleOrDefault(x => x.Key == "pass").Value;
                    }
                    else
                    {
                        Exception ex = new Exception("Usuario o contraseña incorrecta");
                        throw ex;
                    }
                }
                else
                {
                    user = this.MainConfigData.SQLData.IdUsuario;
                    pass = Utils.Decrypt(this.MainConfigData.SQLData.PassUsuario);
                }

                SqlConnection conn = new SqlConnection(
                            "Data Source=" + servidor + ";" +
                            "Initial Catalog=" + databaseName + ";" +
                            "Persist Security Info=True;" +
                            "User Id=" + user + ";" +
                            "Password=" + pass + ";");

                conn.Open();

                string query = "SELECT CODIGO,DESCRIPCION,FLOCALE FROM IDIOMAS";
                SqlCommand cmd = new SqlCommand(query, conn);

                DataTable valuesData = new DataTable();
                valuesData.Load(cmd.ExecuteReader());

                conn.Close();

                foreach (var dr in valuesData.Rows)
                {
                    DataRow dt = (DataRow)dr;
                    Idioma locIdioma = new Idioma();

                    if (dt.ItemArray[0] != null &&
                        !string.IsNullOrEmpty(dt.ItemArray[0].ToString())
                        )
                    {
                        locIdioma.Codigo = dt.ItemArray[0].ToString();

                        if (dt.ItemArray[1] != null &&
                            !string.IsNullOrEmpty(dt.ItemArray[1].ToString())
                        )
                        {
                            locIdioma.Descripcion = dt.ItemArray[1].ToString();

                            if (dt.ItemArray[2] != null &&
                                !string.IsNullOrEmpty(dt.ItemArray[2].ToString())
                            )
                            {
                                locIdioma.Culture = dt.ItemArray[2].ToString();
                            }
                            else
                            {
                                locIdioma.Culture = "unknown";
                            }
                            this.locIdiomas.Add(locIdioma);
                        }
                    }
                }

                this.cbIdioma.DataSource = new BindingSource(locIdiomas, null);
                this.cbIdioma.DisplayMember = "Descripcion";
                this.cbIdioma.ValueMember = "Codigo";
            }
            catch (Exception) { }
        }

        #endregion

        /// <summary>
        /// Handles the Load event of the MainMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainMenu_Load(object sender, EventArgs e)
        {
            toolTipInfo.SetToolTip(this.cbNoTraducir, "Si se marca no se traducirán los valores de los literales");
            toolTipInfo.SetToolTip(this.cbSecureMode, "Si se marca la traducción de cada valor será individual, es más lento pero más preciso");

            this.CenterToScreen();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        /// <summary>
        /// Handles the Click event of the configuraciónToolStripMenuItem1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void configuraciónToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Configuration locConfigView = new Configuration();
            locConfigView.Show();
        }

        /// <summary>
        /// Handles the MouseDoubleClick event of the richTextBox1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        private void richTextBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!string.IsNullOrEmpty(richTextBox1.Text))
            {
                Clipboard.SetText(richTextBox1.Text);
            }
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void loadTableFromDatabase_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtNombreTabla.Text)
                && !string.IsNullOrWhiteSpace(this.txtNombreTabla.Text)
                && this.txtNombreTabla.Text.Contains("LIT_")
                && this.txtNombreTabla.Text.Length > 4
                && this.txtNombreTabla.Text.Length <= 7
                )
            {
                try
                {
                    LoadConfigData();
                    string tableName = this.txtNombreTabla.Text;
                    string servidor = string.Empty;
                    string databaseName = string.Empty;
                    string user = string.Empty;
                    string pass = string.Empty;
                    Dictionary<string, string> dcFuc = new Dictionary<string, string>();

                    if (string.IsNullOrEmpty(this.MainConfigData.SQLData.Servidor))
                    {
                        servidor = "defaultconnectionstring";
                    }
                    else
                    {
                        servidor = this.MainConfigData.SQLData.Servidor;
                    }
                    if (string.IsNullOrEmpty(this.MainConfigData.SQLData.BaseDeDatos))
                    {
                        databaseName = "default database";
                    }
                    else
                    {
                        databaseName = this.MainConfigData.SQLData.BaseDeDatos;
                    }
                    if (string.IsNullOrEmpty(this.MainConfigData.SQLData.IdUsuario) ||
                        string.IsNullOrEmpty(this.MainConfigData.SQLData.PassUsuario))
                    {
                        FastUserCredentials FUC = new FastUserCredentials(ref dcFuc);
                        FUC.ShowDialog();
                        if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "user").ToString()))
                        {
                            user = dcFuc.SingleOrDefault(x => x.Key == "user").Value;
                        }
                        else
                        {
                            Exception ex = new Exception("Usuario o contraseña incorrecta");
                            throw ex;
                        }
                        if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "pass").ToString()))
                        {
                            pass = dcFuc.SingleOrDefault(x => x.Key == "pass").Value;
                        }
                        else
                        {
                            Exception ex = new Exception("Usuario o contraseña incorrecta");
                            throw ex;
                        }
                    }
                    else
                    {
                        user = this.MainConfigData.SQLData.IdUsuario;
                        pass = Utils.Decrypt(this.MainConfigData.SQLData.PassUsuario);
                    }

                    SqlConnection conn = new SqlConnection(
                        "Data Source=" + servidor + ";" +
                        "Initial Catalog=" + databaseName + ";" +
                        "Persist Security Info=True;" +
                        "User Id=" + user + ";" +
                        "Password=" + pass + ";");

                    conn.Open();

                    string query = "SELECT * " +
                        "FROM ilion_admin.." + tableName + " " +
                        "WHERE IDIOMA = '00001'";
                    SqlCommand cmd = new SqlCommand(query, conn);

                    DataTable valuesData = new DataTable();
                    valuesData.Load(cmd.ExecuteReader());
                    DataSet ds = new DataSet();
                    ds.Tables.Add(valuesData);
                    //GenerateDll(ds);
                    DataTable headerData = new DataTable();
                    query = "" +
                        "SELECT COLUMN_NAME " +
                        "FROM ilion_admin.INFORMATION_SCHEMA.COLUMNS " +
                        "WHERE TABLE_NAME = N'" + tableName + "'";
                    cmd = new SqlCommand(query, conn);
                    headerData.Load(cmd.ExecuteReader());

                    conn.Close();
                    if (valuesData != null
                        && valuesData.Rows[0] != null
                        && valuesData.Rows[0].ItemArray != null
                        && valuesData.Rows[0].ItemArray.Count() > 0
                        && headerData != null
                        && headerData.Rows[0] != null
                        && headerData.Rows[0].ItemArray != null
                        && headerData.Rows[0].ItemArray.Count() > 0
                        && headerData.Rows.Count == valuesData.Rows[0].ItemArray.Count()
                        )
                    {
                        DataTable dt = new DataTable();

                        dt.Columns.Add("Nombre del literal");
                        dt.Columns.Add("Valor del literal");

                        int i = 0;
                        foreach (var dr in valuesData.Rows[0].ItemArray)
                        {
                            if (headerData.Rows[i].ItemArray[0].ToString() != "IDIOMA")
                            {
                                object[] o = { headerData.Rows[i].ItemArray[0], dr };
                                dt.Rows.Add(o);
                            }
                            i++;
                        }

                        dgvMainItems.Columns.Clear();
                        dgvMainItems.DataSource = dt;
                        dgvMainItems.Columns[0].Width = 340;
                        dgvMainItems.Columns[1].Width = 342;

                        this.dgvMainItems.Refresh();
                        this.progressBar1.Value = 0;
                        this.btnCopy.Visible = false;
                        this.btOpenWith.Visible = false;
                        this.bt_dllGeneration.Visible = false;
                        this.btSaveFile.Visible = false;
                        this.btParte.Visible = false;
                        this.btExecute.Visible = false;
                        this.richTextBox1.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se ha producido un error. Error original: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("El nombre de la tabla es incorrecto (LIT_XXX)");
            }
        }

        /// <summary>
        /// Handles the Click event of the btOpenWith control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btOpenWith_Click(object sender, EventArgs e)
        {
            try
            {
                string name = "CREATE_TABLE_LIT_";

                if (!string.IsNullOrEmpty(this.txtNombreTabla.Text))
                {
                    name += this.txtNombreTabla.Text;
                }
                else
                {
                    Guid tempId = Guid.NewGuid();
                    name += "temp";
                    name += tempId;
                }

                string tempFile = Path.GetTempPath() + name + ".sql";

                using (StreamWriter objWriter = new StreamWriter(tempFile))
                {
                    objWriter.Write(richTextBox1.Text);
                }

                System.Diagnostics.Process.Start(tempFile);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Tests the DLL.
        /// </summary>
        /// <param name="DSList">The ds list.</param>
        private void GenerateDll(string itemvalue, Idioma idioma, Dictionary<string, string> DSList, string path)
        {
            if (!string.IsNullOrEmpty(idioma.Culture))
            {
                string fileName = string.Empty;
                string LanAssemblyFile = string.Empty,
                       LanAssemblyName = string.Empty,
                       LanResAssemblyName = string.Empty;

                LanAssemblyName = itemvalue + "." + idioma.Culture;
                LanAssemblyFile = LanAssemblyName + ".dll";
                LanResAssemblyName = LanAssemblyName + ".resources";

                //Create assembly resource
                AssemblyName asmName = new AssemblyName();
                asmName.Name = LanAssemblyName;
                asmName.CodeBase = path;
                asmName.CultureInfo = new CultureInfo(idioma.Culture);

                AssemblyBuilder builder = AppDomain.CurrentDomain.DefineDynamicAssembly(
                    asmName,
                    AssemblyBuilderAccess.RunAndSave,
                    path);

                ModuleBuilder myModulo = builder.DefineDynamicModule(LanAssemblyFile, LanAssemblyFile);
                IResourceWriter rw = myModulo.DefineResource(LanResAssemblyName, "description", ResourceAttributes.Public);

                foreach (KeyValuePair<string, string> kvpair in DSList)
                {
                    rw.AddResource(kvpair.Key, kvpair.Value);
                }

                builder.Save(LanAssemblyFile);
            }
        }

        /// <summary>
        /// Handles the Click event of the bt_dllGeneration control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void bt_dllGeneration_Click(object sender, EventArgs e)
        {
            this.HideAllButtons();
            string tablevalue = this.txtNombreTabla.Text;
            string itemvalue = this.txtNombreTabla.Text.Replace("LIT_", "");

            if (string.IsNullOrWhiteSpace(tablevalue)
               || string.IsNullOrWhiteSpace(itemvalue)
               || itemvalue.Length != 3
               || !tablevalue.Contains("LIT_"))
            {
                MessageBox.Show("El formato de tabla es desconocido. Formato correcto: LIT_XXX");
            }

            else
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        string path = fbd.SelectedPath;

                        Dictionary<string, string> locValues = new Dictionary<string, string>();

                        this.progressBar1.Value = 0;
                        this.progressBar1.BackColor = Color.Orange;
                        this.progressBar1.Maximum = this.lNewBotones.Count();
                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            locValues.Add(
                                this.dgvMainItems.Rows[j].Cells[0].Value.ToString(),
                                this.dgvMainItems.Rows[j].Cells[1].Value.ToString());
                        }

                        foreach (Idioma idioma in this.lNewBotones)
                        {
                            if (Utils.EsCastellano(idioma))
                            {
                                GenerateDll(itemvalue, idioma, locValues, path);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(idioma.Culture))
                                {
                                    Dictionary<string, string> locTranslatedValues = new Dictionary<string, string>();

                                    foreach (KeyValuePair<string, string> kvpair in locValues)
                                    {
                                        string value = Traductor.TranslateText(kvpair.Value, "es|" + idioma.Culture.Substring(0, 2) + "");
                                        locTranslatedValues.Add(kvpair.Key, value);
                                    }

                                    GenerateDll(itemvalue, idioma, locTranslatedValues, path);
                                }
                            }
                            this.progressBar1.Value++;
                        }
                    }
                }
            }
            this.progressBar1.Value = this.progressBar1.Maximum;
            this.ShowAllButtons();
        }
        /// <summary>
        /// Handles the Click event of the idiomasToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void idiomasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IdiomasPerso locConfigCustomLanguages = new IdiomasPerso();
            locConfigCustomLanguages = MainConfigData.ConfigCustomLanguages;
            Pantallas.FastLoadLanguages locFastLoadLanguages = new Pantallas.FastLoadLanguages(ref locConfigCustomLanguages);
            locFastLoadLanguages.ShowDialog();
            this.MainConfigData.ConfigCustomLanguages = locConfigCustomLanguages;

            string jsonItem = JsonConvert.SerializeObject(MainConfigData, Formatting.Indented);
            System.IO.File.WriteAllText(MainConfigData.configPath + "configData", jsonItem);
            RellenaConfigIdiomasPerso();

        }

        /// <summary>
        /// Handles the Click event of the btnCopy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(richTextBox1.Text);
        }

        /// <summary>
        /// Handles the Format event of the cbIdioma control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ListControlConvertEventArgs"/> instance containing the event data.</param>
        private void cbIdioma_Format(object sender, ListControlConvertEventArgs e)
        {
            string codigo = ((Idioma)e.ListItem).Codigo;
            string idioma = ((Idioma)e.ListItem).Descripcion;
            e.Value = codigo + " - " + idioma;
        }

        /// <summary>
        /// Handles the Click event of the btUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btUpdate_Click(object sender, EventArgs e)
        {
            this.HideAllButtons();
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;

            string values = "";
            string tablevalue = this.txtNombreTabla.Text;
            string itemvalue = this.txtNombreTabla.Text.Replace("LIT_", "");

            if (string.IsNullOrWhiteSpace(tablevalue)
               || string.IsNullOrWhiteSpace(itemvalue)
               || itemvalue.Length != 3
               || !tablevalue.Contains("LIT_"))
            {
                values += "-- El formato de tabla es desconocido. Formato correcto: LIT_XXX\n";
            }

            if (this.dgvMainItems.Rows.Count > 0 && this.lNewBotones.Count > 0)
            {
                values += UpdateTranslateQuery();
                string finalResult = values.Replace(" = ' ", " = '");
                this.richTextBox1.Text = finalResult;
                this.btnCopy.Visible = true;
                this.btOpenWith.Visible = true;
                this.bt_dllGeneration.Visible = true;
                this.btSaveFile.Visible = true;
                this.btParte.Visible = true;
                this.btExecute.Visible = true;
            }
            this.ShowAllButtons();
        }

        /// <summary>
        /// Handles the Click event of the btExecute control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btExecute_Click(object sender, EventArgs e)
        {
            int TotalTimes = Regex.Matches(this.richTextBox1.Text, "UPDATE").Count;
            DialogResult dialogResult = MessageBox.Show("Se va a ejecutar el script. Esta acción afectará a " + TotalTimes + " elementos.  ¿Continuar?", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (dialogResult == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(this.richTextBox1.Text))
                {
                    try
                    {
                        LoadConfigData();
                        string tableName = this.txtNombreTabla.Text;
                        string servidor = string.Empty;
                        string databaseName = string.Empty;
                        string user = string.Empty;
                        string pass = string.Empty;
                        Dictionary<string, string> dcFuc = new Dictionary<string, string>();

                        if (string.IsNullOrEmpty(this.MainConfigData.SQLData.Servidor))
                        {
                            servidor = "defalut sql connection string";
                        }
                        else
                        {
                            servidor = this.MainConfigData.SQLData.Servidor;
                        }
                        if (string.IsNullOrEmpty(this.MainConfigData.SQLData.BaseDeDatos))
                        {
                            databaseName = "default database";
                        }
                        else
                        {
                            databaseName = this.MainConfigData.SQLData.BaseDeDatos;
                        }
                        if (string.IsNullOrEmpty(this.MainConfigData.SQLData.IdUsuario) ||
                            string.IsNullOrEmpty(this.MainConfigData.SQLData.PassUsuario))
                        {
                            FastUserCredentials FUC = new FastUserCredentials(ref dcFuc);
                            FUC.ShowDialog();
                            if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "user").ToString()))
                            {
                                user = dcFuc.SingleOrDefault(x => x.Key == "user").Value;
                            }
                            else
                            {
                                Exception ex = new Exception("Usuario o contraseña incorrecta");
                                throw ex;
                            }
                            if (!string.IsNullOrEmpty(dcFuc.SingleOrDefault(x => x.Key == "pass").ToString()))
                            {
                                pass = dcFuc.SingleOrDefault(x => x.Key == "pass").Value;
                            }
                            else
                            {
                                Exception ex = new Exception("Usuario o contraseña incorrecta");
                                throw ex;
                            }
                        }
                        else
                        {
                            user = this.MainConfigData.SQLData.IdUsuario;
                            pass = Utils.Decrypt(this.MainConfigData.SQLData.PassUsuario);
                        }

                        SqlConnection conn = new SqlConnection(
                            "Data Source=" + servidor + ";" +
                            "Initial Catalog=" + databaseName + ";" +
                            "Persist Security Info=True;" +
                            "User Id=" + user + ";" +
                            "Password=" + pass + ";");

                        conn.Open();

                        string query = this.richTextBox1.Text;
                        SqlCommand cmd = new SqlCommand(query, conn);

                        DataTable valuesData = new DataTable();
                        valuesData.Load(cmd.ExecuteReader());
                        DataSet ds = new DataSet();
                        ds.Tables.Add(valuesData);
                        DataTable headerData = new DataTable();

                        cmd = new SqlCommand(query, conn);
                        headerData.Load(cmd.ExecuteReader());

                        conn.Close();
                        MessageBox.Show("Se ha ejecutado correctamente.");
                    }
                    catch (SqlException sqlx)
                    {
                        MessageBox.Show("Se ha producido un error con la base de datos. Error original: " + sqlx.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Se ha producido un error. Error original: " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btParte control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btParte_Click(object sender, EventArgs e)
        {
            try
            {
                string values = string.Empty;
                string tablevalue = this.txtNombreTabla.Text;
                string itemvalue = this.txtNombreTabla.Text.Replace("LIT_", string.Empty);

                if (string.IsNullOrWhiteSpace(tablevalue)
                  || string.IsNullOrWhiteSpace(itemvalue)
                  || itemvalue.Length != 3
                  || !tablevalue.Contains("LIT_"))
                {
                    MessageBox.Show("El formato de tabla es desconocido. Formato correcto: LIT_XXX");
                }
                else
                {
                    SaveFileDialog result = new SaveFileDialog();
                    result.Filter = "Documento Excel |*.xlsx";
                    if (result.ShowDialog() == DialogResult.OK)
                    {
                        if (!string.IsNullOrWhiteSpace(result.FileName))
                        {
                            string fname = Path.GetFullPath(result.FileName);

                            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
                            string l = @"ILIONX45/bin/languages/";
                            string p = Directory.GetParent(MainConfigData.configPath).FullName;
                            p += "\\plantilla.xlsx";
                            ExcelFile ef = ExcelFile.Load(p);

                            int columna = 5;

                            foreach (Idioma idioma in this.lNewBotones)
                            {
                                if (!string.IsNullOrEmpty(idioma.Culture))
                                {

                                    ef.Worksheets[2].Cells[columna, 1].Value = l + itemvalue + "." + idioma.Culture + ".dll";
                                    columna++;
                                }
                            }

                            ef.Save(fname);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error: " + ex.Message);
            }
        }

        /// <summary>
        /// Handles the TextChanged event of the tBoxFiltro control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void tBoxFiltro_TextChanged(object sender, EventArgs e)
        {
            string rowFilter = string.Concat("CONVERT(", "[Nombre del literal]", ",System.String) LIKE '%", this.tBoxFiltro.Text, "%' AND ") + string.Concat("CONVERT(", "[Valor del literal]", ",System.String) LIKE '%", this.tbValueFilter.Text, "%'");
            (dgvMainItems.DataSource as DataTable).DefaultView.RowFilter = rowFilter;
        }

        /// <summary>
        /// Handles the TextChanged event of the textBox1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string rowFilter = string.Concat("CONVERT(", "[Nombre del literal]", ",System.String) LIKE '%", this.tBoxFiltro.Text, "%' AND ") + string.Concat("CONVERT(", "[Valor del literal]", ",System.String) LIKE '%", this.tbValueFilter.Text, "%'");
            (dgvMainItems.DataSource as DataTable).DefaultView.RowFilter = rowFilter;
        }
    }
}

