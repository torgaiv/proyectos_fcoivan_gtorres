﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Literals.Pantallas
{
    public partial class TextBoxDialog : Form
    {
        List<string> texts = new List<string>();
        public TextBoxDialog(ref List<string> texts)
        {
            this.texts = texts;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            texts.Add(this.textBox1.Text);
            this.Hide();
        }
    }
}
