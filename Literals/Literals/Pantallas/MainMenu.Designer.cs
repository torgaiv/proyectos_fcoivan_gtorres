﻿namespace Literals
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btAddLanguage = new System.Windows.Forms.Button();
            this.cbIdioma = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.txtNombreTabla = new System.Windows.Forms.TextBox();
            this.lbTableName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btFullTraslationSql = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCargarDatos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSalvarDatos = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.idiomasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btFastLoad = new System.Windows.Forms.Button();
            this.cbDefault = new System.Windows.Forms.ComboBox();
            this.btSaveFile = new System.Windows.Forms.Button();
            this.cbNoTraducir = new System.Windows.Forms.CheckBox();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnCopy = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btOpenWith = new System.Windows.Forms.Button();
            this.cbSecureMode = new System.Windows.Forms.CheckBox();
            this.bt_dllGeneration = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btParte = new System.Windows.Forms.Button();
            this.btExecute = new System.Windows.Forms.Button();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMainItems = new System.Windows.Forms.DataGridView();
            this.tBoxFiltro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbValueFilter = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainItems)).BeginInit();
            this.SuspendLayout();
            // 
            // btAddLanguage
            // 
            this.btAddLanguage.Location = new System.Drawing.Point(339, 348);
            this.btAddLanguage.Name = "btAddLanguage";
            this.btAddLanguage.Size = new System.Drawing.Size(75, 22);
            this.btAddLanguage.TabIndex = 9;
            this.btAddLanguage.Text = "Añadir";
            this.btAddLanguage.UseVisualStyleBackColor = true;
            this.btAddLanguage.Click += new System.EventHandler(this.BotonAdd_Click);
            // 
            // cbIdioma
            // 
            this.cbIdioma.AllowDrop = true;
            this.cbIdioma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIdioma.FormattingEnabled = true;
            this.cbIdioma.Location = new System.Drawing.Point(5, 348);
            this.cbIdioma.Name = "cbIdioma";
            this.cbIdioma.Size = new System.Drawing.Size(328, 21);
            this.cbIdioma.TabIndex = 8;
            this.cbIdioma.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbIdioma_Format);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(5, 375);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(742, 83);
            this.panel1.TabIndex = 7;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(3, 517);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(742, 166);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            this.richTextBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseDoubleClick);
            // 
            // txtNombreTabla
            // 
            this.txtNombreTabla.Location = new System.Drawing.Point(123, 30);
            this.txtNombreTabla.Name = "txtNombreTabla";
            this.txtNombreTabla.Size = new System.Drawing.Size(533, 20);
            this.txtNombreTabla.TabIndex = 16;
            this.txtNombreTabla.Leave += new System.EventHandler(this.txtNombreTabla_Leave);
            // 
            // lbTableName
            // 
            this.lbTableName.AutoSize = true;
            this.lbTableName.Location = new System.Drawing.Point(7, 33);
            this.lbTableName.Name = "lbTableName";
            this.lbTableName.Size = new System.Drawing.Size(102, 13);
            this.lbTableName.TabIndex = 19;
            this.lbTableName.Text = "Nombre de la tabla: ";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.dgvMainItems);
            this.panel2.Location = new System.Drawing.Point(5, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(742, 255);
            this.panel2.TabIndex = 21;
            // 
            // btFullTraslationSql
            // 
            this.btFullTraslationSql.Location = new System.Drawing.Point(378, 461);
            this.btFullTraslationSql.Name = "btFullTraslationSql";
            this.btFullTraslationSql.Size = new System.Drawing.Size(132, 23);
            this.btFullTraslationSql.TabIndex = 23;
            this.btFullTraslationSql.Text = "Generar SQL Completo";
            this.btFullTraslationSql.UseVisualStyleBackColor = true;
            this.btFullTraslationSql.Click += new System.EventHandler(this.btnFullSql_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.configuraciónToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(753, 24);
            this.menuStrip.TabIndex = 25;
            this.menuStrip.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCargarDatos,
            this.tsmSalvarDatos});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // tsmCargarDatos
            // 
            this.tsmCargarDatos.Name = "tsmCargarDatos";
            this.tsmCargarDatos.Size = new System.Drawing.Size(141, 22);
            this.tsmCargarDatos.Text = "Cargar datos";
            this.tsmCargarDatos.Click += new System.EventHandler(this.cargarDatosToolStripMenuItem_Click);
            // 
            // tsmSalvarDatos
            // 
            this.tsmSalvarDatos.Name = "tsmSalvarDatos";
            this.tsmSalvarDatos.Size = new System.Drawing.Size(141, 22);
            this.tsmSalvarDatos.Text = "Salvar datos";
            this.tsmSalvarDatos.Click += new System.EventHandler(this.salvarDatosToolStripMenuItem_Click);
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraciónToolStripMenuItem1,
            this.idiomasToolStripMenuItem});
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.configuraciónToolStripMenuItem.Text = "Herramientas";
            // 
            // configuraciónToolStripMenuItem1
            // 
            this.configuraciónToolStripMenuItem1.Name = "configuraciónToolStripMenuItem1";
            this.configuraciónToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.configuraciónToolStripMenuItem1.Text = "Configuración";
            this.configuraciónToolStripMenuItem1.Click += new System.EventHandler(this.configuraciónToolStripMenuItem1_Click);
            // 
            // idiomasToolStripMenuItem
            // 
            this.idiomasToolStripMenuItem.Name = "idiomasToolStripMenuItem";
            this.idiomasToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.idiomasToolStripMenuItem.Text = "Idiomas";
            this.idiomasToolStripMenuItem.Click += new System.EventHandler(this.idiomasToolStripMenuItem_Click);
            // 
            // btFastLoad
            // 
            this.btFastLoad.Location = new System.Drawing.Point(649, 347);
            this.btFastLoad.Name = "btFastLoad";
            this.btFastLoad.Size = new System.Drawing.Size(96, 23);
            this.btFastLoad.TabIndex = 26;
            this.btFastLoad.Text = "Carga rapida";
            this.btFastLoad.UseVisualStyleBackColor = true;
            this.btFastLoad.Click += new System.EventHandler(this.buttonFastLoad_Click);
            // 
            // cbDefault
            // 
            this.cbDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDefault.FormattingEnabled = true;
            this.cbDefault.Items.AddRange(new object[] {
            "Colombia"});
            this.cbDefault.Location = new System.Drawing.Point(430, 348);
            this.cbDefault.Name = "cbDefault";
            this.cbDefault.Size = new System.Drawing.Size(213, 21);
            this.cbDefault.TabIndex = 27;
            // 
            // btSaveFile
            // 
            this.btSaveFile.Location = new System.Drawing.Point(312, 686);
            this.btSaveFile.Name = "btSaveFile";
            this.btSaveFile.Size = new System.Drawing.Size(156, 22);
            this.btSaveFile.TabIndex = 28;
            this.btSaveFile.Text = "Guardar script";
            this.btSaveFile.UseVisualStyleBackColor = true;
            this.btSaveFile.Visible = false;
            this.btSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // cbNoTraducir
            // 
            this.cbNoTraducir.AutoSize = true;
            this.cbNoTraducir.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNoTraducir.Location = new System.Drawing.Point(521, 464);
            this.cbNoTraducir.Name = "cbNoTraducir";
            this.cbNoTraducir.Size = new System.Drawing.Size(86, 19);
            this.cbNoTraducir.TabIndex = 29;
            this.cbNoTraducir.Text = "No traducir";
            this.cbNoTraducir.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 487);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(742, 23);
            this.progressBar1.TabIndex = 30;
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(208, 686);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(98, 22);
            this.btnCopy.TabIndex = 31;
            this.btnCopy.Text = "Copiar";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Visible = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(662, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "Cargar tabla";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.loadTableFromDatabase_Click);
            // 
            // btOpenWith
            // 
            this.btOpenWith.Location = new System.Drawing.Point(104, 686);
            this.btOpenWith.Name = "btOpenWith";
            this.btOpenWith.Size = new System.Drawing.Size(98, 22);
            this.btOpenWith.TabIndex = 33;
            this.btOpenWith.Text = "Abrir con...";
            this.btOpenWith.UseVisualStyleBackColor = true;
            this.btOpenWith.Visible = false;
            this.btOpenWith.Click += new System.EventHandler(this.btOpenWith_Click);
            // 
            // cbSecureMode
            // 
            this.cbSecureMode.AutoSize = true;
            this.cbSecureMode.Location = new System.Drawing.Point(159, 466);
            this.cbSecureMode.Name = "cbSecureMode";
            this.cbSecureMode.Size = new System.Drawing.Size(88, 17);
            this.cbSecureMode.TabIndex = 34;
            this.cbSecureMode.Text = "Modo seguro";
            this.cbSecureMode.UseVisualStyleBackColor = true;
            // 
            // bt_dllGeneration
            // 
            this.bt_dllGeneration.Location = new System.Drawing.Point(5, 686);
            this.bt_dllGeneration.Name = "bt_dllGeneration";
            this.bt_dllGeneration.Size = new System.Drawing.Size(93, 23);
            this.bt_dllGeneration.TabIndex = 37;
            this.bt_dllGeneration.Text = "Generar DLL";
            this.bt_dllGeneration.UseVisualStyleBackColor = true;
            this.bt_dllGeneration.Visible = false;
            this.bt_dllGeneration.Click += new System.EventHandler(this.bt_dllGeneration_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Location = new System.Drawing.Point(253, 461);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(119, 23);
            this.btUpdate.TabIndex = 38;
            this.btUpdate.Text = "Generar SQL Update";
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btParte
            // 
            this.btParte.Location = new System.Drawing.Point(474, 686);
            this.btParte.Name = "btParte";
            this.btParte.Size = new System.Drawing.Size(98, 22);
            this.btParte.TabIndex = 39;
            this.btParte.Text = "Parte";
            this.btParte.UseVisualStyleBackColor = true;
            this.btParte.Visible = false;
            this.btParte.Click += new System.EventHandler(this.btParte_Click);
            // 
            // btExecute
            // 
            this.btExecute.Location = new System.Drawing.Point(578, 685);
            this.btExecute.Name = "btExecute";
            this.btExecute.Size = new System.Drawing.Size(104, 23);
            this.btExecute.TabIndex = 40;
            this.btExecute.Text = "Ejecutar Script";
            this.btExecute.UseVisualStyleBackColor = true;
            this.btExecute.Visible = false;
            this.btExecute.Click += new System.EventHandler(this.btExecute_Click);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Valor del literal";
            this.Column2.Name = "Column2";
            this.Column2.Width = 350;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nombre del literal";
            this.Column1.Name = "Column1";
            this.Column1.Width = 348;
            // 
            // dgvMainItems
            // 
            this.dgvMainItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMainItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgvMainItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMainItems.Location = new System.Drawing.Point(0, 0);
            this.dgvMainItems.Name = "dgvMainItems";
            this.dgvMainItems.Size = new System.Drawing.Size(742, 255);
            this.dgvMainItems.TabIndex = 4;
            // 
            // tBoxFiltro
            // 
            this.tBoxFiltro.Location = new System.Drawing.Point(123, 61);
            this.tBoxFiltro.Name = "tBoxFiltro";
            this.tBoxFiltro.Size = new System.Drawing.Size(270, 20);
            this.tBoxFiltro.TabIndex = 41;
            this.tBoxFiltro.TextChanged += new System.EventHandler(this.tBoxFiltro_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Filtro por nombre :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(399, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Filtro por valor :";
            // 
            // tbValueFilter
            // 
            this.tbValueFilter.Location = new System.Drawing.Point(484, 61);
            this.tbValueFilter.Name = "tbValueFilter";
            this.tbValueFilter.Size = new System.Drawing.Size(261, 20);
            this.tbValueFilter.TabIndex = 43;
            this.tbValueFilter.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 711);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbValueFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tBoxFiltro);
            this.Controls.Add(this.btExecute);
            this.Controls.Add(this.btParte);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.bt_dllGeneration);
            this.Controls.Add(this.cbSecureMode);
            this.Controls.Add(this.btOpenWith);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cbNoTraducir);
            this.Controls.Add(this.btSaveFile);
            this.Controls.Add(this.cbDefault);
            this.Controls.Add(this.btFastLoad);
            this.Controls.Add(this.btFullTraslationSql);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lbTableName);
            this.Controls.Add(this.txtNombreTabla);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btAddLanguage);
            this.Controls.Add(this.cbIdioma);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainMenu";
            this.ShowIcon = false;
            this.Text = "Literals";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.panel2.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAddLanguage;
        private System.Windows.Forms.ComboBox cbIdioma;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox txtNombreTabla;
        private System.Windows.Forms.Label lbTableName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btFullTraslationSql;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmCargarDatos;
        private System.Windows.Forms.ToolStripMenuItem tsmSalvarDatos;
        private System.Windows.Forms.Button btFastLoad;
        private System.Windows.Forms.ComboBox cbDefault;
        private System.Windows.Forms.Button btSaveFile;
        private System.Windows.Forms.CheckBox cbNoTraducir;
        private System.Windows.Forms.ToolTip toolTipInfo;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btOpenWith;
        private System.Windows.Forms.CheckBox cbSecureMode;
        private System.Windows.Forms.Button bt_dllGeneration;
        private System.Windows.Forms.ToolStripMenuItem idiomasToolStripMenuItem;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btParte;
        private System.Windows.Forms.Button btExecute;
        private System.Windows.Forms.DataGridView dgvMainItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.TextBox tBoxFiltro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbValueFilter;
    }
}