﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Literals.Negocio
{
    public class DatabaseConfig
    {
        /// <summary>
        /// Gets or sets the servidor.
        /// </summary>
        /// <value>
        /// The servidor.
        /// </value>
        public string Servidor { get; set; }
        /// <summary>
        /// Gets or sets the base de datos.
        /// </summary>
        /// <value>
        /// The base de datos.
        /// </value>
        public string BaseDeDatos { get; set; }
        /// <summary>
        /// Gets or sets the identifier usuario.
        /// </summary>
        /// <value>
        /// The identifier usuario.
        /// </value>
        public string IdUsuario { get; set; }
        /// <summary>
        /// Gets or sets the pass usuario.
        /// </summary>
        /// <value>
        /// The pass usuario.
        /// </value>
        public string PassUsuario { get; set; }
    }
}
