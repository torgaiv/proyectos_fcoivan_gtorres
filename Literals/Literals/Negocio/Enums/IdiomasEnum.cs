﻿using System;

namespace Literals
{
    [Obsolete]
    public enum Idiomas
    {
        /// <summary>
        /// UK ENGLISH
        /// </summary>
        Ingles = 3,
        /// <summary>
        /// USA ENGLISH
        /// </summary>
        Americano = 57,
        /// <summary>
        /// FR FRENCH
        /// </summary>
        Frances = 5,
        /// <summary>
        /// SPAIN SPANISH
        /// </summary>
        Español = 1,
        /// <summary>
        /// MEX SPANISH
        /// </summary>
        Mexicano = 4,
        /// <summary>
        /// COLOMBIAN ENGLISH
        /// </summary>
        Colombiano = 25,
        /// <summary>
        /// PERUAN SPANISH
        /// </summary>
        Peruano = 26,
        /// <summary>
        /// PT PORTUGUESE
        /// </summary>
        Portugues = 2,
        /// <summary>
        /// CAT CATALAN
        /// </summary>
        Catalan = 6,
        /// <summary>
        /// ITA ITALIAN
        /// </summary>
        Italiano = 12,
        /// <summary>
        /// CHILEAN SPANISH
        /// </summary>
        Chileno = 9
    }
}