﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Literals.Negocio
{
    public class FastLoadLanguages
    {   
        /// <summary>
        /// Gets the default loyalty demo configuration.
        /// </summary>
        /// <returns></returns>
        public static List<Idiomas> getDefaultLoyaltyDemoConfig()
        {
            List<Idiomas> listaIdiomas = new List<Idiomas>();
            listaIdiomas.Add(Idiomas.Español);
            listaIdiomas.Add(Idiomas.Portugues);
            listaIdiomas.Add(Idiomas.Ingles);
            listaIdiomas.Add(Idiomas.Mexicano);
            listaIdiomas.Add(Idiomas.Frances);
            listaIdiomas.Add(Idiomas.Catalan);
            listaIdiomas.Add(Idiomas.Italiano);
            listaIdiomas.Add(Idiomas.Colombiano);
            return listaIdiomas;
        }
    }
}
