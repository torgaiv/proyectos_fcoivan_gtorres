﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Literals.Negocio
{
    public static class Traductor
    {
        /// <summary>
        /// Translates the text.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="languagePair">The language pair.</param>
        /// <returns></returns>
        public static string TranslateText(string input, string languagePair)
        {
            string languageFrom = languagePair.Substring(0, 2);
            string languageTo = languagePair.Substring(2, 3);
            languageTo = languageTo.Remove(0,1);

       //     string url = String.Format("http://www.google.com/translate_t?hl=en&ie=UTF8&text={0}&langpair={1}", input, languagePair);
            string url= String.Format("https://translate.google.com/m?hl={0}&sl=es&tl={1}&ie=UTF-8&prev=_m&q={2}", languageFrom,languageTo, input);
            HttpClient httpClient = new HttpClient();
            string result = httpClient.GetStringAsync(url).Result;
            string Aresult = System.Net.WebUtility.HtmlDecode(result);
            string TrimValue = "<div dir=\"ltr\" class=\"t0\">";
            int getTranslatedString = Aresult.IndexOf(TrimValue)+TrimValue.Length;
            string getTranslatedFullString = Aresult.Substring(getTranslatedString);

            if (getTranslatedFullString.Contains("\\"))
            {
                if (getTranslatedFullString.Contains("\\x26#39;"))
                {
                    getTranslatedFullString = getTranslatedFullString.Replace("\\x26#39;", "'");
                }
            }

            int FinalValues = getTranslatedFullString.IndexOf("</div><form action=");
            string Result = getTranslatedFullString.Substring(0, FinalValues);            
            return Result.Remove(Result.Length-1);
        }
        
        /// <summary>
        /// Translates this instance.
        /// </summary>
        /// <returns></returns>
        public static string Translate(string value, string idioma)
        {
            string translatedValue = string.Empty;
            string url = String.Format("http://www.google.com/translate_t?hl=en&ie=UTF8&text={0}&langpair={1}", value, idioma);
            HttpClient httpClient = new HttpClient();
            string result = httpClient.GetStringAsync(url).Result;
            result = result.Substring(result.IndexOf("<span title=\"") + "<span title=\"".Length);
            result = result.Substring(result.IndexOf(">") + 1);
            result = result.Substring(0, result.IndexOf("</span>"));
            string decodedString = System.Net.WebUtility.HtmlDecode(result);
            string translatedText = decodedString.Trim();
            return translatedText;
        }
    }
}
