﻿using System;
using System.Collections.Generic;

namespace Literals.Negocio
{
    /// <summary>
    /// ConfigurationData
    /// </summary>
    public class ConfigurationData
    {
        /// <summary>
        /// The configuration path
        /// </summary>
        public string configPath { get; set; }

        /// <summary>
        /// Gets or sets the header information value.
        /// </summary>
        /// <value>
        /// The header information value.
        /// </value>
        public string HeaderInfoValue { get; set; }

        /// <summary>
        /// Gets or sets the actual date information value.
        /// </summary>
        /// <value>
        /// The actual date information value.
        /// </value>
        public string ActualDateInfoValue { get; set; }

        /// <summary>
        /// Gets or sets the configuration data.
        /// </summary>
        /// <value>
        /// The configuration data.
        /// </value>
        public Dictionary<string, string> ConfigData { get; set; }

        /// <summary>
        /// Gets or sets the SQL data.
        /// </summary>
        /// <value>
        /// The SQL data.
        /// </value>
        public DatabaseConfig SQLData { get; set; }

        /// <summary>
        /// Gets or sets the configuration custom languages.
        /// </summary>
        /// <value>
        /// The configuration custom languages.
        /// </value>
        public IdiomasPerso ConfigCustomLanguages { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationData"/> class.
        /// </summary>
        public ConfigurationData()
        {
            this.configPath = Utils.getConfigPath();
            this.ConfigData = new Dictionary<string, string>();
            this.SQLData = new DatabaseConfig();
            this.ConfigCustomLanguages = new IdiomasPerso();
        }
    }
}
