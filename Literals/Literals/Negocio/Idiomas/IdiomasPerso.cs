﻿using System.Collections.Generic;

namespace Literals.Negocio
{
    public class IdiomasPerso
    {
        /// <summary>
        /// Gets or sets the nombre perso configuration.
        /// </summary>
        /// <value>
        /// The nombre perso configuration.
        /// </value>
        public Dictionary<string, List<Idioma>> ConfigIdiomasPerso { get; set; }

        public IdiomasPerso()
        {
            this.ConfigIdiomasPerso = new Dictionary<string, List<Idioma>>();
        }
    }
}
