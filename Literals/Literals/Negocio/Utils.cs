﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Literals.Negocio
{
    /// <summary>
    /// Clase de utiles
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Formats the result string.
        /// </summary>
        /// <param name="argString">The argument string.</param>
        /// <returns></returns>
        public static string FormatResultString(string argString)
        {
            if (argString.Contains("[][]"))
            {
                argString = argString.Replace("[][]", "[]");
            }
            if (argString.Contains("[ ]"))
            {
                argString = argString.Replace("[ ]", "[]");
            }
            if (argString.Contains("[] ]"))
            {
                argString = argString.Replace("[] ]", "[]");
            }
            if (argString.Contains("[ []"))
            {
                argString = argString.Replace("[ []", "[]");
            }
            return argString;
        }

        /// <summary>
        /// Firsts the letter to upper.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        /// <summary>
        /// Firsts the letter to upper.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static string FirstLetterToLower(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToLower(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        /// <summary>
        /// Encrypts the specified clear text.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <returns></returns>
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <returns></returns>
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        /// <summary>
        /// Gets the default loyalty demo configuration.
        /// </summary>
        /// <returns></returns>
        public static List<Idioma> getDefaultLoyaltyDemoConfig()
        {
            List<Idioma> listaIdiomas = new List<Idioma>();
            Idioma EspEsp = new Idioma { Culture = "es-ES", Codigo = "00001", Descripcion = "ESPAÑOL (ESPAÑA)" };
            Idioma PtPt = new Idioma { Culture = "pt-PT", Codigo = "00002", Descripcion = "PORTUGUÉS (PORTUGAL)" };
            Idioma EngIng = new Idioma { Culture = "en-GB", Codigo = "00003", Descripcion = "INGLÉS (REINO UNIDO)" };
            Idioma EspMex = new Idioma { Culture = "es-MX", Codigo = "00004", Descripcion = "ESPAÑOL (MÉJICO)" };
            Idioma FrFra = new Idioma { Culture = "fr-FR", Codigo = "00005", Descripcion = "FRANCÉS (FRANCIA)" };
            Idioma EspCat = new Idioma { Culture = "ca-ES", Codigo = "00006", Descripcion = "CATALÁN (ESPAÑA)" };
            Idioma ItIta = new Idioma { Culture = "it-IT", Codigo = "00012", Descripcion = "ITALIANO (ITALIA)" };
            Idioma EsCol = new Idioma { Culture = "es-CO", Codigo = "00025", Descripcion = "ESPAÑOL (COLOMBIA)" };
            listaIdiomas.Add(EspEsp); listaIdiomas.Add(PtPt); listaIdiomas.Add(EngIng); listaIdiomas.Add(EspMex);
            listaIdiomas.Add(FrFra); listaIdiomas.Add(EspCat); listaIdiomas.Add(ItIta); listaIdiomas.Add(EsCol);
            return listaIdiomas;
        }

        /// <summary>
        /// Devuelve true si el idioma es castellano
        /// </summary>
        /// <returns></returns>
        public static bool EsCastellano(Idioma idioma)
        {
            if (idioma.Codigo != "00006" && (
                idioma.Descripcion.Contains("ñ") ||
                idioma.Descripcion.Contains("Ñ"))
                )
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Codigoes the idioma.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public static string CodigoIdioma(Idioma i)
        {
            if (!string.IsNullOrEmpty(i.Culture))
            {
                return i.Culture.Substring(0, 2);
            }
            return null;
        }

        /// <summary>
        /// Gets the configuration path.
        /// </summary>
        /// <returns></returns>
        public static string getConfigPath()
        {
            return System.Environment.CurrentDirectory + "config";
        }
    }
}
