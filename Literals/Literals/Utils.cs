﻿using System.Collections.Generic;

namespace Literals
{
    /// <summary>
    /// Clase de utiles
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Gets the default loyalty demo configuration.
        /// </summary>
        /// <returns></returns>
        public static List<Idiomas> getDefaultLoyaltyDemoConfig()
        {
            List<Idiomas> listaIdiomas = new List<Idiomas>();
            listaIdiomas.Add(Idiomas.Español);
            listaIdiomas.Add(Idiomas.Portugues);
            listaIdiomas.Add(Idiomas.Ingles);
            listaIdiomas.Add(Idiomas.Mexicano);
            listaIdiomas.Add(Idiomas.Frances);
            listaIdiomas.Add(Idiomas.Catalan);
            listaIdiomas.Add(Idiomas.Italiano);
            listaIdiomas.Add(Idiomas.Colombiano);
            return listaIdiomas;
        }

        /// <summary>
        /// Gets the codigo idioma.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public static string getCodigoIdioma(Idiomas i)
        {
            if (i == Idiomas.Colombiano)
            {
                return "00025";
            }
            else if (i == Idiomas.Español)
            {
                return "00001";
            }
            else if (i == Idiomas.Mexicano)
            {
                return "00004";
            }
            else if (i == Idiomas.Peruano)
            {
                return "00026";
            }
            else if (i == Idiomas.Ingles)
            {
                return "00003";
            }
            else if (i == Idiomas.Americano)
            {
                return "00057";
            }
            else if (i == Idiomas.Catalan)
            {
                return "00006";
            }
            else if (i == Idiomas.Italiano)
            {
                return "00012";
            }
            else if (i == Idiomas.Frances)
            {
                return "00005";
            }
            else if (i == Idiomas.Portugues)
            {
                return "00002";
            }
            else if (i == Idiomas.Chileno)
            {
                return "00009";
            }
            else
            {
                return "00001";
            }
        }

        /// <summary>
        /// Devuelve true si el idioma es castellano
        /// </summary>
        /// <returns></returns>
        public static bool EsCastellano(Idiomas idioma)
        {
            if( idioma == Idiomas.Colombiano ||
                idioma == Idiomas.Español    || 
                idioma == Idiomas.Mexicano   || 
                idioma == Idiomas.Chileno    ||
                idioma == Idiomas.Peruano)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Codigoes the idioma.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <returns></returns>
        public static string CodigoIdioma(Idiomas i)
        {
            string code = null;

            if (i == Idiomas.Colombiano ||
                i == Idiomas.Español ||
                i == Idiomas.Mexicano ||
                i == Idiomas.Peruano ||
                i == Idiomas.Chileno)
            {
                return "es";
            }
            else if (i == Idiomas.Ingles ||
                     i == Idiomas.Americano)
            {
                return "en";
            }
            else if (i == Idiomas.Catalan)
            {
                return "ca";
            }
            else if (i == Idiomas.Italiano)
            {
                return "it";
            }
            else if (i == Idiomas.Frances)
            {
                return "fr";
            }
            else if (i == Idiomas.Portugues)
            {
                return "pt";
            }
            else
            {
                return "es";
            }
        }
    }

    
}
