﻿namespace Literals
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btAddLanguage = new System.Windows.Forms.Button();
            this.cbIdioma = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btTranslate = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.txtNombreTabla = new System.Windows.Forms.TextBox();
            this.lbTableName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvMainItems = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btFullTraslationSql = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCargarDatos = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSalvarDatos = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btFastLoad = new System.Windows.Forms.Button();
            this.cbDefault = new System.Windows.Forms.ComboBox();
            this.btSaveFile = new System.Windows.Forms.Button();
            this.cbNoTraducir = new System.Windows.Forms.CheckBox();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainItems)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btAddLanguage
            // 
            this.btAddLanguage.Location = new System.Drawing.Point(199, 323);
            this.btAddLanguage.Name = "btAddLanguage";
            this.btAddLanguage.Size = new System.Drawing.Size(75, 22);
            this.btAddLanguage.TabIndex = 9;
            this.btAddLanguage.Text = "Añadir";
            this.btAddLanguage.UseVisualStyleBackColor = true;
            this.btAddLanguage.Click += new System.EventHandler(this.BotonAdd_Click);
            // 
            // cbIdioma
            // 
            this.cbIdioma.AllowDrop = true;
            this.cbIdioma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIdioma.FormattingEnabled = true;
            this.cbIdioma.Location = new System.Drawing.Point(5, 324);
            this.cbIdioma.Name = "cbIdioma";
            this.cbIdioma.Size = new System.Drawing.Size(188, 21);
            this.cbIdioma.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(5, 357);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 54);
            this.panel1.TabIndex = 7;
            // 
            // btTranslate
            // 
            this.btTranslate.Location = new System.Drawing.Point(110, 419);
            this.btTranslate.Name = "btTranslate";
            this.btTranslate.Size = new System.Drawing.Size(132, 23);
            this.btTranslate.TabIndex = 5;
            this.btTranslate.Text = "Generar update";
            this.btTranslate.UseVisualStyleBackColor = true;
            this.btTranslate.Click += new System.EventHandler(this.BotonTraduce_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(3, 471);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(528, 183);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // txtNombreTabla
            // 
            this.txtNombreTabla.Location = new System.Drawing.Point(123, 30);
            this.txtNombreTabla.Name = "txtNombreTabla";
            this.txtNombreTabla.Size = new System.Drawing.Size(167, 20);
            this.txtNombreTabla.TabIndex = 16;
            this.txtNombreTabla.Leave += new System.EventHandler(this.txtNombreTabla_Leave);
            // 
            // lbTableName
            // 
            this.lbTableName.AutoSize = true;
            this.lbTableName.Location = new System.Drawing.Point(7, 33);
            this.lbTableName.Name = "lbTableName";
            this.lbTableName.Size = new System.Drawing.Size(102, 13);
            this.lbTableName.TabIndex = 19;
            this.lbTableName.Text = "Nombre de la tabla: ";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.dgvMainItems);
            this.panel2.Location = new System.Drawing.Point(5, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(528, 255);
            this.panel2.TabIndex = 21;
            // 
            // dgvMainItems
            // 
            this.dgvMainItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMainItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgvMainItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMainItems.Location = new System.Drawing.Point(0, 0);
            this.dgvMainItems.Name = "dgvMainItems";
            this.dgvMainItems.Size = new System.Drawing.Size(528, 255);
            this.dgvMainItems.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nombre del literal";
            this.Column1.Name = "Column1";
            this.Column1.Width = 243;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Valor del literal";
            this.Column2.Name = "Column2";
            this.Column2.Width = 242;
            // 
            // btFullTraslationSql
            // 
            this.btFullTraslationSql.Location = new System.Drawing.Point(248, 419);
            this.btFullTraslationSql.Name = "btFullTraslationSql";
            this.btFullTraslationSql.Size = new System.Drawing.Size(132, 23);
            this.btFullTraslationSql.TabIndex = 23;
            this.btFullTraslationSql.Text = "Generar SQL Completo";
            this.btFullTraslationSql.UseVisualStyleBackColor = true;
            this.btFullTraslationSql.Click += new System.EventHandler(this.btnFullSql_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.configuraciónToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(546, 24);
            this.menuStrip.TabIndex = 25;
            this.menuStrip.Text = "menuStrip1";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCargarDatos,
            this.tsmSalvarDatos});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // tsmCargarDatos
            // 
            this.tsmCargarDatos.Name = "tsmCargarDatos";
            this.tsmCargarDatos.Size = new System.Drawing.Size(141, 22);
            this.tsmCargarDatos.Text = "Cargar datos";
            this.tsmCargarDatos.Click += new System.EventHandler(this.cargarDatosToolStripMenuItem_Click);
            // 
            // tsmSalvarDatos
            // 
            this.tsmSalvarDatos.Name = "tsmSalvarDatos";
            this.tsmSalvarDatos.Size = new System.Drawing.Size(141, 22);
            this.tsmSalvarDatos.Text = "Salvar datos";
            this.tsmSalvarDatos.Click += new System.EventHandler(this.salvarDatosToolStripMenuItem_Click);
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraciónToolStripMenuItem1});
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.configuraciónToolStripMenuItem.Text = "Herramientas";
            // 
            // configuraciónToolStripMenuItem1
            // 
            this.configuraciónToolStripMenuItem1.Name = "configuraciónToolStripMenuItem1";
            this.configuraciónToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.configuraciónToolStripMenuItem1.Text = "Configuración";
            // 
            // btFastLoad
            // 
            this.btFastLoad.Location = new System.Drawing.Point(456, 322);
            this.btFastLoad.Name = "btFastLoad";
            this.btFastLoad.Size = new System.Drawing.Size(77, 23);
            this.btFastLoad.TabIndex = 26;
            this.btFastLoad.Text = "Carga rapida";
            this.btFastLoad.UseVisualStyleBackColor = true;
            this.btFastLoad.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // cbDefault
            // 
            this.cbDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDefault.FormattingEnabled = true;
            this.cbDefault.Items.AddRange(new object[] {
            "",
            "Loyalty Demo",
            "Colombia",
            "Peru"});
            this.cbDefault.Location = new System.Drawing.Point(280, 323);
            this.cbDefault.Name = "cbDefault";
            this.cbDefault.Size = new System.Drawing.Size(170, 21);
            this.cbDefault.TabIndex = 27;
            this.cbDefault.SelectedIndexChanged += new System.EventHandler(this.cbPreestablecido_SelectedIndexChanged);
            // 
            // btSaveFile
            // 
            this.btSaveFile.Location = new System.Drawing.Point(224, 657);
            this.btSaveFile.Name = "btSaveFile";
            this.btSaveFile.Size = new System.Drawing.Size(98, 22);
            this.btSaveFile.TabIndex = 28;
            this.btSaveFile.Text = "Guardar script";
            this.btSaveFile.UseVisualStyleBackColor = true;
            this.btSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // cbNoTraducir
            // 
            this.cbNoTraducir.AutoSize = true;
            this.cbNoTraducir.Location = new System.Drawing.Point(386, 423);
            this.cbNoTraducir.Name = "cbNoTraducir";
            this.cbNoTraducir.Size = new System.Drawing.Size(78, 17);
            this.cbNoTraducir.TabIndex = 29;
            this.cbNoTraducir.Text = "No traducir";
            this.cbNoTraducir.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(9, 443);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(522, 23);
            this.progressBar1.TabIndex = 30;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 683);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cbNoTraducir);
            this.Controls.Add(this.btSaveFile);
            this.Controls.Add(this.cbDefault);
            this.Controls.Add(this.btFastLoad);
            this.Controls.Add(this.btFullTraslationSql);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lbTableName);
            this.Controls.Add(this.txtNombreTabla);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btAddLanguage);
            this.Controls.Add(this.cbIdioma);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btTranslate);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainMenu";
            this.ShowIcon = false;
            this.Text = "Literals";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMainItems)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAddLanguage;
        private System.Windows.Forms.ComboBox cbIdioma;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btTranslate;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox txtNombreTabla;
        private System.Windows.Forms.Label lbTableName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btFullTraslationSql;
        private System.Windows.Forms.DataGridView dgvMainItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmCargarDatos;
        private System.Windows.Forms.ToolStripMenuItem tsmSalvarDatos;
        private System.Windows.Forms.Button btFastLoad;
        private System.Windows.Forms.ComboBox cbDefault;
        private System.Windows.Forms.Button btSaveFile;
        private System.Windows.Forms.CheckBox cbNoTraducir;
        private System.Windows.Forms.ToolTip toolTipInfo;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}