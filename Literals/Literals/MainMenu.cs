﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Literals
{
    /// <summary>
    /// Main thread
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainMenu : Form
    {
        /// <summary>
        /// The l botones control
        /// </summary>
        private List<Idiomas> lBotones = new List<Idiomas>();

        private Idiomas MainLanguageValue = Idiomas.Español;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            InitializeComponent();
            rellenaCB();
        }

        /// <summary>
        /// Ordenas the botones.
        /// </summary>
        private void OrdenaBotones()
        {
            //Coordenadas para posicionar los botones
            int x = 0;
            int y = 0;
            this.panel1.Controls.Clear();

            foreach (Idiomas i in lBotones)
            {
                Button B = new Button();
                B.Text = i.ToString();
                B.AutoSize = true;
                int totalControles = this.panel1.Controls.Count;

                if (totalControles == 7)
                {
                    y++;
                    x = 0;
                }

                B.Left = x * 75;
                x++;
                B.Top = y * 23;
                B.Click += new EventHandler(BotonB_Click);
                this.panel1.Controls.Add(B);
                rellenaCB();
            }
        }

        /// <summary>
        /// Fills the combobox with languagues
        /// </summary>
        private void rellenaCB()
        {
            this.cbIdioma.Items.Clear();

            foreach (Idiomas idioma in Enum.GetValues(typeof(Idiomas)))
            {
                bool esta = false;

                if (this.panel1.Controls.Count > 0)
                {
                    foreach (Idiomas i in this.lBotones)
                    {
                        if (idioma == i)
                        {
                            esta = true;
                        }
                    }
                }

                if (!esta)
                {
                    this.cbIdioma.Items.Add(idioma);
                }
            }
        }

        /// <summary>
        /// Gets the translate queries
        /// </summary>
        /// <returns></returns>
        private string UpdateTranslateQuery()
        {
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;

            int intentos = 0;
        startTranslation:
            string query = "";
            try
            {

                this.progressBar1.Maximum = lBotones.Count() + 1;
                this.progressBar1.Value = this.progressBar1.Minimum;
                foreach (Idiomas i in lBotones)
                {
                    string concatenatedValues = "";
                    int milliseconds = 2000;
                    Thread.Sleep(milliseconds);

                    List<string> listStrLineElements = new List<string>();
                    string screen = string.Empty;

                    if (cbNoTraducir.Checked || Utils.EsCastellano(i))
                    {
                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[1].Value.ToString()))
                            {
                                concatenatedValues += this.dgvMainItems.Rows[j].Cells[1].Value.ToString() + ":";
                            }
                        }

                        string translatedString = concatenatedValues;
                        listStrLineElements = translatedString.Split(':').ToList();
                    }
                    else
                    {
                        string ParValues = string.Empty;

                        int TotalRows = 0;

                        for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                        {
                            if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[1].Value.ToString()))
                            {
                                concatenatedValues += this.dgvMainItems.Rows[j].Cells[1].Value.ToString() + ":%20";
                                TotalRows++;
                            }                            
                        }


                        string languageCode = Utils.CodigoIdioma(i);
                        string translatedString = Traductor.TranslateText(concatenatedValues, "es|" + languageCode + "");
                        
                        listStrLineElements = translatedString.Split(':').ToList();
                    }

                    query += "\n --- " + i.ToString() + " \n";

                    for (int j = 0; j < this.dgvMainItems.Rows.Count - 1; j++)
                    {
                        if (!string.IsNullOrEmpty(this.dgvMainItems.Rows[j].Cells[0].Value.ToString()))
                        {
                            string translatedValue = "";
                            if (listStrLineElements.Count > j)
                            {
                                translatedValue = listStrLineElements[j];
                                if (translatedValue.Length > 0)
                                {
                                    if (translatedValue[0] == ' ')
                                    {
                                        translatedValue.Remove(0);
                                    }
                                }
                            }

                            query += "UPDATE dbo." + this.txtNombreTabla.Text + " SET " + this.dgvMainItems.Rows[j].Cells[0].Value.ToString() +
                                " = '" + translatedValue.Replace("'", "´") + "' WHERE IDIOMA = '" + Utils.getCodigoIdioma(i) + "'\n";
                        }
                    }

                    if (this.progressBar1.Value < this.progressBar1.Maximum)
                    {
                        this.progressBar1.Value++;
                    }
                }
                this.progressBar1.Value = this.progressBar1.Maximum;
            }
            catch (Exception ex)
            {
                //if (ex.Message.Equals("AppId is over the quota"))
                //{
                //    intentos++;
                //    if (intentos < 10)
                //    {
                //        Wait(2000);
                //        goto startTranslation;
                //    }
                //}
                MessageBox.Show("Se ha producido un error. Mensaje original: " + ex.Message);
            }
            return query;
        }

        /// <summary>
        /// Waits the specified miliseconds.
        /// </summary>
        /// <param name="miliseconds">The miliseconds.</param>
        public async void Wait(int miliseconds)
        {
            await Task.Delay(miliseconds);
        }

        /// <summary>
        /// Generars the SQL.
        /// </summary>
        /// <returns></returns>
        public string GenerarSQL(string literal, string literalValue)
        {
            string query = "";

            //try
            //{
            //    foreach (Idiomas i in this.lBotones)
            //    {
            //        query += "UPDATE dbo." + this.txtNombreTabla.Text + " SET " + literal + " = '" + Translator.Translate(literalValue, null, Utils.CodigoIdioma(i)).Replace("'", "´") + "' WHERE IDIOMA = '" + Utils.getCodigoIdioma(i) + "'\n";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Se ha producido un error de API. Reintentelo en breve.");
            //}
            return query;
        }


        #region Events

        #region Buttons 

        /// <summary>
        /// Handles the Click event of the btnFullSql control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnFullSql_Click(object sender, EventArgs e)
        {
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;

            string values = "";
            string tablevalue = this.txtNombreTabla.Text;
            string itemvalue = this.txtNombreTabla.Text.Replace("LIT_", "");
            string updatequery = "";

            if (string.IsNullOrWhiteSpace(tablevalue)
               || string.IsNullOrWhiteSpace(itemvalue)
               || itemvalue.Length != 3
               || !tablevalue.Contains("LIT_"))
            {
                values += "-- El formato de tabla es desconocido. Formato correcto: LIT_XXX\n";
                //this.progressBar1.ForeColor = Color.Yellow;
                //progressBar1.Style = ProgressBarStyle.Continuous;
                //this.progressBar1.BackColor = Color.Yellow;
            }

            //this.progressBar1.Maximum = this.lBotones.Count + 2;
            //this.progressBar1.Value = 0;
            if (this.dgvMainItems.Rows.Count > 0 && this.lBotones.Count > 0)
            {
                values += "" +
                          "IF(SELECT COUNT(*) FROM sysobjects WHERE NAME LIKE '" + this.txtNombreTabla.Text + "') > 0" +
                          " \n DROP TABLE dbo." + tablevalue + "" +
                          " \n CREATE TABLE dbo." + tablevalue + "(" +
                          " \n [IDIOMA] [varchar](5) NOT NULL";

                for (int i = 0; i < this.dgvMainItems.Rows.Count; i++)
                {
                    if (this.dgvMainItems.Rows[i].Cells[0].Value != null)
                    {
                        if (this.dgvMainItems.Rows[i].Cells[1].Value != null)
                        {
                            if (!string.IsNullOrWhiteSpace(this.dgvMainItems.Rows[i].Cells[0].Value.ToString()))
                            {
                                values += "," + "\n" + " [" + this.dgvMainItems.Rows[i].Cells[0].Value.ToString().ToUpper() + "] VARCHAR(100)";

                                //if (this.progressBar1.Value < this.progressBar1.Maximum)
                                //{
                                //    this.progressBar1.Value++;
                                //}
                            }
                            else
                            {
                                MessageBox.Show("No se permiten espacios en blanco en el nombre del literal. Fila: " + i + "");
                            }
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                    }
                }
                if (!string.IsNullOrEmpty(values))
                {
                    values += "\n" +
                        ")\n" +
                        "GO \n" +
                        "IF(SELECT TABLA_LITERALES FROM OBJETOS WHERE ITEM = '" + itemvalue + "') IS NULL OR CHARINDEX('" + tablevalue + "', (SELECT TABLA_LITERALES FROM OBJETOS WHERE ITEM = '" + itemvalue + "')) <= 0 \n" +
                        " BEGIN \n" +
                        " UPDATE OBJETOS SET TABLA_LITERALES   = '" + tablevalue + "' WHERE ITEM = '" + itemvalue + "' \n" +
                        " END \n" +
                        "ALTER TABLE dbo." + tablevalue + " ADD CONSTRAINT \n" +
                        " PK_" + tablevalue + " PRIMARY KEY CLUSTERED( \n" +
                        " [IDIOMA] \n" +
                        " )" +
                        "WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY] \n" +
                        "GO \n" +
                        " ALTER TABLE dbo." + tablevalue + " WITH CHECK ADD CONSTRAINT \n" +
                        " FK_LIT_" + itemvalue + "_IDIOMAS FOREIGN KEY(IDIOMA) REFERENCES dbo.Idiomas(CODIGO) \n" +
                        "GO";
                }

                foreach (Idiomas i in this.lBotones)
                {
                    values += "\nINSERT INTO dbo." + tablevalue + " (IDIOMA) VALUES ('" + Utils.getCodigoIdioma(i) + "') ";
                }

                values += "\nGO \n";
                values += UpdateTranslateQuery();
                //this.progressBar1.Value = this.progressBar1.Maximum;
                string finalResult = values.Replace(" = ' ", " = '");
                this.richTextBox1.Text = finalResult;
            }
            else
            {
                MessageBox.Show("No se ha introducido ningun literal");
            }
        }

        /// <summary>
        /// Handles the 1 event of the button1_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            this.UpdateTranslateQuery();
        }

        /// <summary>
        /// Handles the Click event of the salvarDatosToolStripMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void salvarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            string nombreXMLTabla = "";
            if (!string.IsNullOrEmpty(this.txtNombreTabla.Text))
            {
                nombreXMLTabla = this.txtNombreTabla.Text;
            }
            else
            {
                Guid nGuid = Guid.NewGuid();
                nombreXMLTabla = nGuid.ToString();
            }

            DataTable locDataTableLitName = new DataTable
            {
                TableName = "nombreTablaLit"
            };
            locDataTableLitName.Columns.Add("NombreTabla");
            locDataTableLitName.Rows.Add(this.txtNombreTabla.Text);


            DataTable locData = new DataTable
            {
                TableName = nombreXMLTabla
            };

            locData.Columns.Add(this.dgvMainItems.Columns[0].HeaderText, typeof(string));
            locData.Columns.Add(this.dgvMainItems.Columns[1].HeaderText, typeof(string));

            foreach (DataGridViewRow dgvR in this.dgvMainItems.Rows)
            {
                locData.Rows.Add(dgvR.Cells[0].Value, dgvR.Cells[1].Value);
            }

            DataTable locTablaIdiomas = new DataTable
            {
                TableName = "TablaIdiomas"
            };

            locTablaIdiomas.Columns.Add("Idioma");

            foreach (Button b in this.panel1.Controls)
            {
                locTablaIdiomas.Rows.Add(b.Text);
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(locData);
            ds.Tables.Add(locTablaIdiomas);
            ds.Tables.Add(locDataTableLitName);

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fileName = "";
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    myStream.Seek(0, SeekOrigin.Begin);

                    fileName = saveFileDialog1.FileName;
                    myStream.Close();
                }

                if (!string.IsNullOrEmpty(fileName))
                {
                    if (!File.Exists(fileName))

                    {
                        FileStream file = File.Create(fileName);
                        file.Close();
                    }
                    ds.WriteXml(fileName, XmlWriteMode.WriteSchema);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the BotonAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Idiomas valor = (Idiomas)Enum.Parse(typeof(Idiomas), this.cbIdioma.Text);
                this.lBotones.Add(valor);
                OrdenaBotones();
                this.cbIdioma.Text = string.Empty;
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Handles the Click event of the BotonB control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonB_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            Idiomas valor = (Idiomas)Enum.Parse(typeof(Idiomas), b.Text);

            this.cbIdioma.Items.Add(b.Text);
            this.lBotones.Remove(valor);
            this.OrdenaBotones();
        }

        /// <summary>
        /// Handles the Click event of the BotonTraduce control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BotonTraduce_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Text = string.Empty;
            if (this.panel1.Controls.Count > 0)
            {
                if (this.dgvMainItems.Rows.Count > 0)
                {
                    for (int i = 0; i < this.dgvMainItems.Rows.Count; i++)
                    {
                        if (this.dgvMainItems.Rows[i].Cells[0].Value != null)
                        {
                            if (this.dgvMainItems.Rows[i].Cells[1].Value != null)
                            {
                                if (!string.IsNullOrWhiteSpace(this.dgvMainItems.Rows[i].Cells[0].Value.ToString()))
                                {
                                    string query = GenerarSQL(this.dgvMainItems.Rows[i].Cells[0].Value.ToString(), this.dgvMainItems.Rows[i].Cells[1].Value.ToString());
                                    this.richTextBox1.Text += query + "\n" + "---------------------------------------------------------------\n";
                                    this.dgvMainItems.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                                }
                                else
                                {
                                    this.dgvMainItems.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                                    MessageBox.Show("No se permiten espacios en blanco en el nombre del literal. Fila: " + i + "");
                                }
                            }
                            else
                            {
                                this.dgvMainItems.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                            }
                        }
                        else
                        {
                            this.dgvMainItems.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se ha introducido ningun literal");
                }
            }
            else
            {
                MessageBox.Show("No se ha introducido ningun idioma");
            }
        }

        /// <summary>
        /// Loads data from a xml file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cargarDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool cargar = true;

            //Checks if there is any data in the DGV and ask for confirmation to replace it 
            if (this.dgvMainItems.Rows.Count > 1)
            {
                DialogResult dialogResult = MessageBox.Show("Al cargar los datos se borrarán los actuales", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.No)
                {
                    cargar = false;
                }
            }

            if (cargar)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                Stream myStream;
                DataSet dataSet = new DataSet();
                DataTable tablaDiccionario = new DataTable();
                DataTable tablaIdiomas = new DataTable();
                DataTable tablaNombreTabla = new DataTable();

                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        if ((myStream = openFileDialog1.OpenFile()) != null)
                        {
                            using (myStream)
                            {
                                myStream.Seek(0, SeekOrigin.Begin);

                                dataSet.ReadXml(openFileDialog1.FileName);
                                if (dataSet.Tables.Count > 0)
                                {
                                    tablaDiccionario = dataSet.Tables[0];
                                    this.dgvMainItems.Columns.Clear();
                                    this.dgvMainItems.DataSource = tablaDiccionario;

                                    DataGridViewColumn column = dgvMainItems.Columns[0];
                                    column.Width = 242;
                                    column = dgvMainItems.Columns[1];
                                    column.Width = 243;

                                    foreach (DataGridViewRow r in dgvMainItems.Rows)
                                    {
                                        if (string.IsNullOrEmpty(r.Cells[0].Value.ToString()))
                                        {
                                            this.dgvMainItems.Rows.Remove(r);
                                        }
                                    }

                                    tablaIdiomas = dataSet.Tables[1];
                                    this.panel1.Controls.Clear();
                                    rellenaCB();
                                    this.lBotones.Clear();

                                    foreach (DataRow DR in tablaIdiomas.Rows)
                                    {
                                        try
                                        {
                                            Idiomas valor = (Idiomas)Enum.Parse(typeof(Idiomas), DR.ItemArray[0].ToString());
                                            this.lBotones.Add(valor);
                                            OrdenaBotones();
                                        }
                                        catch (Exception ex) { }
                                    }

                                    if (dataSet.Tables.Count > 2 && dataSet.Tables[2] != null)
                                    {
                                        tablaNombreTabla = dataSet.Tables[2];

                                        foreach (DataRow r in tablaNombreTabla.Rows)
                                        {
                                            if (!string.IsNullOrEmpty(r.ItemArray[0].ToString()))
                                            {
                                                this.txtNombreTabla.Text = r.ItemArray[0].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: No se ha podido leer el archivo. Error original: " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the 3 event of the button1_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click_3(object sender, EventArgs e)
        {
            bool ok = true;

            //Checks if there is already languages in the panel and ask for confirmation
            if (cbDefault.SelectedIndex != 0)
            {
                if (this.lBotones.Count > 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Se perderán los idiomas actuales. ¿Continuar?", "¡Atención!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialogResult == DialogResult.No)
                    {
                        ok = false;
                    }
                }

                if (ok)
                {
                    this.lBotones.Clear();
                    this.panel1.Controls.Clear();
                    this.rellenaCB();
                    if (cbDefault.SelectedIndex == 1)
                    {
                        this.lBotones = Utils.getDefaultLoyaltyDemoConfig();
                    }

                    this.OrdenaBotones();
                    this.rellenaCB();
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnSaveFile control.
        /// Saves the richtextbox content as .sql file
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                Stream myStream;
                saveFileDialog1.Filter = "sql files (*.sql)|*.sql|All files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.DefaultExt = ".sql";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string fileName = "";
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        myStream.Seek(0, SeekOrigin.Begin);
                        fileName = saveFileDialog1.FileName;
                        myStream.Close();
                    }
                }

                using (StreamWriter objWriter = new StreamWriter(saveFileDialog1.FileName))
                {
                    objWriter.Write(richTextBox1.Text);
                    MessageBox.Show("Guardado correctamente.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido algun error al guardar el archivo. Mensaje completo: " + ex.Message);
            }
        }

        #endregion

        /// <summary>
        /// Handles the SelectedIndexChanged event of the cbPreestablecido control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void cbPreestablecido_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbDefault.Text = this.cbDefault.Text;
        }

        /// <summary>
        /// Handles the Leave event of the txtNombreTabla control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void txtNombreTabla_Leave(object sender, EventArgs e)
        {
            string litName = this.txtNombreTabla.Text.ToUpper();
            this.txtNombreTabla.Text = litName;
        }

        #endregion

        private void MainMenu_Load(object sender, EventArgs e)
        {
            toolTipInfo.SetToolTip(this.cbNoTraducir, "Si se activa no se traducirán los valores de los literales");
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

    }
}
