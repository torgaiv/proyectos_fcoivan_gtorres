﻿using System.Web;
using System.Web.Mvc;

namespace Proyecto_Spot_it_Free
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new HandleErrorAttribute());
    }
  }
}
