﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Proyecto_Spot_it_Free.Models
{
    public class Busqueda
    {
        public List<Cancion> Canciones { get; set; }
        public List<Disco> Discos { get; set; }
        public List<Artista> Artistas { get; set; }
        public List<Artista> Locales { get; set; }


        public string querydebusqueda { get; set; }

        public List<int> CancionesID { get; set; }
        public List<int> DiscosID { get; set; }
        public List<string> ArtistasID { get; set; }
        public List<string> LocalesID { get; set; }


        public Busqueda(string busqueda)
        {
            this.querydebusqueda = busqueda;
            this.CancionesID = new List<int>();
            this.DiscosID = new List<int>();
            this.ArtistasID = new List<string>();
            this.LocalesID= new List<string>();


            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);

            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT * FROM Cancion where titulo like '"+busqueda+"%'", conexion);
            SqlDataReader registros = comando.ExecuteReader();
            int i = 0;
            while (registros.Read() && i < 5)
            {
                int c = Convert.ToInt32(registros["Id"].ToString());
                CancionesID.Add(c);


            }
            conexion.Close();


            conexion.Open();
            SqlCommand comando2 = new SqlCommand("SELECT * FROM disco where nombre like '"+querydebusqueda+"%'", conexion);
            SqlDataReader registros2 = comando2.ExecuteReader();

            while (registros2.Read())
            {
                int c = Convert.ToInt32(registros2["Id"].ToString());
                DiscosID.Add(c);

            }
            conexion.Close();

            conexion.Open();
            SqlCommand comando3 = new SqlCommand("SELECT * FROM Artista where nombre_artista like '"+querydebusqueda+"%'", conexion);
            SqlDataReader registros3 = comando3.ExecuteReader();
            i = 0;
            while (registros3.Read())
            {
                string c = registros3["Usuario"].ToString();
                ArtistasID.Add(c);
            }
            conexion.Close();

            conexion.Open();
            SqlCommand comando4 = new SqlCommand("SELECT * FROM USUARIO where localidad='"+querydebusqueda+"'", conexion);
            SqlDataReader registros4 = comando4.ExecuteReader();
            i = 0;
            while (registros4.Read())
            {
                string lcl = registros4["Nick"].ToString();
                LocalesID.Add(lcl);
            }
            conexion.Close();
        }

        public Busqueda(List<Cancion> c, List<Disco> d, List<Artista> a, List<Artista> lcl)
        {
            this.Canciones = c;
            this.Discos = d;
            this.Artistas = a;
            this.Locales= lcl;
        }


    }

}
