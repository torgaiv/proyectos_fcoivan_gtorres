﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Proyecto_Spot_it_Free.Models
{
    public class Explora
    {
        private object validationError;

        public List<int> CancionesID { get; set; }
        public List<int> DiscosID { get; set; }
        public List<string> ArtistasID { get; set; }

         public List<string> LocalId { get; set; }


        public List<Cancion> Canciones { get; set; }
        public List<Disco> Discos { get; set; }
        public List<Artista> Artistas { get; set; }
        public List<Artista> Local { get; set; }


        public Explora(string ex) {
            this.CancionesID = new List<int>();
            this.DiscosID = new List<int>();
            this.ArtistasID = new List<string>();
            this.LocalId= new List<string>();


            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);

            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT TOP 5 * FROM Cancion ORDER BY NEWID()", conexion);
            SqlDataReader registros = comando.ExecuteReader();
            int i = 0;
            while (registros.Read() && i<5)
            {
                int c = Convert.ToInt32(registros["Id"].ToString());
                CancionesID.Add(c);
            }
            conexion.Close();

            conexion.Open();
            SqlCommand comando2 = new SqlCommand("SELECT TOP 5 * FROM Disco ORDER BY NEWID()", conexion);
            SqlDataReader registros2 = comando2.ExecuteReader();
            
            while (registros2.Read() )
            {
                int c = Convert.ToInt32(registros2["Id"].ToString());
                DiscosID.Add(c);
            }
            conexion.Close();

            conexion.Open();
            SqlCommand comando3 = new SqlCommand("SELECT TOP 5 Usuario FROM Artista ORDER BY NEWID()", conexion);
            SqlDataReader registros3 = comando3.ExecuteReader();
            i = 0;
            while (registros3.Read())
            {
                string c = registros3["Usuario"].ToString();
                ArtistasID.Add(c);
            }
            conexion.Close();



            conexion.Open();
            SqlCommand comando4 = new SqlCommand("SELECT * FROM artista  join usuario on (usuario.nick=artista.usuario) where localidad='"+ex+"'", conexion);
            SqlDataReader registros4 = comando4.ExecuteReader();
            i = 0;
            while (registros4.Read())
            {
                string c = registros4["Nick"].ToString();
                LocalId.Add(c);
            }
            conexion.Close();
        }
        public Explora(List<Cancion> c, List<Disco> d, List<Artista> a, List<Artista> local) {
            this.Canciones = c;
            this.Discos = d;
            this.Artistas = a;
            this.Local = local;
        }

    }
}