﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Proyecto_Spot_it_Free.Models
{
    public class Lista_cancion
    {
        public Lista lista { get; set; }
        public List<Cancion> canciones{get;set;}


        public void InsertListaCancion(int lista, int cancion) {

            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["espotiEntities"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand("insert into Listacancion(Cancion,Lista) values("+cancion+","+lista+")", conexion);
            comando.ExecuteNonQuery();
            conexion.Close();

        }
        public List<Cancion> LlenaLista(int id) {

            List<Cancion> cancion = new List<Cancion>();
            try
            {
                string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["espotiEntities"].ConnectionString;
                SqlConnection conexion = new SqlConnection(cadena);
                conexion.Open();
                SqlCommand comando = new SqlCommand("select * from cancion join Listacancion on (Listacancion.Lista=" + id + ")", conexion);
                SqlDataReader registros = comando.ExecuteReader();

                while (registros.Read())
                {
                    Cancion c = new Cancion();
                    c.Id = Convert.ToInt32(registros["Id"].ToString());
                    c.Reproducciones= Convert.ToInt32(registros["Reproducciones"].ToString());
                    c.Titulo = registros["Titulo"].ToString();
                    c.Visible = Convert.ToInt32(registros["Visible"].ToString());
                    c.Votos = Convert.ToInt32(registros["Votos"].ToString());
                    c.Valoracion = Convert.ToDouble(registros["Valoracion"].ToString());
                    c.Disco = Convert.ToInt32(registros["Disco"].ToString());
                    c.Genero = registros["Genero"].ToString();
                    canciones.Add(c);
                    conexion.Close();
                }

            }
            catch (Exception e)
            {
            }
            finally{
                
            }
            return cancion;
        }
        
    }
    
}
 