﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free;
using System.IO;
using System.Data.SqlClient;
using Renci.SshNet;
using System.Diagnostics;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class CancionesController : Controller
    {
        private espotiEntities db = new espotiEntities();
        private int idDelDisco = -1;

        // GET: Canciones
        public async Task<ActionResult> Index()
        {
            var cancion = db.Cancion.Include(c => c.Disco1).Include(c => c.Genero1);
            return View(await cancion.ToListAsync());
        }

        // GET: Canciones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancion cancion = await db.Cancion.FindAsync(id);
            if (cancion == null)
            {
                return HttpNotFound();
            }
            return View(cancion);
        }

        /* GET: Canciones/Create
        public ActionResult Create()
        {
            ViewBag.Disco = new SelectList(db.Disco, "Id", "Nombre");
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion");
            return View();
        }*/
        public ActionResult Create(int iddisco)
        {
            int id = iddisco;
            this.idDelDisco = iddisco;
            ViewBag.Disco= db.Disco.Where(x => x.Id == iddisco).FirstOrDefault();
          
            ViewBag.Disco = new SelectList(db.Disco.Where(x => x.Id == iddisco), "Id", "Nombre");
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion");
            return View();
        }

        public ActionResult SumaRepro(int cancion) {
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["espotiEntities"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand("update Cancion set  Reproducciones=Reproducciones +1 where id ="+cancion+"", conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
            return View();
        }

        // POST: Canciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Titulo,Valoracion,Votos,Reproducciones,Genero,Disco,Visible,ubi")] Cancion cancion, HttpPostedFileBase FileAudio, bool? VisibleC)
        {
            int ide = devuelveid();

            if (ModelState.IsValid)
            {

                /**
                 *Transforma el archivo HTTPPOSTED FILE en un archivo que podemos manejar  
                 */
                if (FileAudio != null)
               {
                    if (VisibleC == null)
                    {
                        cancion.Visible = 0;
                    }
                    else
                    {
                        cancion.Visible = 1;
                    }
                    string filename = Path.GetFileNameWithoutExtension(FileAudio.FileName); //Obitene el nombre sin extension
                    string extension = Path.GetExtension(FileAudio.FileName);
                    filename = ide.ToString() + extension;
                    cancion.Valoracion = 0;
                    cancion.Votos = 0;
                    cancion.Reproducciones = 0;
               //     cancion.Disco =Disco;
                  
                    cancion.ubi= filename; // Este será el nombre que guardamos en la BDD 
                    filename = Path.Combine(Server.MapPath("~/Song/"), filename);
                    FileAudio.SaveAs(filename);
                    //subeArchivoSFTP(filename); 
                    db.Cancion.Add(cancion);
                    await db.SaveChangesAsync();
                }              
               
            }
            ViewBag.Disco = new SelectList(db.Disco, "Id", "Nombre", cancion.Disco);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", cancion.Genero);
            return RedirectToAction("Details","Discos", new { id = devuelvedisco(ide)});
        }

        public bool subeArchivoSFTP(string url)
        {
            bool subida = false;
            const string host = "direcciondelhost";
            const string username = "usuariodelhost";
            const string password = "passdelhost";
            const string workingdirectory = "directoriodelhost";
            string uploadfile = url;
            using (var client = new SftpClient(host, 22, username, password))
            {
                client.Connect();
                client.ChangeDirectory(workingdirectory);
                var listDirectory = client.ListDirectory(workingdirectory);
                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                {
                    client.BufferSize = 4 * 1024; // bypass Payload error large files
                    client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                    subida = true;
                }
            }
            return subida;
        }


        // GET: Canciones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancion cancion = await db.Cancion.FindAsync(id);
            if (cancion == null)
            {
                return HttpNotFound();
            }
            ViewBag.Disco = new SelectList(db.Disco, "Id", "Nombre", cancion.Disco);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", cancion.Genero);
            return View(cancion);
        }
        public int devuelveid()
        {
            string query = "select max(Id) as id  from Cancion";
            int id = -1;
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();

            while (registros.Read())
            {
                try
                {
                    id = Convert.ToInt32(registros["id"].ToString());
                }
                catch (Exception e) 
                {
                    id = 0;
                }
            }
            id++;
            conexion.Close();
            return id;        
        }

        public int devuelvedisco(int idx)
        {
            string query = "select disco as id from cancion where id="+idx+"";
            int id = -1;
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();

            while (registros.Read())
            {
                try
                {
                    id = Convert.ToInt32(registros["id"].ToString());
                }
                catch (Exception e)
                {
                    id = 0;
                }
            }
            conexion.Close();
            return id;
        }


        // POST: Canciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Titulo,Valoracion,Votos,Reproducciones,Genero,Disco,Visible")] Cancion cancion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cancion).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Disco = new SelectList(db.Disco, "Id", "Nombre", cancion.Disco);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", cancion.Genero);
            return RedirectToAction("Details", "Discos", new { id = devuelvedisco(devuelveid()) });
        }

        // GET: Canciones/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cancion cancion = await db.Cancion.FindAsync(id);
            if (cancion == null)
            {
                return HttpNotFound();
            }
            return View(cancion);
        }

        // POST: Canciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            int x = devuelvedisco(id);
            Cancion cancion = await db.Cancion.FindAsync(id);
            db.Cancion.Remove(cancion);
            await db.SaveChangesAsync();
            return RedirectToAction("Details", "Discos", new { id = x });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
