﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data.Entity.Validation;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class DiscosController : Controller
    {
        private espotiEntities db = new espotiEntities();

        // GET: Discos
        public async Task<ActionResult> Index()
        {
            var disco = db.Disco.Include(d => d.Artista1).Include(d => d.Genero1);
            return View(await disco.ToListAsync());
        }

        // GET: Discos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = await db.Disco.FindAsync(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        // GET: Discos/Create
        public ActionResult Create(string nickusuario)
        {
            ViewBag.Artista = new SelectList(db.Artista.Where(x => x.Usuario1.Nick== nickusuario), "Usuario", "Nombre_artista");
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion");
            return View();
        }

        // POST: Discos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Nombre,Artista,Visible,Imagen")] Disco disco, HttpPostedFileBase ImagenDisco, bool? VisibleC)
        {
            int discok = devuelveid();
            if (ImagenDisco != null)
            {
                if (VisibleC == null)
                {
                    disco.Visible = 0;
                }
                else
                {
                    disco.Visible = 1;
                }
                string filename = Path.GetFileNameWithoutExtension(ImagenDisco.FileName); //Obitene el nombre sin extension
                string extension = Path.GetExtension(ImagenDisco.FileName);
                filename = discok.ToString() + extension;
                disco.Imagen = filename; // Este será el nombre que guardamos en la BDD 
                filename = Path.Combine(Server.MapPath("~/Portadas/"), filename);
                ImagenDisco.SaveAs(filename);
                disco.Genero = "nulo";
                // subeArchivoSFTP(filename);
            }
            else
            {
                disco.Imagen = "defaultdisk.png";
            }
            if (ModelState.IsValid)
            {


            }
            try
            {
                db.Disco.Add(disco);
                await db.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("*****************0000000:::::Property: {0} Error: {1}",
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }
            }
           
            ViewBag.Artista = new SelectList(db.Artista, "Usuario", "Nombre_artista", disco.Artista);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", disco.Genero);
            return RedirectToAction("Details", "Discos", new { id = discok }); 
        }

        public int devuelveid()
        {
            string query = "select max(Id) as id  from Disco";
            int id = -1;
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();

            while (registros.Read())
            {
                try
                {
                    id = Convert.ToInt32(registros["id"].ToString());
                }
                catch (Exception e) {
                    id = 0;
                }
            }
            id++;
            conexion.Close();
            return id;
        }      
        // GET: Discos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = await db.Disco.FindAsync(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            ViewBag.Artista = new SelectList(db.Artista, "Usuario", "Nombre_artista", disco.Artista);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", disco.Genero);
            return View(disco);
        }

        // POST: Discos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nombre,Genero,Artista,Visible,Imagen")] Disco disco)
        {
            if (ModelState.IsValid)
            {   
                db.Entry(disco).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = disco.Id });
            }
            ViewBag.Artista = new SelectList(db.Artista, "Usuario", "Nombre_artista", disco.Artista);
            ViewBag.Genero = new SelectList(db.Genero, "Nombre", "Descripcion", disco.Genero);
            return View(disco);
        }

        // GET: Discos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disco disco = await db.Disco.FindAsync(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        // POST: Discos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Disco disco = await db.Disco.FindAsync(id);
            string identidad = disco.Artista1.Usuario1.Nick;
            List<Cancion> c = new List<Cancion>();
            c = disco.Cancion.ToList<Cancion>();

            for (int i = 0; i < c.Count; i++) {
                db.Cancion.Remove(c[i]);
                await db.SaveChangesAsync();
            }

            await db.SaveChangesAsync();
            db.Disco.Remove(disco);
            await db.SaveChangesAsync();
            return RedirectToAction("Details", "Artistas", new { id = identidad });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
