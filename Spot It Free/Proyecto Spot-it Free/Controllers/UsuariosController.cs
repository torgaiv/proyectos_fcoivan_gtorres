﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free;
using System.Net.Mail;
using Renci.SshNet;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.SqlClient;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class UsuariosController : Controller
    {
        private espotiEntities db = new espotiEntities();
        
        // GET: Usuarios
        public async Task<ActionResult> Index()
        {
            var usuario = db.Usuario.Include(u => u.Artista).Include(u => u.Genero);
            return View(await usuario.ToListAsync());
        }

        public ActionResult convertirImagen(string mail)
        {
          var imagen = db.Usuario.Where(x => x.Mail == mail).FirstOrDefault();
          return File(imagen.Imagen, "image/jpeg");
        }

        // GET: Usuarios/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            ViewBag.Nick = new SelectList(db.Artista, "Usuario", "Nombre_artista");
            ViewBag.Generofav = new SelectList(db.Genero, "Nombre", "Descripcion");
            return View();
        }

        // POST: Usuarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Nombre,Apellidos,Mail,Pass,Nick,Generofav,Localidad,Pais,Direccion,Tipo,Telefono,Imagen,Codigo_validacion,Validado")] Usuario usuario, HttpPostedFileBase ImagenUsuario, string apodoartista)
        {
            //Genera un random de 4 numeros
            Random r = new Random();
            int codigo = r.Next(1000,9999);
            int validado = 0;
            bool esArtista = false;
            string apododeartista = "";
            if (apodoartista != string.Empty) {
                apododeartista = apodoartista;
                esArtista = true;
            }
            /**
             *Transforma el archivo HTTPPOSTED FILE en una imagen que podemos manejar  
             */

            if (ImagenUsuario != null)
            {
                string filename = Path.GetFileNameWithoutExtension(ImagenUsuario.FileName); //Obitene el nombre sin extension
                string extension = Path.GetExtension(ImagenUsuario.FileName);
                filename = usuario.Nick + extension;
                usuario.Imagen = filename; // Este será el nombre que guardamos en la BDD 
                filename = Path.Combine(Server.MapPath("~/Images/"), filename);
                ImagenUsuario.SaveAs(filename);
               // subeArchivoSFTP(filename);
            }
            else {
                usuario.Imagen = "default.png";
            }

            if (ModelState.IsValid)
            {
                if (esArtista == null)
                {
                    usuario.Tipo = 0;
                }
                else
                {
                    usuario.Tipo = 0;
                    esArtista = true;
                }
                
                usuario.Codigo_validacion = codigo.ToString();
                usuario.Validado = validado;
                enviaEmail(usuario.Mail, usuario.Nombre, usuario.Codigo_validacion);          

                try
                {
                    db.Usuario.Add(usuario);
                    await db.SaveChangesAsync();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("*****************0000000:::::Property: {0} Error: {1}",
                                validationError.PropertyName,
                                validationError.ErrorMessage);
                        }
                    }
                }

                if (esArtista == true) {
                    insertaArtista(apododeartista, usuario.Nick);
                }
            }
            var obj = db.Usuario.FirstOrDefault(a => a.Nick == usuario.Nick && a.Pass == usuario.Pass);
            Session["Usuario"] = obj;
            ViewBag.Nick = new SelectList(db.Artista, "Usuario", "Nombre_artista", usuario.Nick);
            ViewBag.Generofav = new SelectList(db.Genero, "Nombre", "Descripcion", usuario.Generofav);
            return RedirectToAction("Index", "Validacion"); 
        }
        public void insertaArtista(string apodo,string usuario) {
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand("insert into Artista(usuario,Nombre_artista) values('"+usuario+"','"+apodo+"')", conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }
        public bool subeArchivoSFTP(string url) {
            bool subida = false;
            const string host = "direcciondelhost";
            const string username = "usuariodelhost";
            const string password = "passdelhost";
            const string workingdirectory = "directoriodelhost";
            string uploadfile = url;
            using (var client = new SftpClient(host, 22, username, password))
            {
                client.Connect();
                client.ChangeDirectory(workingdirectory);
                var listDirectory = client.ListDirectory(workingdirectory);
                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                {
                    client.BufferSize = 4 * 1024; // bypass Payload error large files
                    client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                    subida = true;
                }
            }
            return subida;
        }


        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }


        public void enviaEmail(string email, string nombre, string codigo) {
            string mailBodyhtml = "";
            var msg = new MailMessage("spotitfree@gmail.com", email, "Codigo de confirmacion: ", mailBodyhtml);
         //   msg.To.Add(email);
            msg.IsBodyHtml = true;
            msg.Body = "Hola " + nombre + ", tu codigo de confirmacion es: " + codigo + "";

            var smtpClient = new SmtpClient("smtp.gmail.com", 587); //if your from email address is "from@hotmail.com" then host should be "smtp.hotmail.com"**
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new NetworkCredential("spotitfree@gmail.com", "spotitfree.net");
            smtpClient.EnableSsl = true;
            smtpClient.Send(msg);
            Console.WriteLine("Email Sended Successfully");
        }

        // GET: Usuarios/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.Nick = new SelectList(db.Artista, "Usuario", "Nombre_artista", usuario.Nick);
            ViewBag.Generofav = new SelectList(db.Genero, "Nombre", "Descripcion", usuario.Generofav);
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Nombre,Apellidos,Mail,Pass,Nick,Generofav,Localidad,Pais,Direccion,Tipo,Telefono,Imagen,Codigo_validacion,Validado")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Nick = new SelectList(db.Artista, "Usuario", "Nombre_artista", usuario.Nick);
            ViewBag.Generofav = new SelectList(db.Genero, "Nombre", "Descripcion", usuario.Generofav);
            return View(usuario);
        }

        // GET: Usuarios/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Usuario usuario = await db.Usuario.FindAsync(id);
            db.Usuario.Remove(usuario);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
