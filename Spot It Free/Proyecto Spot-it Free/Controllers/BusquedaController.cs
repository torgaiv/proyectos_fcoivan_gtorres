﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free.Models;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class BusquedaController : Controller
    {
        private espotiEntities db = new espotiEntities();

        // GET: Busqueda
        public ActionResult Index(string SearchText)
        {
            Busqueda e = new Busqueda(SearchText);
            List<Cancion> k = new List<Cancion>();
            List<Disco> dk = new List<Disco>();
            List<Artista> at = new List<Artista>();
            List<Artista> local = new List<Artista>();




            //Esto es un comentario
            /* 
             * Esto tambien, no se ejecuta
             */

            for (int i = 0; i < e.CancionesID.Count(); i++)
            {
                int id = e.CancionesID[i];
                Cancion cs = db.Cancion.Where(x => x.Id == id).SingleOrDefault(); 
                k.Add(cs);
            }
            for (int i = 0; i < e.DiscosID.Count(); i++)
            {
                int id = e.DiscosID[i];
                var disco = db.Disco.Where(x => x.Id == id).FirstOrDefault();
                dk.Add(disco);
            }

            for (int i = 0; i < e.ArtistasID.Count(); i++)
            {
                string user = e.ArtistasID[i];
                var artistas = db.Artista.Where(x => x.Usuario1.Nick == user).FirstOrDefault();
                at.Add(artistas);
            }

            for (int i = 0; i < e.LocalesID.Count(); i++)
            {
                string user = e.LocalesID[i];
                var artistasLocales = db.Artista.Where(x => x.Usuario1.Nick == user).FirstOrDefault();
                local.Add(artistasLocales);
            }

            Busqueda BBB = new Busqueda(k, dk, at,local);
            return View(BBB);
        }



    }
}