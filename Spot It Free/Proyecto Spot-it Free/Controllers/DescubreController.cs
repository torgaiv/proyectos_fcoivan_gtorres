﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free.Models;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class DescubreController : Controller
    {
        private espotiEntities db = new espotiEntities();

        // GET: Descubre
        public ActionResult Index()
        {
            Descubre e = new Descubre();
            List<Cancion> k = new List<Cancion>();
            List<Disco> dk = new List<Disco>();
            List<Artista> at = new List<Artista>();



            for (int i = 0; i < e.CancionesID.Count(); i++)
            {
                int id = e.CancionesID[i];
                Cancion cs = db.Cancion.Where(x => x.Id == id).SingleOrDefault();
                k.Add(cs);
            }
            for (int i = 0; i < e.DiscosID.Count(); i++)
            {
                int id = e.DiscosID[i];
                var disco = db.Disco.Where(x => x.Id == id).FirstOrDefault();
                dk.Add(disco);
            }

            for (int i = 0; i < e.ArtistasID.Count(); i++)
            {
                string user = e.ArtistasID[i];
                var artistas = db.Artista.Where(x => x.Usuario1.Nick == user).FirstOrDefault();
                at.Add(artistas);
            }
            Descubre BBB = new Descubre(k, dk, at);
            return View(BBB);
        }
    }
}